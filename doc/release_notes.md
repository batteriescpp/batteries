- 0.58.5 (2025/01/30)
    - Disabled code coverage in pipelines (will re-enable when performance regression(s) are investigated/fixed)
    - Fixed compatibility issue by relaxing CMake version requirement to 3.22

- 0.58.2 (2025/01/13)
    - Fix some doc comment typos
    - Expand the coding style document
    - Add `BATT_SUPPRESS_IF_GCC_VERSION(version, warning_id_str)` to `<batteries/suppress.hpp>`
    - Fix some spurious warnings to enable gcc 13 support (not yet added to pipelines)

- 0.58.1 (2024/11/18)
    - Add code coverage tools to the pipeline docker image
    - Bug fixes/improvements to scripts
    - Fix Conan editable mode
    - Fix bugs re: multiple check failures suppress full trace when multi-threaded
    - Fix bug in Optional that allows implicit conversion from primitive type to strong typedef

- 0.58.0 (2024/10/10)
    - Added support for throwing out of Batteries-based code into a larger app context that uses exceptions.
        - `batt::StatusException` - exception type that wraps a `batt::Status` plus an optional extra message string; derives from `std::runtime_error`
        - `BATT_THROW_IF_NOT_OK(expr)` - macro that accepts any expression evaluating to a `batt::Status` or `batt::StatusOr<T>`; expands to statement that checks for ok and throws `batt::StatusException` if not ok; supports stream-insertion (`<<`) of extra information similar to `BATT_CHECK`
        - `BATT_OK_RESULT_OR_THROW(expr)` - macro that accepts an expression that evalutes to a `batt::StatusOr<T>` and expands to an expression that checks for ok (throws `batt::StatusException` if not), plus a dereference of the `StatusOr` if it is ok

- 0.57.2 (2024/10/02)
    - Fixed compilation error on gcc with C++ std versions before 20 (17, gnu17).

- 0.57.1 (2024/10/01)
    - Fixed #23 (bug) - `batt::seq::chain()` produces sequence that gives incorrect result for `peek()` when first sequence empty, second sequence non-empty.
    - Upgrade script submodule - moved test from top-level Makefile to script/conan-targets.mk; added script/run-tests.sh.

- 0.57.0 (2024/09/25)
    - Added SSL/TLS (`https://`) support to `batt::HttpClient`
        - SSL context initialization can be customized by supplying a `std::function` to `batt::HttpClient::set_ssl_init_fn`.  The function should have the signature `void(Optional<boost::asio::ssl::context>&, const HostAddress&)`, and should construct the passed optional context in-place (using `batt::Optional::emplace`).
    - Worked around race conditions in conan install when downloading/building new packages
    - Added support in CI pipelines for uploading any packages that were built from source during `make install` to a cache artifact server.
    - Upgraded dependencies in the lockfile (and also added openssl, for https support).

- 0.56.2 (2024/09/10)
    - Fixed race condition bugs in `batt::ReadWriteLock` that could lead to panics or deadlocks
    - Added `HoldOwnership` and `ReleaseOwnership` modes for all `WatchAtomic<T>` simple modification functions
        - `HoldOwnership` is the default; it is slightly faster and more scalable, but assumes that the caller will keep the Watch object alive until the function returns
        - `ReleaseOwnership` is slightly slower and less scalable, but is safe to call even when the modification being performed to the Watch value will cause the Watch itself to be deleted
    - Added a `batt::FakeTimeService` instance to `batt::FakeExecutionContext`; added support in `batt::FakeExecutionContext::run_with_random_seed` for simulated timer expirations

- 0.56.0 (2024/09/06)
    - Renamed `batt::Grant` to `batt::SpinGrant`
    - Introduced new Grant implementation, `batt::ScalableGrant`
        - Uses explicit mutex locks and a waiter queue to scale more gracefully than the old Grant (now SpinGrant), where all contenders to claim Grant count use a single `batt::Watch` and CAS-like loops
        - `AsyncScalableGrantTest.ScalabilityBenchmark` test (newly added) demonstrates that, as expected, SpinGrant degrades linearly with the number of contenders, whereas ScalableGrant maintains a constant throughput even when faced with hundreds or thousands of Tasks competing for Grant count.
    - `batt::Grant` is now a type alias to `batt::ScalableGrant`
    - Minor bug fixes that cause spurious failed checks when panicking on another thread

- 0.55.1 (2024/07/25)
    - Fixed bug where printing a `batt::WaitForResource` to a `std::ostream` results in a crash
    - Added test for `std::ostream` printing of `batt::CountMetric`

- 0.55.0 (2024/07/19)
    - Added `batt::LazyLatch<T>` - like `batt::Latch<T>` but
      automatically initializes when value is requested, via an
      `init_fn` passed in at construction time.

- 0.54.0
    - Added template parameter `bool kAllowAlloc` for `batt::SmallFn`,
      which enables falling back to dynamic (heap) allocation if a
      type-erased function is too large to fit in the inline small
      object buffer (similar to `batt::SmallVec`).
    - Added `batt::InstanceCounter<T>` for easy debug tracking of the
      number of active instances for a given type.

- 0.53.0
    - Batteries now uses Conan version ranges for dependencies + lock files.
    - Added `batt::StaticTypeMap<KeyTupleT, ValueT>`, to store a fixed-size collection of values indexed by distinct types.

- 0.52.5
    - Added `batt::ReadWriteMutex<T>` to wrap `batt::ReadWriteLock` and an instance of `T`, using an API similar to `batt::Mutex<T>`.

- 0.52.3
    - Fixed race condition bug in `WatchAtomic::poke()`/`Queue<T>::poke()` that could cause the next waiter to miss the poke.

- 0.52.1
    - Added a static assert to detect incorrect use of `batt::seq::map` (when the map function returns void).

- 0.52.0
    - Added `batt::HttpServer`: a basic HTTP/1.1 server implementation based on `batt::Task`, sharing common code with `batt::HttpClient`
    - Added `cleanup_fn` optional argument to `batt::Queue<T>::drain`
    - Added `batt::HttpMessageBase::close()` (inherited by `batt::HttpRequest` and `batt::HttpResponse`)
    - Added `batt::HttpMessageBase::invoke_stream_writer(writer_fn)` (inherited by `batt::HttpRequest` and `batt::HttpResponse`)
    - Moved `serialize` method from `batt::HttpRequest` to `batt::HttpMessageBase` so it can be used by `batt::HttpResponse`
    - Moved `RequestInfo` to top level `batt::HttpMessageInfo` so it can be used with requests and responses

- 0.51.1
    - Added `BATT_ASSERT` statements to detect attempts to access the contents of an invalid `batt::Optional<T>`.

- 0.51.0
    - Added `batt::bit_count`, a portable version of the "how many 1-bits in this integer" machine instruction (POPCOUNT on Intel).

- 0.50.9
    - Added better explanation of the `batt::Mutex<T>::with_lock` (doc comments).
    - Added a `\return` doc tag for `WatchAtomic<T>::set_value`.

- 0.50.7
    - Added `patchelf` package to Docker image for CI/CD pipelines.
    - Added `batt::InlineSubTask<kStackBufferSize>` (`#include <batteries/async/inline_sub_task.hpp>`) for easy allocation/launching of sub-tasks using part of the stack memory of an existing task.

- 0.50.6
    - Added environment variable `BATT_DEFAULT_SCHEDULER_PIN_CPU` to control whether `batt::Runtime::default_scheduler()` pins its thread pool 1:1 to specific CPUs.

- 0.50.5
    - Added `batt::ReadWriteLock`, an implementation of a fair reader/writer mutex as described in "Synchronization Without Contention" (Mellor-Crummey, Scott, 1991)
    - Changed the Dockerfile to install all Python packages (including Conan) in a virtual environment (/opt/venv) instead of the ambient environment.
    - Upgrade the Dockerfile to install Conan version 2.0.16.
    - Added `batt::WatchAtomic<T>::poll()`, which yields the current task and then reloads/returns the watch value.

- 0.50.3
    - Fixed a problem with the CMakeLists.txt and conanfile.py that caused unit tests to be (re-)built when running conan install from a downstream project.

- 0.50.1
    - Added `batt::AtomicCounterGauge<T>`, an atomic variable with both gauge-like (moves up and down) and counter-like (add/subtract counters are monotonically increasing) semantics.

- 0.49.2
    - Added `batt::BoolStatus` (`<batteries/bool_status.hpp>`), an enum that has true/false/unknown values and overloaded logical operators.

- 0.49.1
    - Added to `<batteries/type_traits.hpp>`:
        - `is_any_true(expr0, expr1, expr2, ...)`: (`constexpr`) Returns true (and short circuits) at the first `true`-valued argument; else `false`
        - `are_all_true(expr0, expr1, expr2, ...)`: (`constexpr`) Returns false (and short circuits) at the first `false`-valued argument else `true`
        - `IsOneOf<T, M0, M1, ...>`: (`inline constexpr bool`) True iff `T` is exactly one of the set `M0`, `M1`, ...
        - `DecaysToOneOf<T, M0, M1, ...>`: (`inline constexpr bool`) True iff `std::decay_t<T>` is one of the set `M0`, `M1`, ...

- 0.49.0
    - Added `BATT_RETURN_OK_RESULT(expr)`, which unwraps and returns expr if it evaluates to an ok status, continues without returning otherwise.

- 0.48.9/0.48.10 (2023/11/27)
    - Fix conan install bug in conanfile.py: search for script/batt.py relative to the source folder, in addition to the conanfile.py folder.

- 0.48.8 (2023/11/21)
    - Add `batt::Promise<T>::is_ready()` to test whether a future value or error has been set.

- 0.48.4 (2023/11/16)
    - `batt::Status` now preserves message strings from `std::error_code` (for the built-in error categories: `std::system_category()`, `std::generic_category()`, `std::future_category()`, and `std::iostream_category()`).

- 0.48.3 (2023/11/15)
    - Added header `operators.hpp`:
        - `BATT_OBJECT_PRINT_IMPL(inline_decl, type, fields_tuple)`
            - Defines `<<` to print the object to an ostream
        - `BATT_OBJECT_EQUALS_IMPL(inline_decl, type, fields_tuple)`
            - Defines `==` as conjunction of field-wise equality
        - `BATT_OBJECT_LESS_THAN_IMPL(inline_decl, type, fields_tuple)`
            - Defines `<` as a lexicographical ordering of the field orders
        - `BATT_EQUALITY_COMPARABLE(inline_decl, left_type, right_type)`
            - Defines `!=` based on `==`
        - `BATT_TOTALLY_ORDERED(inline_decl, left_type_right_type)`
            - Defines `>`, `<=`, and `>=` based on `<`

- 0.48.2 (2023/11/06)
    - Added `batt::Future<T>::from_value`, `batt::Future<T>::from_error`: convenient API for making completed Futures.
    - Print `batt::Status` as `GROUP.CODE:MESSAGE` instead of `VALUE:MESSAGE`; this allows the savvy reader to convert printed Status values to, for example, errno codes by inspection.

- 0.47.0 (2023/10/17)
    - Added `batt::Future<T>::and_then` for monadic future composition.

- 0.33.0 (2023/05/19)
    - Added Conan options for build variants that affect ABI:
        - `header_only`
        - `glog_support`
        - `protobuf_support`
        - `asserts_enabled`
        - `shared`
    - Added `<batteries/stacktrace.hpp>`, which includes boost
      stacktrace and suppresses spurious GCC warnings in the boost
      code.
    - Added a runtime check system to `<batteries/config.hpp>` that
      computes a "feature [bit-]mask" which captures the build
      variant; this is done for each compilation unit that includes
      `config.hpp` (using static init of a `bool` in an anonymous
      namespace) and compared to a global, canonical feature mask.  In
      this way, we can now detect bugs where different libraries were
      built with different Batteries feature settings and halt the
      program before undefined behavior ensues.
    - Added new macros to control optimization suppression in the code:
        - `BATT_GCC_BEGIN_OPTIMIZE(setting)` - setting must be a string literal, e.g. "O0"
        - `BATT_GCC_END_OPTIMIZE()` - reverts the optimization level set by `BATT_GCC_BEGIN_OPTIMIZE`
        - `BATT_BEGIN_NO_OPTIMIZE()` - begins a section in which optimization is completely disabled
        - `BATT_END_NO_OPTIMIZE()` - ends a section begun by `BATT_BEGIN_NO_OPTIMIZE`
    - Enabling `BATT_ASSERT` statements can now be done independent of
      the `NDEBUG` define, using `BATT_ASSERTS_ENABLED`, which is
      controlled by the Conan config via the `asserts_enabled` option,
      which can be `None` (default), `True`, or `False`.  If
      `BATT_ASSERTS_ENABLED` is not defined (i.e., `asserts_enabled`
      is `None`), then asserts are only enabled when `NDEBUG` is
      defined, like before.
    - Added support for `std::ostream operator<<` to `batt::LogLevel` (`<batteries/status.hpp>`)
    - Fixed silent ABI breakage bug in
      `batt::detail::NotOkStatusWrapper` that causes aborts at runtime
      when some libraries are compiled with `BATT_GLOG_AVAILABLE` and
      some without.
    - Fixed an overly aggressive check in `batt::Task`

- 0.31.3 (2023/05/03)
    - Fixed bug in TakeNSource that causes hang after limit is reached
      (instead of returning batt::StatusCode::kEndOfStream).

- 0.31.0 (2023/05/03)
    - Added `batt::SingleBufferSource`.

- 0.30.0 (2023/04/13)
    - Fix #4 (Added `std::ostream& operator<<(std::ostream&, batt::Watch<T>)`)
    - Fix #5 (Deadlock if a failed BATT_CHECK generates another failed check when trying to print the message)
    - Fix #6 (Added `batt::Watch<T>::reset()`)

- 0.29.0 (2023/03/31)
    - Add `batt::seq::zip`.

- 0.28.5 (2023/03/17)
    - Add doc comment for `batt::getenv_as`.

- 0.28.4 (2023/03/17)
    - Added `batt::Optional::or_else(Fn)` to match `std::optional`.
    - Changed `batt::require_fail_thread_default_log_level()` to return `batt::Optional<batt::LogLevel>&` instead of `batt::LogLevel&`, and made it default to the _current_ global setting when it is `batt::None`.

- 0.28.3 (2023/03/13)
    - Fix bug that prevented the use of `batt::Future<T>::await`.

- 0.27.1 (2023/03/08)
    - Fixed typo in case\_of.hpp doc.
    - Added support for silent conversion of `batt::StatusOr<batt::NoneType>` to `batt::Status`.

- 0.27.0 (2023/03/07)
    - Added `BATT_CHECK_TYPE(type, expr)` to `<batteries/static_assert.hpp>`:
        - At runtime, evaluates to just `expr`, but adds a compile-time assertion that the type of `expr` is `type`.

- 0.26.0 (2023/03/04)
    - Added `batt::Task::DeferStart{bool}` option to suppress automatic Task starting; also:
        - Added member fn `batt::Task::is_started()`
        - Added member fn `batt::Task::start()`
        - Added class `batt::Task::Options`

- 0.25.3 (2023/03/02)
    - Fixed many missing functions/operators for `batt::Optional<T&>` partial specialization.

- 0.25.0 (2023/02/23)
    - Added `batt::Task::await_signal` (moved from "upper layers").

- 0.24.6 (2023/02/22)
    - Added support for MacOS/Clang build pipelines
    - Added `batt::Task::spawn()`
    - Added `batt::NoDestruct<T>`

- 0.23.1 (2023/02/06)
    - Fixed bug in script/run-with-docker.sh
    - Added xxd to docker images

- 0.22.1 (2023/01/27)
    - Added `batt::DefaultInitialized`, a type that implicitly converts to any default-constructible type.
    - Added a `step` parameter (type `batt::TransferStep &`, batteries/async/fetch.hpp) that allows callers to determine which step failed when `batt::transfer_chunked_data` returns an error.

- 0.21.0 (2023/01/25)
    - Added `batt::register_error_category` to allow applications to specify how `error_code` values with custom `error_category` objects should be converted to `batt::Status`.
    - Upgraded boost to 1.81.0
    - Upgraded gtest to 1.13.0

- 0.20.1 (2023/01/18)
    - Added `batt::transfer_chunked_data` (in batteries/async/fetch.hpp).

- 0.19.8 (2023/01/11)
    - Upgrade to Conan 1.56 (from 1.53).

- 0.19.6 (2022/12/19)
    - Fixed build error with `batt::to_status(std::error_code)`

- 0.19.5 (2022/12/16)
    - Fixed crash bug caused by accidental implicit conversion from error_code enum values to batt::Status.

- 0.19.2 (2022/12/16)
    - Fixed test regression by updating the expected error status to match the boost::system::error_code value.

- 0.19.1 (2022/12/16)
    - Added accurate translation of more boost::system::error_code values into batt::Status.

- 0.19.0 (2022/12/14)
    - Added `batt::Latch<T>::set_error` to explicitly set the Latch to an error status for better readability.
    - Added new header batteries/async/fetch.hpp with:
        - `batt::fetch_chunk`: blocking API for no-copy input streams with an async_fetch method
        - `batt::BasicScopedChunk<Stream>`: automatically calls consume on fetched data at scope exit

- 0.18.1 (2022/12/09)
    - Added `batt::PinnablePtr<T>`.

- 0.18.0 (2022/12/07)
    - Added `batt::Channel<T>::async_read`, along with unit tests for the async methods of `batt::Channel<T>`.

- 0.17.1 (2022/12/02)
    - Added `batt::Runtime::reset()` and `batt::Runtime::is_halted()` so that downstream libraries that use a Google Test global environment to start/stop batt::Runtime can restart the default thread pool, for GTEST_REPEAT/--gtest_repeat testing.

- 0.16.2 (2022/11/30)
    - Fixed a broken link in the release notes.

- 0.16.1 (2022/11/30)
    - Added release notes (this document) to the doc pages.

- 0.16.0 (2022/11/30)
    - Added clamp min/max functions to [batt::Watch][battwatch], to atomically enforce upper/lower bounds on the watched value.
