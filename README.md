# Getting Started

Add batteriesincluded.cc to your conan remotes:

```
conan remote add batteriesincluded https://batteriesincluded.cc/artifactory/api/conan/conan-local
```

Now a package ref to your Conan project requirements (conanfile.py):

```
    requires = [
        ...
        "batteries/0.58.5",
    ]

```

You can find the available [release versions here](https://batteriesincluded.cc/ui/packages/conan:%2F%2Fbatteries?name=batteries&type=packages).

Check out the [documentation](https://batteriescpp.github.io/)!
