##=##=##=#==#=#==#===#+==#+==========+==+=+=+=+=+=++=+++=+++++=-++++=-+++++++++++
# Copyright 2022-2023 Anthony Paul Astolfi
#
from conan import ConanFile
import os, sys, platform

#==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
# Import batt helper utilities module.
#
import script.batt
from script.batt import OVERRIDE, VISIBLE
#
#+++++++++++-+-+--+----- --- -- -  -  -   -

#=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------
#
class BatteriesConan(ConanFile):
    name = "batteries"

    #------ --- -- -  -  -   -
    # version is set automatically from Git tags - DO NOT SET IT HERE
    #------ --- -- -  -  -   -

    license = "Apache Public License 2.0"

    author = "Tony Astolfi <tastolfi@gmail.com>"

    url = "https://github.com/tonyastolfi/batteries.git"

    description = "C++ Essentials left out of std:: and boost::"

    settings = "os", "compiler", "build_type", "arch"

    options = {
        "header_only": [True, False],
        "with_glog": [True, False],
        "with_protobuf": [True, False],
        "with_asserts": [None, True, False],
        "shared": [False],
    }

    default_options = {
        "header_only": False,
        "with_glog": True,
        "with_protobuf": True,
        "with_asserts": None,
        "shared": False,
    }

    build_policy = "missing"

    exports = [
        "script/*.sh",
        "script/**/.sh",
        "script/*.py",
        "script/**/.py",
    ]
    exports_sources = [
        "src/CMakeLists.txt",
        "src/batteries.hpp",
        "src/batteries/*.hpp",
        "src/batteries/*/*.hpp",
        "src/batteries/**/*.hpp",
        "src/batteries/*.ipp",
        "src/batteries/*/*.ipp",
        "src/batteries/**/*.ipp",
        "src/batteries/*.cpp",
        "src/batteries/**/*.cpp",
    ]

    #+++++++++++-+-+--+----- --- -- -  -  -   -

    def _get_cxx_flags(self):
        yield "-D_GNU_SOURCE"

        if self.options.header_only:
            yield "-DBATT_HEADER_ONLY=1"
        else:
            yield "-DBATT_HEADER_ONLY=0"

        if self.options.with_glog:
            yield "-DBATT_WITH_GLOG=1"
        else:
            yield "-DBATT_WITH_GLOG=0"

        if self.options.with_protobuf:
            yield "-DBATT_WITH_PROTOBUF=1"
        else:
            yield "-DBATT_WITH_PROTOBUF=0"

        if self.options.with_asserts == True:
            yield "-DBATT_WITH_ASSERTS=1"
        elif self.options.with_asserts == False:
            yield "-DBATT_WITH_ASSERTS=0"


    def configure(self):
        self.options["gtest"].shared = False
        self.options["boost"].shared = False
        self.options["boost"].without_test = True
        self.options["libunwind"].minidebuginfo = False

    def requirements(self):
        self.requires("boost/[>=1.83.0]", **VISIBLE)
        self.requires("libbacktrace/[>=cci.20210118]", **VISIBLE)
        self.requires("pcg-cpp/[>=cci.20220409]", **VISIBLE)
        self.requires("openssl/[>=3]", **VISIBLE)

        if self.options.with_glog:
            self.requires("glog/[>=0.6.0]", **VISIBLE)

        if self.options.with_protobuf:
            self.requires("protobuf/[>=3.21.12]", **VISIBLE)

        self.test_requires("gtest/[>=1.14.0]")

        if platform.system() == 'Linux':
            self.requires("libunwind/[>=1.7.2]", **VISIBLE, **OVERRIDE)

    #+++++++++++-+-+--+----- --- -- -  -  -   -

    from script.batt import set_version_from_git_tags      as set_version
    from script.batt import cmake_unified_src_layout       as layout
    from script.batt import default_cmake_generate         as generate
    from script.batt import default_cmake_build            as build
    from script.batt import default_cmake_lib_package      as package
    from script.batt import default_lib_package_info       as package_info
    from script.batt import default_lib_package_id         as package_id

    #+++++++++++-+-+--+----- --- -- -  -  -   -
