//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2022-2023 Anthony Paul Astolfi, Eitan Steiner
//
#include <batteries/metrics/metric_collectors.hpp>
#include <batteries/metrics/metric_csv_formatter.hpp>
#include <batteries/metrics/metric_dumper.hpp>
#include <batteries/metrics/metric_formatter.hpp>
#include <batteries/metrics/metric_registry.hpp>
//
#include <batteries/metrics/metric_collectors.hpp>
#include <batteries/metrics/metric_csv_formatter.hpp>
#include <batteries/metrics/metric_dumper.hpp>
#include <batteries/metrics/metric_formatter.hpp>
#include <batteries/metrics/metric_registry.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#if BATT_COMPILER_IS_GCC
#include <experimental/random>
#endif

#include <chrono>
#include <thread>

namespace {

TEST(MetricsStats, BasicLogicTest)
{
    const int first_value = 1;
    batt::StatsMetric<int> stats(first_value);
    EXPECT_EQ(first_value, stats.max());
    EXPECT_EQ(first_value, stats.min());
    EXPECT_EQ(1, stats.count());
    EXPECT_EQ(first_value, stats.total());
    for (int i = 2; i <= 2048; i *= 2) {
        const auto current_count = stats.count() + 1;
        const auto current_total = stats.total() + i;
        stats.update(i);
        EXPECT_EQ(i, stats.max());
        EXPECT_EQ(first_value, stats.min());
        EXPECT_EQ(current_count, stats.count());
        EXPECT_EQ(current_total, stats.total());
    }
    EXPECT_EQ(2048, stats.max());
    EXPECT_EQ(first_value, stats.min());
    EXPECT_EQ(12, stats.count());
    EXPECT_EQ(4095, stats.total());

    stats.reset();
    EXPECT_EQ(std::numeric_limits<int>::min(), stats.max());
    EXPECT_EQ(std::numeric_limits<int>::max(), stats.min());
    EXPECT_EQ(0, stats.count());
    EXPECT_EQ(0, stats.total());
    stats.update(-first_value);
    EXPECT_EQ(-first_value, stats.max());
    EXPECT_EQ(-first_value, stats.min());
    EXPECT_EQ(1, stats.count());
    EXPECT_EQ(-first_value, stats.total());
    for (int i = 2; i <= 2048; i *= 2) {
        const auto current_count = stats.count() + 1;
        const auto current_total = stats.total() - i;
        stats.update(-i);
        EXPECT_EQ(-first_value, stats.max());
        EXPECT_EQ(-i, stats.min());
        EXPECT_EQ(current_count, stats.count());
        EXPECT_EQ(current_total, stats.total());
    }
    EXPECT_EQ(-first_value, stats.max());
    EXPECT_EQ(-2048, stats.min());
    EXPECT_EQ(12, stats.count());
    EXPECT_EQ(-4095, stats.total());

    stats.reset();
    EXPECT_EQ(std::numeric_limits<int>::min(), stats.max());
    EXPECT_EQ(std::numeric_limits<int>::max(), stats.min());
    EXPECT_EQ(0, stats.count());
    EXPECT_EQ(0, stats.total());
    stats.update(42);
    EXPECT_EQ(42, stats.max());
    EXPECT_EQ(42, stats.min());
    EXPECT_EQ(1, stats.count());
    EXPECT_EQ(42, stats.total());
    stats.update(41);
    EXPECT_EQ(42, stats.max());
    EXPECT_EQ(41, stats.min());
    EXPECT_EQ(2, stats.count());
    EXPECT_EQ(83, stats.total());
    stats.update(44);
    EXPECT_EQ(44, stats.max());
    EXPECT_EQ(41, stats.min());
    EXPECT_EQ(3, stats.count());
    EXPECT_EQ(127, stats.total());
    stats.update(48);
    EXPECT_EQ(48, stats.max());
    EXPECT_EQ(41, stats.min());
    EXPECT_EQ(4, stats.count());
    EXPECT_EQ(175, stats.total());
    stats.update(40);
    EXPECT_EQ(48, stats.max());
    EXPECT_EQ(40, stats.min());
    EXPECT_EQ(5, stats.count());
    EXPECT_EQ(215, stats.total());
}

TEST(MetricsStats, BasicConcurrentTest)
{
    batt::StatsMetric<int> stats;
    // Initial state:
    EXPECT_EQ(std::numeric_limits<int>::min(), stats.max());
    EXPECT_EQ(std::numeric_limits<int>::max(), stats.min());
    EXPECT_EQ(0, stats.count());
    EXPECT_EQ(0, stats.total());
    // Concurrent updates:
    auto run_update = [&stats](int from, int to, int step) {
        for (int i = from; (step > 0 ? i <= to : i >= to); i += step) {
            stats.update(i);
        }
    };
    std::thread t1(run_update, 52, 100, 2);  // even - up
    std::thread t2(run_update, 50, 2, -2);   // even - down
    std::thread t3(run_update, 51, 99, 2);   // odd  - up
    std::thread t4(run_update, 49, 1, -2);   // odd  - down
    t1.join();
    t2.join();
    t3.join();
    t4.join();
    // Final state:
    EXPECT_EQ(100, stats.max());
    EXPECT_EQ(1, stats.min());
    EXPECT_EQ(100, stats.count());
    EXPECT_EQ(5050, stats.total());
}

TEST(MetricsStats, FocusedConcurrentTest)
{
    constexpr int RUNS = 100;
    batt::StatsMetric<int> stats;
    auto run_update = [&stats](int value) {
        for (int i = 0; i < RUNS; i++) {
            stats.update(value);
        }
    };
    // Initial state:
    stats.reset();
    EXPECT_EQ(std::numeric_limits<int>::min(), stats.max());
    EXPECT_EQ(std::numeric_limits<int>::max(), stats.min());
    EXPECT_EQ(0, stats.count());
    EXPECT_EQ(0, stats.total());
    {
        // Concurrent updates:
        std::thread t1(run_update, 4);
        std::thread t2(run_update, 3);
        std::thread t3(run_update, 2);
        std::thread t4(run_update, 1);
        std::thread t5(run_update, 8);
        std::thread t6(run_update, 7);
        std::thread t7(run_update, 6);
        std::thread t8(run_update, 5);
        // Interim state:
        EXPECT_LE(1, stats.min());
        EXPECT_LE(stats.max(), 8);
        t1.join();
        t2.join();
        t3.join();
        t4.join();
        t5.join();
        t6.join();
        t7.join();
        t8.join();
    }
    // Final state:
    EXPECT_EQ(8, stats.max());
    EXPECT_EQ(1, stats.min());
    EXPECT_EQ(8 * RUNS, stats.count());
    EXPECT_EQ(36 * RUNS, stats.total());
}

TEST(MetricsStats, RegistryTest)
{
    const batt::MetricLabel label{batt::Token("job"), batt::Token("batt")};

    batt::StatsMetric<int> stats;
    EXPECT_EQ(std::numeric_limits<int>::min(), stats.max());
    EXPECT_EQ(std::numeric_limits<int>::max(), stats.min());
    EXPECT_EQ(0, stats.count());
    EXPECT_EQ(0, stats.total());
    stats.update(17);
    EXPECT_EQ(17, stats.max());
    EXPECT_EQ(17, stats.min());
    EXPECT_EQ(1, stats.count());
    EXPECT_EQ(17, stats.total());
    stats.update(20);
    EXPECT_EQ(20, stats.max());
    EXPECT_EQ(17, stats.min());
    EXPECT_EQ(2, stats.count());
    EXPECT_EQ(37, stats.total());
    stats.update(15);
    EXPECT_EQ(20, stats.max());
    EXPECT_EQ(15, stats.min());
    EXPECT_EQ(3, stats.count());
    EXPECT_EQ(52, stats.total());
    stats.update(12);
    EXPECT_EQ(20, stats.max());
    EXPECT_EQ(12, stats.min());
    EXPECT_EQ(4, stats.count());
    EXPECT_EQ(64, stats.total());

    batt::MetricRegistry& registry = ::batt::global_metric_registry();
    registry.add("test_ping_stats", stats, batt::MetricLabelSet{label});
    auto on_test_exit = batt::finally([&] {
        registry.remove(stats);
    });

    const std::string header(
        "id,time_usec,date_time,test_ping_stats_count_job_batt,test_ping_stats_max_job_batt,"
        "test_ping_stats_min_job_batt,test_ping_stats_total_job_batt\n");

    std::ostringstream oss;
    batt::MetricCsvFormatter csv;
    csv.initialize(registry, oss);
    EXPECT_THAT(oss.str(), testing::StrEq(header));

    csv.format_values(registry, oss);
    const auto& actual = oss.str();
    EXPECT_THAT(actual, testing::StartsWith(header + "1,"));
    EXPECT_THAT(actual, testing::EndsWith(/* skip time_usec,date_time timestamps */ ",4,20,12,64\n"));
}

}  // namespace
