//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_METRICS_PROMETHEUS_EXPORTER_HPP
#define BATTERIES_METRICS_PROMETHEUS_EXPORTER_HPP

#include <batteries/config.hpp>
//
#include <batteries/async/task.hpp>
#include <batteries/metrics/metric_collectors.hpp>
#include <batteries/metrics/metric_otel_formatter.hpp>
#include <batteries/metrics/metric_registry.hpp>
#include <batteries/status.hpp>

#include <batteries/asio/io_context.hpp>
#include <batteries/asio/ip_tcp.hpp>

#include <cstring>
#include <sstream>

namespace batt {

class PrometheusMetricExporter
{
   public:
    using Self = PrometheusMetricExporter;

    struct Metrics {
        CountMetric<u64> export_count{0};
    };

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    static Metrics& metrics()
    {
        static Metrics metrics_;
        static bool init_ [[maybe_unused]] = [&] {
            global_metric_registry().add("export_count", metrics_.export_count);
            return true;
        }();
        return metrics_;
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    PrometheusMetricExporter(const PrometheusMetricExporter&) = delete;
    PrometheusMetricExporter& operator=(const PrometheusMetricExporter&) = delete;

    explicit PrometheusMetricExporter(boost::asio::io_context& io, MetricRegistry& registry,
                                      const boost::asio::ip::tcp::endpoint& endpoint) noexcept
        : acceptor_{io.get_executor(), endpoint}
        , registry_{registry}
        , endpoint_{endpoint}
    {
    }

    explicit PrometheusMetricExporter(boost::asio::io_context& io,
                                      const boost::asio::ip::tcp::endpoint& endpoint) noexcept
        : PrometheusMetricExporter{io, global_metric_registry(), endpoint}
    {
    }

    explicit PrometheusMetricExporter(boost::asio::io_context& io, u16 port) noexcept
        : PrometheusMetricExporter{
              io, global_metric_registry(),
              boost::asio::ip::tcp::endpoint{boost::asio::ip::address_v4::from_string("0.0.0.0"), port}}
    {
    }

    void start()
    {
        if (!this->task_) {
            this->task_.emplace(this->acceptor_.get_executor(), [this] {
                Status status = this->run();
                BATT_VLOG(1) << BATT_INSPECT(status);
            });
        }
    }

    void halt()
    {
        boost::system::error_code ec;
        this->acceptor_.close(ec);

        BATT_VLOG(1) << BATT_INSPECT(batt::to_status(ec));
    }

    void join()
    {
        if (this->task_) {
            this->task_->join();
            this->task_ = None;
        }
    }

    batt::Status run()
    {
        boost::system::error_code ec;
        this->acceptor_.listen(boost::asio::ip::tcp::socket::max_listen_connections, ec);
        BATT_REQUIRE_OK(ec);

        for (;;) {
            auto socket =
                batt::Task::await<batt::IOResult<boost::asio::ip::tcp::socket>>([this](auto&& handler) {
                    this->acceptor_.async_accept(BATT_FORWARD(handler));
                });

            BATT_REQUIRE_OK(socket);

            Task::spawn(
                Task::current().get_executor(),
                [this, socket = std::move(*socket)]() mutable {
                    Status status = this->run_connection(std::move(socket));
                    BATT_VLOG(1) << BATT_INSPECT(status);
                },
                "metric exporter connection");
        }
    }

    batt::Status run_connection(boost::asio::ip::tcp::socket socket)
    {
        auto on_scope_exit = batt::finally([&] {
            boost::system::error_code ec;
            socket.close(ec);
            BATT_VLOG(1) << "PrometheusMetricExporter socket closed:" << BATT_INSPECT(ec);
        });

        Self::metrics().export_count.add(1);

        std::ostringstream body_out;

        this->formatter_.format_values(this->registry_, body_out);

        std::string body_str = std::move(body_out).str();

        std::ostringstream header_out;

        header_out << "HTTP/1.1 200 OK\r\n"
                   << "Content-Type: text/plain; version=0.0.4; charset=utf-8\r\n"
                   << "Content-Length: " << body_str.size() << "\r\n"
                   << "Connection: close\r\n"
                   << "\r\n";

        std::string header_str = std::move(header_out).str();

        IOResult<usize> result = Task::await_write(
            socket, std::array<ConstBuffer, 2>{ConstBuffer{header_str.data(), header_str.size()},
                                               ConstBuffer{body_str.data(), body_str.size()}});

        BATT_VLOG(1) << "Metrics written;" << BATT_INSPECT(result);

        std::array<char, 512> buffer;
        auto mbuffer = MutableBuffer{buffer.data(), buffer.size()};
        usize extra = 0;
        for (;;) {
            IOResult<usize> n_read = Task::await_read_some(socket, mbuffer);
            BATT_REQUIRE_OK(n_read);

            auto str = std::string_view{buffer.data(), *n_read + extra};

            const auto CRLF_CRLF = std::string_view{"\r\n\r\n"};
            const auto LF_LF = std::string_view{"\n\n"};

            if (str.find(CRLF_CRLF) != std::string_view::npos || str.find(LF_LF) != std::string_view::npos) {
                break;
            }

            extra = std::min<usize>(3, str.size());
            std::memmove(buffer.data(), buffer.data() + (str.size() - extra), extra);
            mbuffer = MutableBuffer{buffer.data() + extra, buffer.size() - extra};
        }

        return OkStatus();
    }

   private:
    boost::asio::ip::tcp::acceptor acceptor_;
    MetricRegistry& registry_;
    boost::asio::ip::tcp::endpoint endpoint_;
    MetricOpenTelemetryFormatter formatter_;
    batt::Optional<batt::Task> task_;
};

}  //namespace batt

#endif  // BATTERIES_METRICS_PROMETHEUS_EXPORTER_HPP
