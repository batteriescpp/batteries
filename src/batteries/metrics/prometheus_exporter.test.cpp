//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#include <batteries/metrics/prometheus_exporter.hpp>
//
#include <batteries/metrics/prometheus_exporter.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <batteries/env.hpp>

namespace {

TEST(MetricsPrometheusExporterTest, Test)
{
    bool interactive = batt::getenv_as<bool>("BATT_INTERACTIVE_METRIC_EXPORT_TEST").value_or(false);

    if (interactive) {
        boost::asio::io_context io;

        batt::PrometheusMetricExporter exporter{io, /*port=*/29900};

        exporter.start();
        io.run();

        exporter.halt();
        exporter.join();
    }
}

}  // namespace
