//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2024 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_MATH_HPP
#define BATTERIES_MATH_HPP

#include <batteries/config.hpp>
//
#include <batteries/int_types.hpp>

#include <type_traits>

#ifdef _MSC_VER
#include <intrin.h>
#define __builtin_clzll _lzcnt_u64
#define __builtin_ctzll _tzcnt_u64
#define __builtin_popcountll __popcnt64
#endif

namespace batt {

/** \brief Returns log_2(i), rounded down to the nearest integer.
 */
inline constexpr i32 log2_ceil(u64 i)
{
    return (i <= 1) ? 0 : (64 - (i32)__builtin_clzll(i - 1));
}

/** \brief Returns log_2(i), rounded down to the nearest integer.
 */
inline constexpr i32 log2_floor(u64 i)
{
    return (i == 0) ? 0 : (63 - (i32)__builtin_clzll(i));
}

/** \brief Least Significant Bits Mask.
 *
 * Returns a value of type IntT with `bits` ones as the least significant bits, and zeros for all other bits.
 */
template <typename IntT>
inline constexpr IntT lsb_mask(i32 bits)
{
    return ((IntT{1} << bits) - 1);
}

/** \brief Rounds an integer value down to the nearest multiple of 2^bits.
 *
 * For example, round_down_bits(8, n) will round n down to the nearest multiple of 256; if n is already a
 * multiple of 256, it will return n.
 */
template <typename IntT>
inline constexpr IntT round_down_bits(i32 bits, IntT n)
{
    return n & ~lsb_mask<IntT>(bits);
}

/** \brief Rounds an integer value up to the nearest multiple of 2^bits.
 *
 * For example, round_up_bits(8, n) will round n up to the nearest multiple of 256; if n is already a multiple
 * of 256, it will return n.
 */
template <typename IntT>
inline constexpr IntT round_up_bits(i32 bits, IntT n)
{
    return round_down_bits(bits, n + lsb_mask<IntT>(bits));
}

/** \brief Rounds `n` down to the nearest multiple of `unit`.
 */
template <typename IntT>
inline constexpr IntT round_down_to(IntT unit, IntT n)
{
    return n - (n % unit);
}

/** \brief Rounds `n` up to the nearest multiple of `unit`.
 */
template <typename IntT>
inline constexpr IntT round_up_to(IntT unit, IntT n)
{
    return round_down_to(unit, n + (unit - 1));
}

/** \brief Compile-time integer exponentiation.
 */
template <typename IntT>
inline constexpr IntT ipow(IntT base, IntT exponent, IntT accumulator = static_cast<IntT>(1))
{
    static_assert(std::is_integral_v<IntT>, "batt::ipow may only be used with integral types.");
    return (exponent > 0) ? ipow(base, exponent - 1, accumulator * base) : accumulator;
}

/** \brief Returns the number of set (1) bits in the passed integer.
 */
inline i32 bit_count(u64 n)
{
    return __builtin_popcountll(n);
}

}  // namespace batt

#endif  // BATTERIES_MATH_HPP
