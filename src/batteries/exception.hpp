//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2024 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_EXCEPTION_HPP
#define BATTERIES_EXCEPTION_HPP

#include <batteries/config.hpp>
//
#include <batteries/hint.hpp>
#include <batteries/status.hpp>
#include <batteries/stream_util.hpp>

#include <sstream>
#include <stdexcept>
#include <string_view>

namespace batt {

/** \brief Exception wrapper for a batt::Status value.
 *
 * Use this class to adapt code that uses Batteries exception-free style to a containing context of code that
 * uses exceptions to report errors.
 *
 * \see BATT_THROW_IF_NOT_OK, BATT_OK_RESULT_OR_THROW
 */
class StatusException : public std::runtime_error
{
   public:
    /** \brief Construct a StatusException with the given status code and optional message.
     *
     * The `what()` message returned by this exception will contain the string form of the status code, as
     * well as any optional message provided here.
     */
    explicit StatusException(Status status, Optional<std::string_view>&& optional_message = None) noexcept;

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    /** \brief Returns the status code wrapped by this exception.
     */
    Status status() const noexcept;

    //+++++++++++-+-+--+----- --- -- -  -  -   -
   private:
    Status status_;
};

namespace detail {

/** \brief Temporary object that collects any optional message string parts, throwing a StatusException when
 * destructed (unless this object has been moved).
 */
class ThrowWhenDestroyed
{
   public:
    /** \brief Constructs an empty object that will not throw on destruction, unless it is assigned a new
     * value.
     */
    explicit ThrowWhenDestroyed() noexcept;

    /** \brief Constructs an object that will throw a StatusException wrapping the passed `status` value on
     * destruction.
     */
    explicit ThrowWhenDestroyed(Status status) noexcept;

    /** \brief ThrowWhenDestroyed is not copyable.
     */
    ThrowWhenDestroyed(const ThrowWhenDestroyed&) = delete;

    /** \brief ThrowWhenDestroyed is not copyable.
     */
    ThrowWhenDestroyed& operator=(const ThrowWhenDestroyed&) = delete;

    /** \brief ThrowWhenDestroyed is a move-only type.
     *
     * Calls `that.cancel()` after moving its status and message string to this
     */
    ThrowWhenDestroyed(ThrowWhenDestroyed&& that) noexcept;

    /** \brief ThrowWhenDestroyed is a move-only type.
     *
     * Calls `that.cancel()` after moving its status and message string to this
     */
    ThrowWhenDestroyed& operator=(ThrowWhenDestroyed&& that) noexcept;

    /** \brief Throws a StatusException if `this->has_status()` is true, otherwise silently destructs this
     * object.
     */
    ~ThrowWhenDestroyed() noexcept(false);

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    /** \brief Returns true iff this object contains a Status code.
     */
    bool has_status() const noexcept;

    /** \brief Returns the wrapped Status value, if `this->has_status()` is true; otherwise behavior is
     * undefined.
     */
    Status status() const noexcept;

    /** \brief Clears the wrapped status value and message string, if present, so that `this->has_status()`
     * will return false.
     */
    void cancel() noexcept;

    /** \brief Lazily constructs the optional message std::ostringstream for this object.
     */
    std::ostream& message_out() noexcept;

    /** \brief Allows this object to act like a std::ostream; anything written to this object via this method
     * will be appended to the optional message string that will be included in the StatusException.
     */
    template <typename T>
    ThrowWhenDestroyed& operator<<(T&& value) noexcept;

    /** \brief Allows this object to act like a std::ostream; anything written to this object via this method
     * will be appended to the optional message string that will be included in the StatusException.
     */
    ThrowWhenDestroyed& operator<<(std::ostream& (&fn)(std::ostream&)) noexcept;

    //+++++++++++-+-+--+----- --- -- -  -  -   -
   private:
    Optional<Status> status_;
    Optional<std::ostringstream> message_stream_;
};

}  //namespace detail

/** \brief Expands to a statement which throws a batt::StatusException if the passed expression evaluates to a
 * non-ok batt::StatusOr<T> value.
 *
 * This macro allows additional information to appended to the message of the exception via ostream-style
 * insertion:
 *
 * ```c++
 * int arg = 7;
 * batt::Status status = something_that_might_fail(arg);
 * BATT_THROW_IF_NOT_OK(status) << "Turns out it did fail!" << BATT_INSPECT(arg);
 * ```
 */
#define BATT_THROW_IF_NOT_OK(status_expr)                                                                    \
    for (auto&& BOOST_PP_CAT(BATTERIES_temp_status_result_, __LINE__) = (status_expr);                       \
         !::batt::is_ok_status(BOOST_PP_CAT(BATTERIES_temp_status_result_, __LINE__));)                      \
        ::batt::detail::ThrowWhenDestroyed                                                                   \
        {                                                                                                    \
            ::batt::to_status(BATT_FORWARD(BOOST_PP_CAT(BATTERIES_temp_status_result_, __LINE__)))           \
        }

/** \brief Expands to an expression which throws a StatusException if the passed expression evaluates to a
 * non-ok batt::StatusOr<T> value, and otherwise evaluates to the `T` value contained within the
 * batt::StatusOr.
 *
 * Example:
 *
 * ```c++
 * batt::StatusOr<std::string> part1 = get_part1();
 * batt::StatusOr<std::string> part2 = get_part2();
 *
 * // This line will throw if either part1 or part2 is not ok; otherwise it concatenates the strings.
 * //
 * std::string both_parts = BATT_OK_RESULT_OR_THROW(part1) + BATT_OK_RESULT_OR_THROW(part2);
 * ```
 */
#define BATT_OK_RESULT_OR_THROW(statusor_expr)                                                               \
    [&](auto&& statusor_value) {                                                                             \
        BATT_THROW_IF_NOT_OK(statusor_value)                                                                 \
            << "(" << BOOST_PP_STRINGIZE(statusor_expr) << ").ok() == false";                                \
        return std::move(*BATT_FORWARD(statusor_value));                                                     \
    }((statusor_expr))

//#=##=##=#==#=#==#===#+==#+==========+==+=+=+=+=+=++=+++=+++++=-++++=-+++++++++++

namespace detail {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
inline ThrowWhenDestroyed& ThrowWhenDestroyed::operator<<(T&& value) noexcept
{
    if (this->status_) {
        this->message_out() << BATT_FORWARD(value);
    }
    return *this;
}

}  //namespace detail
}  //namespace batt

#if BATT_HEADER_ONLY
#include <batteries/exception_impl.hpp>
#endif

#endif  // BATTERIES_EXCEPTION_HPP
