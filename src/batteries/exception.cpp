//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2024 Anthony Paul Astolfi
//
#include <batteries/config.hpp>

#if !BATT_HEADER_ONLY

#include <batteries/exception.hpp>
#include <batteries/exception_impl.hpp>

#endif  // !BATT_HEADER_ONLY
