// Copyright 2021-2024 Anthony Paul Astolfi
//
#pragma once

#include <batteries/config.hpp>

#include <boost/preprocessor/comparison/greater_equal.hpp>
#include <boost/preprocessor/control/if.hpp>

#ifdef BATT_GENERATING_DOCS

// =============================================================================
/// Disables specific compiler warnings (by id).  Nests with BATT_UNSUPPRESS().
///
/// Example:
/// ```
/// #include <batteries/strict.hpp>
/// #include <batteries/suppress.hpp>
///
/// // Ordinarily the function below would fail to compile because:
/// //  - foo() is declared as returning int, but never returns anything
/// //  - foo() is never used
/// //
///
/// BATT_SUPPRESS("-Wreturn-type")
/// BATT_SUPPRESS("-Wunused-function")
///
/// int foo()
/// {}
///
/// BATT_UNSUPPRESS()
/// BATT_UNSUPPRESS()
/// ```
///
#define BATT_SUPPRESS(warn_id)

/// \see [BATT_SUPPRESS](/reference/files/suppress_8hpp/#define-batt_suppress)
#define BATT_UNSUPPRESS()

#endif  // BATT_GENERATING_DOCS

// =============================================================================
/// Disables optimization for a single function.
///
/// Example:
/// ```
/// void BATT_NO_OPTIMIZE empty_function() {}
/// ```
///
#if BATT_COMPILER_IS_CLANG

#define BATT_NO_OPTIMIZE __attribute__((optnone))

#define BATT_GCC_BEGIN_OPTIMIZE(opt_level)
#define BATT_GCC_END_OPTIMIZE()

#define BATT_BEGIN_NO_OPTIMIZE()
#define BATT_END_NO_OPTIMIZE()

#elif BATT_COMPILER_IS_GCC

#define BATT_NO_OPTIMIZE __attribute__((optimize("O0")))

#define BATT_GCC_BEGIN_OPTIMIZE(opt_level) _Pragma("GCC push_options") BATT_DO_PRAGMA(GCC optimize(opt_level))
#define BATT_GCC_END_OPTIMIZE() _Pragma("GCC pop_options")

#define BATT_BEGIN_NO_OPTIMIZE() BATT_GCC_BEGIN_OPTIMIZE("O0")
#define BATT_END_NO_OPTIMIZE() BATT_GCC_END_OPTIMIZE()

#elif BATT_COMPILER_IS_MSVC

#define BATT_NO_OPTIMIZE

#define BATT_GCC_BEGIN_OPTIMIZE(opt_level)
#define BATT_GCC_END_OPTIMIZE()

#define BATT_BEGIN_NO_OPTIMIZE()
#define BATT_END_NO_OPTIMIZE()

#else

#error Please define BATT_NO_OPTIMIZE, etc. for your compiler

#endif

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------
#if BATT_COMPILER_IS_GCC

#define BATT_DO_PRAGMA(x) _Pragma(#x)

#define BATT_SUPPRESS_IF_GCC(warn_id)                                                                        \
    _Pragma("GCC diagnostic push") BATT_DO_PRAGMA(GCC diagnostic ignored warn_id)

#define BATT_UNSUPPRESS_IF_GCC() _Pragma("GCC diagnostic pop")

#define BATT_SUPPRESS_IF_GCC_VERSION(version, warn_id)                                                       \
    BOOST_PP_IF(BOOST_PP_GREATER_EQUAL(__GNUC__, version), /*then*/ BATT_SUPPRESS_IF_GCC,                    \
                /*else*/ BATT_EXPANDS_TO_NOTHING_A)                                                          \
    (warn_id)

#define BATT_UNSUPPRESS_IF_GCC_VERSION(version)                                                              \
    BOOST_PP_IF(BOOST_PP_GREATER_EQUAL(__GNUC__, version), /*then*/ BATT_UNSUPPRESS_IF_GCC,                  \
                /*else*/ BATT_EXPANDS_TO_NOTHING_A)                                                          \
    ()

#else  // BATT_COMPILER_IS_GCC

#define BATT_SUPPRESS_IF_GCC(warn_id)
#define BATT_UNSUPPRESS_IF_GCC()
#define BATT_SUPPRESS_IF_GCC_VERSION(version, warn_id)
#define BATT_UNSUPPRESS_IF_GCC_VERSION(version)

#endif  // BATT_COMPILER_IS_GCC

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------
#if BATT_COMPILER_IS_CLANG

#define BATT_DO_PRAGMA(x) _Pragma(#x)

#define BATT_SUPPRESS_IF_CLANG(warn_id)                                                                      \
    _Pragma("GCC diagnostic push") BATT_DO_PRAGMA(GCC diagnostic ignored warn_id)

#define BATT_UNSUPPRESS_IF_CLANG() _Pragma("GCC diagnostic pop")

#else  // BATT_COMPILER_IS_CLANG

#define BATT_SUPPRESS_IF_CLANG(warn_id)
#define BATT_UNSUPPRESS_IF_CLANG()

#endif  // BATT_COMPILER_IS_CLANG

#if BATT_COMPILER_IS_MSVC

#define BATT_SUPPRESS_IF_MSVC(warn_id) __pragma(warning(push)) __pragma(warning(disable : warn_id))

#define BATT_UNSUPPRESS_IF_MSVC() __pragma(warning(pop))

#else  // BATT_COMPILER_IS_MSVC

#define BATT_SUPPRESS_IF_MSVC(warn_id)
#define BATT_UNSUPPRESS_IF_MSVC()

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------

//----- --- -- -  -  -   -
#if BATT_COMPILER_IS_GCC
#define BATT_SUPPRESS BATT_SUPPRESS_IF_GCC
#define BATT_UNSUPPRESS BATT_UNSUPPRESS_IF_GCC

//----- --- -- -  -  -   -
#elif BATT_COMPILER_IS_CLANG
#define BATT_SUPPRESS BATT_SUPPRESS_IF_CLANG
#define BATT_UNSUPPRESS BATT_UNSUPPRESS_IF_CLANG

//----- --- -- -  -  -   -
#elif BATT_COMPILER_IS_MSVC
#define BATT_SUPPRESS BATT_SUPPRESS_IF_MSVC
#define BATT_UNSUPPRESS BATT_UNSUPPRESS_IF_MSVC

//----- --- -- -  -  -   -
#else
#error Please define BATT_SUPPRESS for your compiler!
#endif

#endif  // BATT_COMPILER_IS_MSVC
