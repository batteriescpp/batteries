//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2024 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_OPTIONAL_HPP
#define BATTERIES_OPTIONAL_HPP

#include <batteries/config.hpp>
//
#include <batteries/assert.hpp>
#include <batteries/hint.hpp>
#include <batteries/strong_typedef.hpp>
#include <batteries/suppress.hpp>
#include <batteries/type_traits.hpp>

#include <optional>

#ifdef BATT_USE_BOOST_OPTIONAL

#include <boost/optional.hpp>
#include <boost/optional/optional_io.hpp>

namespace batt {

template <typename T>
using Optional = boost::optional<T>;

namespace {
decltype(auto) None = boost::none;
decltype(auto) InPlaceInit = boost::in_place_init;
}  // namespace

using NoneType = std::decay_t<decltype(boost::none)>;

template <typename... Args>
auto make_optional(Args&&... args)
{
    return boost::make_optional(std::forward<Args>(args)...);
}

template <typename T, typename U, typename = decltype(std::declval<const T&>() == std::declval<const U&>())>
inline bool operator==(const Optional<T>& l, const Optional<U>& r)
{
    return (l && r && *l == *r) || (!l && !r);
}

}  // namespace batt

#else

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------
// Custom optional implementation.
//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------

#include <batteries/type_traits.hpp>
#include <batteries/utility.hpp>

#include <type_traits>

namespace batt {

struct NoneType {
};

inline bool operator==(const NoneType&, const NoneType&)
{
    return true;
}

inline bool operator!=(const NoneType&, const NoneType&)
{
    return false;
}

struct InPlaceInitType {
};

BATT_SUPPRESS_IF_GCC("-Wunused-variable")

namespace {
NoneType None;
InPlaceInitType InPlaceInit;
}  // namespace

BATT_UNSUPPRESS_IF_GCC()

namespace detail {

template <typename T>
class OptionalBase
{
   protected:
    void* storage_address() noexcept
    {
        return &this->storage_;
    }

    const void* storage_address() const noexcept
    {
        return &this->storage_;
    }

   private:
    std::aligned_storage_t<sizeof(T), alignof(T)> storage_;
};

template <typename T>
class OptionalEmptyBase
{
   protected:
    void* storage_address() noexcept
    {
        return this;
    }

    const void* storage_address() const noexcept
    {
        return this;
    }
};

}  //namespace detail

template <typename T>
class Optional
    : private std::conditional_t<        //
          can_be_empty_base<T>(),        //
          detail::OptionalEmptyBase<T>,  //
          detail::OptionalBase<T>        //
          >
{
   public:
    static_assert(std::is_same_v<T, std::decay_t<T>>,
                  "Optional<T&> is (partially) explicitly specialized below");

    Optional() noexcept : valid_{false}
    {
    }

    Optional(NoneType) noexcept : valid_{false}
    {
    }

    template <typename... Args, typename = std::enable_if_t<std::is_constructible_v<T, Args&&...>>>
    Optional(InPlaceInitType, Args&&... args) noexcept : valid_{false}
    {
        new (this->storage_address()) T(BATT_FORWARD(args)...);
        this->valid_ = true;
    }

    Optional(std::optional<T>&& init) noexcept : valid_{!!init}
    {
        if (this->valid_) {
            new (this->storage_address()) T(std::move(*init));
        }
    }

    template <typename U,
              typename = std::enable_if_t<std::is_convertible_v<U, T> && !std::is_same_v<T, Optional<U>>>,
              typename = batt::EnableIfNoShadow<Optional, U>>
    Optional(Optional<U>&& u) noexcept : valid_{false}
    {
        if (u) {
            new (this->storage_address()) T(std::move(*u));
            this->valid_ = true;
        }
    }

    template <typename U,
              typename = std::enable_if_t<std::is_convertible_v<U, T> && !std::is_same_v<T, Optional<U>>>,
              typename = batt::EnableIfNoShadow<Optional, U>>
    Optional(const Optional<U>& u) noexcept : valid_{false}
    {
        if (u) {
            new (this->storage_address()) T(*u);
            this->valid_ = true;
        }
    }

    template <typename... Args,
              typename = std::enable_if_t<std::is_constructible_v<T, Args&&...> &&
                                          (!IsStrongType<std::decay_t<T>>{} ||
                                           IsCallable<void (&)(T), Args&&...>{})>,
              typename = batt::EnableIfNoShadow<Optional, Args...>>
    Optional(Args&&... args) noexcept : valid_{false}
    {
        new (this->storage_address()) T(BATT_FORWARD(args)...);
        this->valid_ = true;
    }

    Optional(const T& val) noexcept : valid_{false}
    {
        new (this->storage_address()) T(val);
        this->valid_ = true;
    }

    ~Optional() noexcept
    {
        if (this->valid_) {
            this->obj().~T();
        }
    }

    Optional(Optional&& that) noexcept : valid_{false}
    {
        if (that.valid_) {
            new (this->storage_address()) T(std::move(that.obj()));
            valid_ = true;
        }
    }

    Optional(const Optional& that) noexcept : valid_{false}
    {
        if (that.valid_) {
            new (this->storage_address()) T(that.obj());
            valid_ = true;
        }
    }

    void reset() noexcept
    {
        if (this->valid_) {
            T& obj_ref = this->obj();
            this->valid_ = false;
            obj_ref.~T();
        }
    }

    Optional& operator=(Optional&& that) noexcept
    {
        if (BATT_HINT_TRUE(this != &that)) {
            this->reset();
            if (that.valid_) {
                new (this->storage_address()) T(std::move(that.obj()));
                this->valid_ = true;
            }
        }
        return *this;
    }

    Optional& operator=(const Optional& that) noexcept
    {
        if (BATT_HINT_TRUE(this != &that)) {
            this->reset();
            if (that.valid_) {
                new (this->storage_address()) T(that.obj());
                this->valid_ = true;
            }
        }
        return *this;
    }

    template <typename... Args>
    T& emplace(Args&&... args) noexcept
    {
        this->reset();
        new (this->storage_address()) T(BATT_FORWARD(args)...);
        this->valid_ = true;
        return this->obj();
    }

    template <typename U>
    T value_or(U&& else_val) const noexcept
    {
        if (this->valid_) {
            return this->obj();
        }
        return T(BATT_FORWARD(else_val));
    }

    template <typename Fn>
    T or_else(Fn&& fn) const noexcept(noexcept(std::declval<Fn>()()))
    {
        if (this->valid_) {
            return this->obj();
        }
        return BATT_FORWARD(fn)();
    }

    template <typename Fn, typename U = std::invoke_result_t<Fn, const T&>>
    Optional<U> map(Fn&& fn) const noexcept
    {
        if (this->valid_) {
            return BATT_FORWARD(fn)(this->obj());
        }
        return None;
    }

    template <typename Fn, typename OptionalU = std::invoke_result_t<Fn, const T&>>
    OptionalU flat_map(Fn&& fn) const noexcept
    {
        if (this->valid_) {
            return BATT_FORWARD(fn)(this->obj());
        }
        return None;
    }

    explicit operator bool() const noexcept
    {
        return this->valid_;
    }

    bool has_value() const noexcept
    {
        return this->valid_;
    }

    Optional& operator=(NoneType) noexcept
    {
        this->reset();
        return *this;
    }

    T& operator*() & noexcept
    {
        return this->obj();
    }

    const T& operator*() const& noexcept
    {
        return this->obj();
    }

    T operator*() && noexcept
    {
        return std::move(this->obj());
    }

    T operator*() const&& noexcept = delete;

    T* operator->() noexcept
    {
        return &this->obj();
    }

    const T* operator->() const noexcept
    {
        return &this->obj();
    }

    T* get_ptr() noexcept
    {
        return &this->obj();
    }

    const T* get_ptr() const noexcept
    {
        return &this->obj();
    }

   private:
    T& obj() noexcept
    {
        BATT_ASSERT(this->valid_);
        return *(T*)this->storage_address();
    }

    const T& obj() const noexcept
    {
        BATT_ASSERT(this->valid_);
        return *(const T*)this->storage_address();
    }

    bool valid_ = false;
};

template <typename T>
class Optional<T&>
{
   public:
    Optional() noexcept : Optional{None}
    {
    }

    Optional(NoneType) noexcept : ptr_{nullptr}
    {
    }

    template <typename... Args, typename = std::enable_if_t<std::is_constructible_v<T&, Args&&...>>>
    Optional(InPlaceInitType, Args&&... args) noexcept
        : Optional{[](T& ref) -> T& {
            return ref;
        }(BATT_FORWARD(args)...)}
    {
    }

    template <
        typename U,
        typename = std::enable_if_t<std::is_convertible_v<U*, T*> && !std::is_same_v<T, const Optional<U>>>,
        typename = batt::EnableIfNoShadow<Optional, U>>
    Optional(const Optional<U&>& u) noexcept : ptr_{u.ptr_}
    {
    }

    /*implicit*/ Optional(T& ref) noexcept : ptr_{&ref}
    {
    }

    Optional(const Optional&) = default;
    Optional& operator=(const Optional&) = default;

    ~Optional() noexcept
    {
        // nothing to do.
    }

    template <typename Fn, typename U = std::invoke_result_t<Fn, const T&>>
    Optional<U> map(Fn&& fn) const noexcept
    {
        if (this->ptr_) {
            return BATT_FORWARD(fn)(*this->ptr_);
        }
        return None;
    }

    template <typename Fn, typename OptionalU = std::invoke_result_t<Fn, const T&>>
    OptionalU flat_map(Fn&& fn) const noexcept
    {
        if (this->ptr_) {
            return BATT_FORWARD(fn)(*this->ptr_);
        }
        return None;
    }

    T& operator*() const noexcept
    {
        return *this->ptr_;
    }

    T* operator->() noexcept
    {
        return this->ptr_;
    }

    const T* operator->() const noexcept
    {
        return this->ptr_;
    }

    bool has_value() const noexcept
    {
        return this->ptr_ != nullptr;
    }

    explicit operator bool() const noexcept
    {
        return this->has_value();
    }

    bool operator!() const noexcept
    {
        return this->ptr_ == nullptr;
    }

    T& emplace(T& ref) noexcept
    {
        this->ptr_ = &ref;
        return *this->ptr_;
    }

    Optional& operator=(NoneType) noexcept
    {
        this->ptr_ = nullptr;
        return *this;
    }

    T& value_or(T& other) noexcept
    {
        if (this->ptr_) {
            return *this->ptr_;
        }
        return other;
    }

    template <typename Fn>
    T& or_else(Fn&& fn) const noexcept(noexcept(std::declval<Fn>()()))
    {
        if (this->ptr_) {
            return *this->ptr_;
        }
        return BATT_FORWARD(fn)();
    }

   private:
    T* ptr_ = nullptr;
};

template <typename T0, typename T1>
inline bool operator==(const Optional<T0>& v0, const Optional<T1>& v1)
{
    return (v0 && v1 && (*v0 == *v1)) || (!v0 && !v1);
}

template <typename T0, typename T1>
inline bool operator!=(const Optional<T0>& v0, const Optional<T1>& v1)
{
    return !(v0 == v1);
}

template <typename T0, typename T1>
inline bool operator==(const Optional<T0>& v0, const T1& v1)
{
    return v0 && (*v0 == v1);
}

template <typename T0, typename T1>
inline bool operator!=(const Optional<T0>& v0, const T1& v1)
{
    return !(v0 == v1);
}

template <typename T0, typename T1>
inline bool operator==(const T0& v0, const Optional<T1>& v1)
{
    return v1 && (v0 == *v1);
}

template <typename T0, typename T1>
inline bool operator!=(const T0& v0, const Optional<T1>& v1)
{
    return !(v0 == v1);
}

template <typename T>
inline bool operator==(NoneType, const Optional<T>& v)
{
    return !v;
}

template <typename T>
inline bool operator!=(NoneType, const Optional<T>& v)
{
    return !(None == v);
}

template <typename T>
inline bool operator==(const Optional<T>& v, NoneType)
{
    return !v;
}

template <typename T>
inline bool operator!=(const Optional<T>& v, NoneType)
{
    return !(v == None);
}

template <typename T>
inline std::ostream& operator<<(std::ostream& out, const Optional<T>& t)
{
    if (!t) {
        return out << "--";
    }
    return out << make_printable(*t);
}

inline std::ostream& operator<<(std::ostream& out, const NoneType&)
{
    return out << "--";
}

template <typename T>
Optional<std::decay_t<T>> make_optional(T&& val) noexcept
{
    return {BATT_FORWARD(val)};
}

}  // namespace batt

#endif

namespace batt {

template <typename T>
decltype(auto) get_or_panic(Optional<T>& opt)
{
    BATT_CHECK(opt);
    return *opt;
}

template <typename T>
decltype(auto) get_or_panic(const Optional<T>& opt)
{
    BATT_CHECK(opt);
    return *opt;
}

template <typename T>
decltype(auto) get_or_panic(Optional<T>&& opt)
{
    BATT_CHECK(opt);
    return std::move(*opt);
}

}  // namespace batt

#endif  // BATTERIES_OPTIONAL_HPP
