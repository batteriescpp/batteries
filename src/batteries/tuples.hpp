// Copyright 2021-2024 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_TUPLES_HPP
#define BATTERIES_TUPLES_HPP

#include <batteries/config.hpp>
//
#include <batteries/int_types.hpp>
#include <batteries/type_traits.hpp>

#include <array>
#include <tuple>
#include <type_traits>

namespace batt {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -

// Take the element types of a tuple and apply them as the args to some other
// template to produce a new type.
//
// Example:
// ```c++
// using MyTypes = std::tuple<int, char, std::string>;
// using Morphed = batt::MorphTuple_t<std::variant, MyTypes>;
//
// static_assert(std::is_same_v<Morphed, std::variant<int, char, std::string>>, "");
// ```
//
template <template <typename...> class TemplateT, typename TupleT>
struct MorphTuple;

template <template <typename...> class TemplateT, typename... Ts>
struct MorphTuple<TemplateT, std::tuple<Ts...>> : StaticType<TemplateT<Ts...>> {
};

template <template <typename...> class TemplateT, typename TupleT>
using MorphTuple_t = typename MorphTuple<TemplateT, std::decay_t<TupleT>>::type;

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -

template <typename TupleT, typename T>
struct TupleIndexOf;

template <typename T>
struct TupleIndexOf<std::tuple<>, T> : BATT_STATIC_VALUE(0u) {
};

template <typename T, typename... Rest>
struct TupleIndexOf<std::tuple<T, Rest...>, T> : BATT_STATIC_VALUE(0u) {
};

template <typename T, typename First, typename... Rest>
struct TupleIndexOf<std::tuple<First, Rest...>, T>
    : BATT_STATIC_VALUE((1u + TupleIndexOf<std::tuple<Rest...>, T>::value)) {
};

template <typename TupleT, typename T>
constexpr auto TupleIndexOf_v = TupleIndexOf<std::decay_t<TupleT>, T>::value;

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -

template <template <typename> class PerTypeT, typename TupleT>
struct MapTuple;

template <template <typename> class PerTypeT, typename... Ts>
struct MapTuple<PerTypeT, std::tuple<Ts...>> : StaticType<std::tuple<PerTypeT<Ts>...>> {
};

template <template <typename> class PerTypeT, typename TupleT>
using MapTuple_t = typename MapTuple<PerTypeT, std::decay_t<TupleT>>::type;

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -

/** \brief Stores a fixed-size set of Value objects, indexed by a set of bounded types (keys).
 *
 * Example:
 *
 * ```c++
 * batt::StaticTypeMap<std::tuple<char, int, bool>, std::string> m;
 *
 * m[batt::StaticType<char>{}] = "char";
 * m[batt::StaticType<int>{}] = "int";
 * m[batt::StaticType<bool>{}] = "bool";
 * ```
 *
 * When used as a range, only the values are iterated.
 */
template <typename KeyTupleT, typename Value>
class StaticTypeMap
{
   public:
    using Self = StaticTypeMap;

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    /** \brief Returns the number of items in the map.
     *
     * This is the same as the number of elements of the KeyTupleT used to instantiate this class template.
     */
    static constexpr usize size()
    {
        return std::tuple_size<KeyTupleT>::value;
    }

    using ValueArray = std::array<Value, Self::size()>;
    using iterator = typename ValueArray::iterator;
    using const_iterator = typename ValueArray::const_iterator;

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    /** \brief Constructs a new StaticTypeMap, value-initializing all items to Value{}.
     */
    StaticTypeMap() noexcept
    {
    }

    /** \brief Copy-constructs a StaticTypeMap instance.
     */
    StaticTypeMap(const StaticTypeMap&) = default;

    /** \brief Copy-assigns a StaticTypeMap instance.
     */
    StaticTypeMap& operator=(const StaticTypeMap&) = default;

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    /** \brief Returns a (non-const) reference to the value for the given key.
     */
    template <typename KeyT>
    Value& get([[maybe_unused]] StaticType<KeyT> key = {}) noexcept
    {
        constexpr usize kIndex = TupleIndexOf_v<KeyTupleT, KeyT>;
        static_assert(kIndex < Self::size());

        return this->values_[kIndex];
    }

    /** \brief Returns a (const) reference to the value for the given key.
     */
    template <typename KeyT>
    const Value& get([[maybe_unused]] StaticType<KeyT> key = {}) const noexcept
    {
        constexpr usize kIndex = TupleIndexOf_v<KeyTupleT, KeyT>;
        static_assert(kIndex < Self::size());

        return this->values_[kIndex];
    }

    /** \brief Returns a (non-const) reference to the value for the given key.
     */
    template <typename KeyT>
    Value& operator[](StaticType<KeyT> key) noexcept
    {
        return this->get(key);
    }

    /** \brief Returns a (const) reference to the value for the given key.
     */
    template <typename KeyT>
    const Value& operator[](StaticType<KeyT> key) const noexcept
    {
        return this->get(key);
    }

    /** \brief Returns an iterator to the first value in the map.
     */
    iterator begin() noexcept
    {
        return this->values_.begin();
    }

    /** \brief Returns an iterator to one past the last value in the map.
     */
    iterator end() noexcept
    {
        return this->values_.end();
    }

    /** \brief Returns an iterator (const) to the first value in the map.
     */
    const_iterator begin() const noexcept
    {
        return this->values_.begin();
    }

    /** \brief Returns an iterator (const) to one past the last value in the map.
     */
    const_iterator end() const noexcept
    {
        return this->values_.end();
    }

   private:
    // Stores all values in the map.
    //
    std::array<Value, Self::size()> values_{};
};

}  // namespace batt

#endif  // BATTERIES_TUPLES_HPP
