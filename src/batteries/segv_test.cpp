// Copyright 2021-2023 Anthony Paul Astolfi
//
#include <batteries/segv.hpp>
//
#include <batteries/segv.hpp>

#include <gtest/gtest.h>

#include <batteries/suppress.hpp>

namespace {

void BATT_NO_OPTIMIZE level_three()
{
    raise(SIGSEGV);
}

void BATT_NO_OPTIMIZE level_two()
{
    level_three();
}

void BATT_NO_OPTIMIZE level_one()
{
    level_two();
}

#if BATT_PLATFORM_SUPPORTS_DEATH_TESTS

TEST(Segv, StackTraceOnSegvDeath)
{
    EXPECT_DEATH(level_one(),
                 "Seg.*fault"
#ifdef BATT_PLATFORM_IS_APPLE
                 ".*level_three.*level_two.*level_one.*Segv_StackTraceOnSegvDeath_Test::TestBody()"
#endif  // BATT_PLATFORM_IS_APPLE
    );
}

#endif // BATT_PLATFORM_SUPPORTS_DEATH_TESTS

}  // namespace
