//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2024 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_INSTANCE_COUNTER_HPP
#define BATTERIES_INSTANCE_COUNTER_HPP

#include <batteries/config.hpp>
//
#include <batteries/int_types.hpp>

#include <atomic>

namespace batt {

/** \brief A quick and easy way to track the number of instances of a given type in existence; just add
 * batt::InstanceCounter<T> as a field or base class of T.
 */
template <typename T>
class InstanceCounter
{
   public:
    using Self = InstanceCounter;

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    static isize get() noexcept
    {
        return Self::instance().load();
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    InstanceCounter() noexcept
    {
        Self::instance().fetch_add(1);
    }

    ~InstanceCounter() noexcept
    {
        Self::instance().fetch_sub(1);
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -
   private:
    static std::atomic<isize>& instance() noexcept
    {
        static std::atomic<isize> instance_{0};
        return instance_;
    }
};

}  //namespace batt

#endif  // BATTERIES_INSTANCE_COUNTER_HPP
