//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_SYSCALL_RETRY_HPP
#define BATTERIES_SYSCALL_RETRY_HPP

#include <batteries/config.hpp>
//

#ifndef BATT_PLATFORM_IS_WINDOWS
#include <unistd.h>
#endif

namespace batt {

// Executes the passed op repeatedly it doesn't fail with EINTR.
//
template <typename Op>
auto syscall_retry(Op&& op)
{
    for (;;) {
        const auto result = op();
#ifndef BATT_PLATFORM_IS_WINDOWS
        if (result != -1 || errno != EINTR)
#endif  // BATT_PLATFORM_IS_WINDOWS
        {
            return result;
        }
    }
}

}  // namespace batt

#endif  // BATTERIES_SYSCALL_RETRY_HPP
