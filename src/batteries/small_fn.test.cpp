// Copyright 2021-2024 Anthony Paul Astolfi
//
#include <batteries/small_fn.hpp>
//
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <batteries/int_types.hpp>
#include <batteries/small_fn.hpp>

#include <deque>
#include <memory>
#include <numeric>

namespace {

using namespace batt::int_types;
using batt::SmallFn;
using batt::UniqueSmallFn;

TEST(SmallFnTest, CopyAndInvoke)
{
    SmallFn<auto(int)->int> fn = [](int i) {
        return i + 1;
    };

    ASSERT_TRUE(fn);
    ASSERT_FALSE(!fn);
    EXPECT_EQ(2, fn(1));

    SmallFn<auto(int)->int> fn2 = fn;

    ASSERT_TRUE(fn);
    ASSERT_TRUE(fn2);
    ASSERT_FALSE(!fn);
    ASSERT_FALSE(!fn2);
    EXPECT_EQ(2, fn(1));
    EXPECT_EQ(2, fn2(1));

    SmallFn<auto(int)->int> fn3;

    ASSERT_TRUE(!fn3);
    ASSERT_FALSE(fn3);

    fn3 = fn2;

    ASSERT_TRUE(fn2);
    ASSERT_TRUE(fn3);
    ASSERT_FALSE(!fn2);
    ASSERT_FALSE(!fn3);
    EXPECT_EQ(2, fn2(1));
    EXPECT_EQ(2, fn3(1));

    fn2 = [](int i) {
        return i + 2;
    };
    fn3 = std::move(fn2);

    ASSERT_FALSE(fn2);
    ASSERT_TRUE(fn3);
    EXPECT_EQ(3, fn3(1));
}

TEST(UniqueSmallFnTest, MoveAndInvoke)
{
    UniqueSmallFn<auto(int)->int> fn = [delta = std::make_unique<int>(3)](int i) {
        return i + *delta;
    };

    ASSERT_TRUE(fn);
    ASSERT_FALSE(!fn);
    EXPECT_EQ(4, fn(1));

    UniqueSmallFn<auto(int)->int> fn2 = std::move(fn);

    ASSERT_TRUE(fn2);
    ASSERT_FALSE(fn);

    SmallFn<auto(int)->int> fn3 = [](int i) {
        return i + i + 1;
    };
    const SmallFn<auto(int)->int> fn4 = [](int i) {
        return i + i + 1;
    };

    fn2 = fn3;

    ASSERT_TRUE(fn2);
    ASSERT_TRUE(fn3);
    EXPECT_EQ(5, fn2(2));

    fn2 = fn4;

    ASSERT_TRUE(fn2);
    ASSERT_TRUE(fn4);
    EXPECT_EQ(7, fn2(3));

    UniqueSmallFn<auto(int)->int> fn5 = fn3;

    ASSERT_TRUE(fn5);
    ASSERT_TRUE(fn3);
    EXPECT_EQ(9, fn5(4));

    UniqueSmallFn<auto(int)->int> fn6 = fn4;

    ASSERT_TRUE(fn6);
    ASSERT_TRUE(fn4);
    EXPECT_EQ(11, fn5(5));
}

TEST(UniqueSmallFnTest, PushToCollection)
{
    int called = 0;
    std::deque<UniqueSmallFn<void(), 128 - 16>> queue;

    for (usize i = 0; i < 10; ++i) {
        UniqueSmallFn<void(), 128 - 16> fn = [&called] {
            ++called;
        };
        queue.emplace_back(std::move(fn));
    }

    for (auto& f : queue) {
        f();
    }
    EXPECT_EQ(called, 10);
}

TEST(SmallFnTest, AllowAllocCopyable)
{
    SmallFn<auto(int)->int, /*max_size=*/24, /*move_only=*/false, /*allow_alloc=*/true> fn;

    EXPECT_FALSE(fn);

    std::array<int, 200> lookup;
    std::iota(lookup.begin(), lookup.end(), 49);

    auto raw_fn = [lookup](int i) -> int {
        if (i < 0) {
            i = 0;
        }
        if (i >= (int)lookup.size()) {
            i = lookup.size() - 1;
        }
        return lookup[i];
    };

    EXPECT_GT(sizeof(raw_fn), sizeof(fn));

    fn = raw_fn;

    EXPECT_TRUE(fn);

    {
        auto fn2 = fn;

        EXPECT_TRUE(fn);
        EXPECT_TRUE(fn2);
        EXPECT_EQ(fn2(-1), 49);
        EXPECT_EQ(fn2(0), 49);
        EXPECT_EQ(fn2(1), 50);
        EXPECT_EQ(fn2(199), 248);
        EXPECT_EQ(fn2(200), 248);

        auto fn3 = std::move(fn2);

        EXPECT_FALSE(fn2);
        EXPECT_TRUE(fn3);
        EXPECT_EQ(fn3(-1), 49);
        EXPECT_EQ(fn3(0), 49);
        EXPECT_EQ(fn3(1), 50);
        EXPECT_EQ(fn3(199), 248);
        EXPECT_EQ(fn3(200), 248);
    }

    EXPECT_EQ(fn(-1), 49);
    EXPECT_EQ(fn(0), 49);
    EXPECT_EQ(fn(1), 50);
    EXPECT_EQ(fn(199), 248);
    EXPECT_EQ(fn(200), 248);

    auto raw_fn2 = [](int i) -> int {
        return -i;
    };

    EXPECT_LT(sizeof(raw_fn2) + sizeof(void*), sizeof(fn));

    fn = raw_fn2;

    EXPECT_EQ(fn(-1), 1);
    EXPECT_EQ(fn(0), 0);
    EXPECT_EQ(fn(1), -1);
    EXPECT_EQ(fn(199), -199);
    EXPECT_EQ(fn(200), -200);
}

TEST(SmallFnTest, AllowAllocMoveOnly)
{
    SmallFn<auto(int)->int, /*max_size=*/24, /*move_only=*/true, /*allow_alloc=*/true> fn;

    EXPECT_FALSE(fn);

    std::array<int, 200> lookup;
    std::iota(lookup.begin(), lookup.end(), 49);

    auto raw_fn = [lookup, ptr = std::make_unique<int>(2)](int i) -> int {
        if (i < 0) {
            i = 0;
        }
        if (i >= (int)lookup.size()) {
            i = lookup.size() - 1;
        }
        return lookup[i] + *ptr;
    };

    EXPECT_GT(sizeof(raw_fn), sizeof(fn));

    fn = std::move(raw_fn);

    EXPECT_TRUE(fn);
    EXPECT_EQ(fn(-1), 51);
    EXPECT_EQ(fn(0), 51);
    EXPECT_EQ(fn(1), 52);
    EXPECT_EQ(fn(199), 250);
    EXPECT_EQ(fn(200), 250);

    {
        auto fn2 = std::move(fn);

        EXPECT_FALSE(fn);
        EXPECT_TRUE(fn2);
        EXPECT_EQ(fn2(-1), 51);
        EXPECT_EQ(fn2(0), 51);
        EXPECT_EQ(fn2(1), 52);
        EXPECT_EQ(fn2(199), 250);
        EXPECT_EQ(fn2(200), 250);
    }

    auto raw_fn2 = [ptr = std::make_unique<int>(-2)](int i) -> int {
        return *ptr * i;
    };

    EXPECT_LT(sizeof(raw_fn2) + sizeof(void*), sizeof(fn));

    fn = std::move(raw_fn2);

    EXPECT_EQ(fn(-1), 2);
    EXPECT_EQ(fn(0), 0);
    EXPECT_EQ(fn(1), -2);
    EXPECT_EQ(fn(199), -398);
    EXPECT_EQ(fn(200), -400);
}

}  // namespace
