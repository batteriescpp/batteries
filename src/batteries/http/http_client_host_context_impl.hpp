//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2022-2024 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_HTTP_HTTP_CLIENT_HOST_CONTEXT_IMPL_HPP
#define BATTERIES_HTTP_HTTP_CLIENT_HOST_CONTEXT_IMPL_HPP

#include <batteries/config.hpp>
//
#include <batteries/http/http_client.hpp>
#include <batteries/http/http_client_connection_decl.hpp>
#include <batteries/http/http_client_host_context_decl.hpp>

#include <batteries/http/http_client_connection.ipp>

#if BATT_HEADER_ONLY
#include <batteries/http/http_client_impl.hpp>
#endif

namespace batt {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*explicit*/ HttpClientHostContext::HttpClientHostContext(
    HttpClient& client, const HostAddress& host_addr) noexcept
    : client_{client}
    , host_address_{host_addr}
    , request_queue_{}
    , active_connections_{0}
    , max_connections_{this->client_.get_default_max_connections_per_host()}
    , connection_timeout_ms_{this->client_.get_default_connection_timeout_ms()}
    , task_{this->client_.get_io_context().get_executor(),
            [this] {
                this->host_task_main();
            },
            "HttpClientHostContext::host_task_main"}
{
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL boost::asio::io_context& HttpClientHostContext::get_io_context() noexcept
{
    return this->client_.get_io_context();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
std::function<void(Optional<boost::asio::ssl::context>&, const HostAddress&)>
HttpClientHostContext::get_ssl_init_fn() const noexcept
{
    return this->client_.get_ssl_init_fn();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL Status HttpClientHostContext::submit_request(Pin<HttpRequest>&& request,
                                                              Pin<HttpResponse>&& response) noexcept
{
    BATT_VLOG(1) << "Request submitted...";
    return this->request_queue_.push(std::make_tuple(std::move(request), std::move(response)));
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void HttpClientHostContext::halt()
{
    this->request_queue_.close();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void HttpClientHostContext::join()
{
    this->task_.join();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL bool HttpClientHostContext::can_grow() const
{
    return this->active_connections_.get_value() < this->max_connections_.load();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL const HostAddress& HttpClientHostContext::host_address() const noexcept
{
    return this->host_address_;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL StatusOr<std::tuple<Pin<HttpRequest>, Pin<HttpResponse>>>
HttpClientHostContext::await_next_request() noexcept
{
    BATT_VLOG(1) << "Awaiting next request (client host context)";
    auto on_scope_exit = batt::finally([&] {
        BATT_VLOG(1) << "Request submitted & claimed by connection";
    });
    return this->request_queue_.await_next();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void HttpClientHostContext::host_task_main()
{
    Status status = [&]() -> Status {
        std::atomic<bool> done{false};
        boost::asio::deadline_timer heartbeat_timer{this->get_io_context()};

        // Launch a task to repeatedly wait for a configured interval and then poke the request queue to wake
        // any tasks waiting for work to do.
        //
        Task heartbeat_task{
            //
            this->get_io_context().get_executor(),
            [&done, &heartbeat_timer, this] {
                while (!done.load()) {
                    this->request_queue_.poke();
                    ErrorCode ec = Task::await<ErrorCode>([&](auto&& handler) {
                        const auto timeout_ms = (this->connection_timeout_ms_.load() + 3) / 4;

                        BATT_VLOG(1) << "Sleeping for " << timeout_ms << "ms";

                        ErrorCode ec;
                        heartbeat_timer.expires_from_now(boost::posix_time::milliseconds(timeout_ms), ec);
                        if (ec) {
                            BATT_FORWARD(handler)(ec);
                        } else {
                            heartbeat_timer.async_wait(BATT_FORWARD(handler));
                        }
                    });

                    // Cancelling the timer also means we are done.
                    //
                    if (ec) {
                        break;
                    }
                }
            },
            "HttpClientHostContext::heartbeat_task"};

        auto on_scope_exit = batt::finally([&] {
            done.store(true);
            {
                boost::system::error_code ec;
                heartbeat_timer.cancel(ec);
            }
            this->request_queue_.close();
            heartbeat_task.join();
        });

        for (;;) {
            const usize queue_depth = this->request_queue_.size();
            if (queue_depth > 0 && this->can_grow()) {
                this->create_connection();
            }

            BATT_DEBUG_INFO("HttpClientHostContext::host_task_main waiting for queue depth to change;"
                            << BATT_INSPECT(queue_depth));

            StatusOr<i64> new_queue_depth = this->request_queue_.await_size_is_truly([&](i64 size) {
                return size != BATT_CHECKED_CAST(i64, queue_depth);
            });
            if (new_queue_depth.status() == StatusCode::kPoke) {
                continue;
            }
            BATT_REQUIRE_OK(new_queue_depth);
        }
    }();

    status.IgnoreError();  // TODO [tastolfi 2022-03-17] do something better!
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void HttpClientHostContext::create_connection()
{
    if (this->host_address_.scheme == "http") {
        HttpClientConnection::spawn(*this, this->active_connections_);
    } else if (this->host_address_.scheme == "https") {
        HttpsClientConnection::spawn(*this, this->active_connections_);
    }
}

}  // namespace batt

#endif  // BATTERIES_HTTP_HTTP_CLIENT_HOST_CONTEXT_IMPL_HPP
