//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2024 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_HTTP_HTTP_SERVER_DECL_HPP
#define BATTERIES_HTTP_HTTP_SERVER_DECL_HPP

#include <batteries/config.hpp>
//
#include <batteries/http/host_address.hpp>
#include <batteries/http/http_request.hpp>
#include <batteries/http/http_response.hpp>
#include <batteries/http/http_server_settings.hpp>

#include <batteries/async/grant.hpp>
#include <batteries/async/task.hpp>
#include <batteries/async/watch.hpp>

#include <batteries/small_fn.hpp>
#include <batteries/status.hpp>

#include <boost/asio/io_context.hpp>

#include <memory>

namespace batt {

/** \brief One request dispatcher is created per accepted connection; it is reused in the context of that
 * connection until the connection is closed.
 */
using HttpRequestDispatcherFn = SmallFn<Status(HttpRequest& request, HttpResponse& response)>;

/** \brief Called once per accepted connection.
 */
using HttpRequestDispatcherFactoryFn = SmallFn<StatusOr<HttpRequestDispatcherFn>()>;

/** \brief An HTTP server.  Currently only supports non-encrypted TCP/IP transport.
 */
class HttpServer
{
   public:
    explicit HttpServer(boost::asio::io_context& io, HostAddress&& host_address,
                        HttpRequestDispatcherFactoryFn&& dispatcher_factory,
                        const Optional<HttpServerSettings>& settings = None) noexcept;

    ~HttpServer() noexcept;

    boost::asio::io_context& get_io_context() const noexcept
    {
        return this->io_;
    }

    void halt();

    void join();

    Status get_final_status() const;

    //+++++++++++-+-+--+----- --- -- -  -  -   -
   private:
    void acceptor_task_main();

    void connection_task_main(boost::asio::ip::tcp::socket&& socket);

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    boost::asio::io_context& io_;

    HostAddress host_address_;

    HttpRequestDispatcherFactoryFn dispatcher_factory_;

    HttpServerSettings settings_;

    boost::asio::ip::tcp::acceptor acceptor_;

    Watch<bool> halt_requested_{false};

    Status final_status_;

    Grant::Issuer connection_tokens_;

    Task acceptor_task_;
};

}  // namespace batt

#endif  // BATTERIES_HTTP_HTTP_SERVER_DECL_HPP
