//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2022-2024 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_HTTP_HTTP_SERVER_CONNECTION_DECL_HPP
#define BATTERIES_HTTP_HTTP_SERVER_CONNECTION_DECL_HPP

#include <batteries/config.hpp>
//
#include <batteries/http/http_data.hpp>
#include <batteries/http/http_request.hpp>
#include <batteries/http/http_response.hpp>
#include <batteries/http/http_server_decl.hpp>

#include <batteries/async/buffer_source.hpp>
#include <batteries/async/queue.hpp>
#include <batteries/async/stream_buffer.hpp>
#include <batteries/async/watch.hpp>

#include <batteries/asio/deadline_timer.hpp>
#include <batteries/asio/ip_tcp.hpp>
#include <batteries/status.hpp>

#include <tuple>

namespace batt {

class HttpServerConnection
{
   public:
    /** \brief Used to start an HttpServerConnection processing Task.
     */
    static void spawn(boost::asio::ip::tcp::socket&& socket,
                      HttpRequestDispatcherFactoryFn&& request_dispatcher_factory,
                      const HttpServerSettings& settings) noexcept;

    //+++++++++++-+-+--+----- --- -- -  -  -   -
   private:
    explicit HttpServerConnection(boost::asio::ip::tcp::socket&& socket,
                                  HttpRequestDispatcherFactoryFn&& request_dispatcher_factory,
                                  const HttpServerSettings& settings) noexcept;

    ~HttpServerConnection() noexcept;

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    /* Data/Control flow diagram:

      ╔════╗
      ║Task║
      ╚════╝

            ╔══════════════════════╗   ┌───────────────┐
        ┌──▶║ fill_input_buffer()  ║──▶│ input_buffer_ │
        │   ╚══════════════════════╝   └───────────────┘
        │                                      │
        │                                      ▼
        │                          ╔══════════════════════╗     ┌────────────────┐
        │                       ┌──║   read_requests()    ║◀────│ request_queue_ │◀─┐
        │                       │  ╚══════════════════════╝     └────────────────┘  │
  ┌───────────┐                 ▼              │                                    │
  │  socket_  │        ┌────────────────┐      │          ╔══════════════════════╗  │
  └───────────┘        │response_queue_ │      │          ║  process_requests()  ╠╗ │
        ▲              └────────────────┘      │          ║  ┌────────────────┐  ║╠╗│
        │                       │              └──────────╬─▷│  HttpRequest   │──╬╬╬┤
        │                       ▼                         ║  └────────────────┘  ║║║│
        │           ╔══════════════════════╗              ║  ┌────────────────┐  ║║║│
        └───────────║  write_responses()   ║◁─────────────╬──│  HttpResponse  │──╬╬╬┘
                    ╚══════════════════════╝              ║  └────────────────┘  ║║║
                                                          ╚╦═════════════════════╝║║
                                                           ╚╦═════════════════════╝║
                                                            ╚══════════════════════╝
    */

    StatusOr<boost::asio::ip::tcp::endpoint> local_endpoint() const noexcept;

    StatusOr<boost::asio::ip::tcp::endpoint> remote_endpoint() const noexcept;

    /** \brief Starts all subtasks.
     */
    void start();

    /** \brief Halts the connection and all associated tasks.
     */
    void halt();

    /** \brief Waits for all subtasks to complete.
     */
    void join();

    Status fill_input_buffer();

    Status read_requests();

    void try_spawn_request_processor() noexcept;

    Status process_requests(Grant token);

    Status send_responses();

    Status open_connection();

    StatusOr<i32> read_next_request(pico_http::Request& request);

    void idle_connection_timeout_task_main();

    void halt_idle_timeout_task() noexcept;

    void idle_shutdown() noexcept;

    void shutdown_request_queue();

    void shutdown_response_queue();

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    boost::asio::ip::tcp::socket socket_;

    HttpRequestDispatcherFactoryFn request_dispatcher_factory_;

    HttpServerSettings settings_;

    Grant::Issuer request_processor_tokens_;

    boost::asio::deadline_timer idle_timer_;

    StreamBuffer input_buffer_;

    Queue<std::tuple<Pin<HttpRequest>, Pin<HttpResponse>>> request_queue_;

    Queue<Pin<HttpResponse>> response_queue_;

    Watch<bool> halt_requested_{false};

    Watch<usize> request_count_{0};

    Watch<u64> activity_{0};

    Optional<Task> fill_input_buffer_task_;

    Optional<Task> read_requests_task_;

    Optional<Task> send_responses_task_;

    Optional<Task> idle_connection_timeout_task_;
};

}  // namespace batt

#endif  // BATTERIES_HTTP_HTTP_SERVER_CONNECTION_DECL_HPP
