//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2024 Anthony Paul Astolfi
//
#include <batteries/config.hpp>

#include <batteries/http/http_server_settings_decl.hpp>

#if BATT_HEADER_ONLY
#include <batteries/http/http_server_settings_impl.hpp>
#endif  // BATT_HEADER_ONLY
