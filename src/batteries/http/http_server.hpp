//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2022-2024 Anthony Paul Astolfi
//
#include <batteries/config.hpp>

#include <batteries/http/http_server_decl.hpp>

#if BATT_HEADER_ONLY
#include <batteries/http/http_server_impl.hpp>
#endif  // BATT_HEADER_ONLY
