//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2024 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_HTTP_HTTP_SERVER_SETTINGS_DECL_HPP
#define BATTERIES_HTTP_HTTP_SERVER_SETTINGS_DECL_HPP

#include <batteries/config.hpp>
//
#include <batteries/constants.hpp>
#include <batteries/int_types.hpp>

namespace batt {

struct HttpServerSettings {
    static constexpr usize kDefaultMaxConnections = 1024;
    static constexpr i32 kDefaultIdleConnectionTimeoutMs = 60 * 1000;
    static constexpr usize kDefaultConnectionBufferSize = 16 * kKiB;

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    /** \brief Returns a settings object with default values.
     */
    static HttpServerSettings with_default_values() noexcept;

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    usize max_connections;
    usize max_requests_per_connection;
    i32 idle_connection_timeout_ms;
    usize connection_buffer_size;
};

}  //namespace batt

#endif  // BATTERIES_HTTP_HTTP_SERVER_SETTINGS_DECL_HPP
