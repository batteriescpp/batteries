//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2022 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_HTTP_REQUEST_HPP
#define BATTERIES_HTTP_REQUEST_HPP

#include <batteries/config.hpp>
//
#include <batteries/async/buffer_source.hpp>

#include <batteries/http/http_data.hpp>
#include <batteries/http/http_header.hpp>
#include <batteries/http/http_message_base.hpp>

#include <batteries/int_types.hpp>
#include <batteries/status.hpp>
#include <batteries/stream_util.hpp>

#include <string_view>

namespace batt {

class HttpRequest : public HttpMessageBase<pico_http::Request>
{
   public:
    using HttpMessageBase<pico_http::Request>::HttpMessageBase;
    using HttpMessageBase<pico_http::Request>::major_version;
    using HttpMessageBase<pico_http::Request>::minor_version;
    using HttpMessageBase<pico_http::Request>::headers;
    using HttpMessageBase<pico_http::Request>::find_header;

    const std::string_view& method()
    {
        return this->await_message_or_panic().method;
    }

    const std::string_view& path()
    {
        return this->await_message_or_panic().path;
    }
};

}  // namespace batt

#endif  // BATTERIES_HTTP_REQUEST_HPP
