//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2024 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_HTTP_HTTP_SERVER_CONNECTION_IMPL_HPP
#define BATTERIES_HTTP_HTTP_SERVER_CONNECTION_IMPL_HPP

#include <batteries/http/http_message_info.hpp>

#include <boost/date_time/posix_time/posix_time.hpp>

namespace batt {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*static*/ void HttpServerConnection::spawn(
    boost::asio::ip::tcp::socket&& socket, HttpRequestDispatcherFactoryFn&& request_dispatcher_factory,
    const HttpServerSettings& settings) noexcept
{
    Task::spawn(Task::current().get_executor(), [socket = std::move(socket),
                                                 request_dispatcher_factory =
                                                     std::move(request_dispatcher_factory),
                                                 settings]() mutable {
        HttpServerConnection connection{std::move(socket), std::move(request_dispatcher_factory), settings};

        connection.start();
        connection.join();
    });
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*explicit*/ HttpServerConnection::HttpServerConnection(
    boost::asio::ip::tcp::socket&& socket, HttpRequestDispatcherFactoryFn&& request_dispatcher_factory,
    const HttpServerSettings& settings) noexcept
    : socket_{std::move(socket)}
    , request_dispatcher_factory_{std::move(request_dispatcher_factory)}
    , settings_{settings}
    , request_processor_tokens_{this->settings_.max_requests_per_connection}
    , idle_timer_{this->socket_.get_executor()}
    , input_buffer_{this->settings_.connection_buffer_size}
{
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL HttpServerConnection::~HttpServerConnection() noexcept
{
    this->halt();
    this->join();

    BATT_VLOG(1) << "HttpServerConnection::~HttpServerConnection() Done";
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL StatusOr<boost::asio::ip::tcp::endpoint> HttpServerConnection::local_endpoint()
    const noexcept
{
    boost::system::error_code ec;
    auto ep = this->socket_.local_endpoint(ec);
    BATT_REQUIRE_OK(ec);
    return ep;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL StatusOr<boost::asio::ip::tcp::endpoint> HttpServerConnection::remote_endpoint()
    const noexcept
{
    boost::system::error_code ec;
    auto ep = this->socket_.remote_endpoint(ec);
    BATT_REQUIRE_OK(ec);
    return ep;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void HttpServerConnection::start()
{
    BATT_VLOG(1) << "HttpServerConnection::run() entered";

    auto executor = Task::current().get_executor();

    BATT_CHECK(!this->fill_input_buffer_task_);
    this->fill_input_buffer_task_.emplace(executor, [this] {
        this->fill_input_buffer().IgnoreError();
    });

    BATT_CHECK(!this->read_requests_task_);
    this->read_requests_task_.emplace(executor, [this] {
        this->read_requests().IgnoreError();
    });

    BATT_CHECK(!this->send_responses_task_);
    this->send_responses_task_.emplace(executor, [this] {
        this->send_responses().IgnoreError();
    });

    BATT_CHECK(!this->idle_connection_timeout_task_);
    this->idle_connection_timeout_task_.emplace(executor, [this] {
        this->idle_connection_timeout_task_main();
    });
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void HttpServerConnection::halt()
{
    this->idle_shutdown();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void HttpServerConnection::join()
{
    const auto join_subtask = [](Optional<Task>& subtask) {
        if (subtask) {
            subtask->join();
            subtask = None;
        }
    };

    join_subtask(this->fill_input_buffer_task_);
    join_subtask(this->read_requests_task_);
    join_subtask(this->send_responses_task_);
    join_subtask(this->idle_connection_timeout_task_);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL Status HttpServerConnection::fill_input_buffer()
{
    auto on_exit = finally([this] {
        BATT_VLOG(1) << "HttpServerConnection::fill_input_buffer() exiting"
                     << BATT_INSPECT(this->local_endpoint()) << BATT_INSPECT(this->remote_endpoint());

        this->input_buffer_.close_for_write();
    });

    for (;;) {
        BATT_VLOG(1) << "Reading from socket";

        // Allocate some space in the input buffer for incoming data.
        //
        StatusOr<SmallVec<MutableBuffer, 2>> buffer = this->input_buffer_.prepare_at_least(1);
        BATT_REQUIRE_OK(buffer);

        // Read data from the socket into the buffer.
        //
        auto n_read = Task::await<IOResult<usize>>([&](auto&& handler) {
            this->socket_.async_read_some(*buffer, BATT_FORWARD(handler));
        });
        BATT_REQUIRE_OK(n_read) << "(HttpServerConnection::fill_input_buffer)";

        BATT_VLOG(1) << "Read " << *n_read << " bytes; committing to buffer";

        this->activity_.fetch_add(1);

        // Assuming we were successful, commit the read data so it can be consumed by the parser task.
        //
        this->input_buffer_.commit(*n_read);
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL Status HttpServerConnection::read_requests()
{
    auto on_exit = finally([this] {
        BATT_VLOG(1) << "HttpServerConnection::read_requests() exiting"
                     << BATT_INSPECT(this->local_endpoint()) << BATT_INSPECT(this->remote_endpoint());

        // Unblock all other tasks by closing anything they might wait on.
        //
        this->activity_.close();
        this->halt_idle_timeout_task();
        this->input_buffer_.close_for_read();
        this->shutdown_request_queue();
        this->response_queue_.close();

        boost::system::error_code ec;
        this->socket_.shutdown(boost::asio::ip::tcp::socket::shutdown_receive, ec);
    });

    for (;;) {
        BATT_VLOG(1) << "Awaiting request";

        //----- --- -- -  -  -   -
        // If there are no request processor subtasks asking for work to do, then try to start one.
        //
        this->try_spawn_request_processor();

        //----- --- -- -  -  -   -
        // Wait for an available request processor to ask for work by queuing a request/response object pair.
        //
        using RequestResponseTuple = std::tuple<Pin<HttpRequest>, Pin<HttpResponse>>;

        BATT_ASSIGN_OK_RESULT(RequestResponseTuple request_response, this->request_queue_.await_next());

        Pin<HttpRequest> request;
        Pin<HttpResponse> response;
        std::tie(request, response) = std::move(request_response);

        BATT_CHECK_NOT_NULLPTR(request);
        BATT_CHECK_NOT_NULLPTR(response);

        // By default, we will mark the request and response as 'closed' so that the processor task who owns
        // them isn't indefinitely blocked.  These FinalAct objects will be cancelled once an alternate code
        // path for updating the state has been entered.
        //
        auto close_request = batt::finally([&request] {
            request->close();
        });
        auto close_response = batt::finally([&response] {
            response->close();
        });

        //----- --- -- -  -  -   -
        // We're only interested in the request; send the pinned response ref to the response writer task
        // (send_responses).  NOTE: The fact that there is only one task per connection inside read_requests
        // guarantees that the order of requests and responses will always match!
        //
        BATT_REQUIRE_OK(this->response_queue_.push(std::move(response)));
        close_response.cancel();

        BATT_VLOG(1) << "Got request/response pair";

        //----- --- -- -  -  -   -
        // Read a single HTTP request message from the input buffer.
        //
        pico_http::Request request_message;
        StatusOr<i32> message_length = this->read_next_request(request_message);
        BATT_REQUIRE_OK(message_length)
            << LogLevel::kError << "Failed to read/parse request (HttpServerConnection::read_requests)";

        BATT_VLOG(1) << "Parsed response header";

        // Extract connection-level information about the request.
        //
        HttpMessageInfo request_info(request_message);
        if (!request_info.is_valid()) {
            return StatusCode::kInvalidArgument;
        }

        //----- --- -- -  -  -   -
        // Request state is now kInitialized.
        //
        close_request.cancel();
        request->state().set_value(HttpRequest::kInitialized);

        //----- --- -- -  -  -   -
        // Pass control over to the request processor task who owns `request` and wait for it to signal it is
        // done reading the message headers/url/etc.
        //
        Status message_consumed = request->await_set_message(request_message);
        BATT_REQUIRE_OK(message_consumed) << LogLevel::kError;

        BATT_VLOG(1) << "Done reading request header";
        this->activity_.fetch_add(1);

        // The consume is done with the message; consume it and move on to the body.
        //
        this->input_buffer_.consume(*message_length);

        //----- --- -- -  -  -   -
        // Create an HttpData object to read the body of the request.  Hand control over to the request
        // processor task and wait for it to signal it is done.
        //
        HttpData request_data{request_info.get_data(this->input_buffer_)};
        request->await_set_data(request_data).IgnoreError();
        //
        BATT_VLOG(1) << "Done reading request body";
        //
        // We must not touch `request` after `await_set_data` returns!

        // Whether or not we keep the connection alive, acknowledge the response when we exit the loop.
        //
        auto on_scope_exit = batt::finally([this] {
            this->request_count_.fetch_add(1);
        });

        this->activity_.fetch_add(1);

        //----- --- -- -  -  -   -
        // If we are going to keep the connection alive, we must consume any extra data that wasn't read
        // by the request processor.  Otherwise we just close the socket.
        //
        if (request_info.keep_alive) {
            BATT_VLOG(1) << "Request is Keep-Alive";
            Status data_consumed = std::move(request_data) | seq::consume();
            BATT_REQUIRE_OK(data_consumed) << LogLevel::kError;
        } else {
            BATT_VLOG(1) << "Response is Close; closing socket";
            break;
        }
    }

    return OkStatus();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void HttpServerConnection::try_spawn_request_processor() noexcept
{
    if (this->request_queue_.size() == 0) {
        // Each request processor task must hold a token; this limits the total number of tasks and the
        // pipelined request depth.  Try to acquire 1 token; if this succeeds, then spawn a request
        // processor task.
        //
        StatusOr<Grant> maybe_token = this->request_processor_tokens_.issue_grant(1, WaitForResource::kFalse);

        if (maybe_token.ok()) {
            Task::spawn(Task::current().get_executor(), [this, token = std::move(*maybe_token)]() mutable {
                this->process_requests(std::move(token)).IgnoreError();
            });
        }
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL StatusOr<i32> HttpServerConnection::read_next_request(pico_http::Request& request)
{
    BATT_VLOG(1) << "HttpServerConnection::read_next_request";

    i32 result = 0;
    usize min_to_fetch = 1;
    for (;;) {
        StatusOr<SmallVec<ConstBuffer, 2>> fetched = this->input_buffer_.fetch_at_least(min_to_fetch);
        BATT_REQUIRE_OK(fetched) << "(HttpServerConnection::read_next_request)";

        auto& buffers = *fetched;
        const usize n_bytes_fetched = boost::asio::buffer_size(buffers);

        BATT_CHECK(!buffers.empty());
        result = request.parse(buffers.front());

        if (result == pico_http::kParseIncomplete) {
            min_to_fetch = std::max(min_to_fetch + 1, n_bytes_fetched);
            continue;
        }

        if (result == pico_http::kParseFailed) {
            auto chunk = buffers.front();
            BATT_VLOG(1) << "Failed to parse http message: "
                         << c_str_literal(std::string_view{(const char*)chunk.data(), chunk.size()});
            return {StatusCode::kInternal};
        }

        BATT_CHECK_GT(result, 0);
        return result;
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void HttpServerConnection::idle_connection_timeout_task_main()
{
    for (;;) {
        const u64 activity_before = this->activity_.get_value();

        // IMPORTANT: we must set the timer and _then_ check the halt_requested_ flag (the reverse of the
        // order in which they are set when shutting down), so we will never miss a shutdown.
        //
        boost::system::error_code ec;
        this->idle_timer_.expires_from_now(
            boost::posix_time::milliseconds(this->settings_.idle_connection_timeout_ms), ec);

        BATT_VLOG(1) << BATT_INSPECT(activity_before) << "; Waiting for "
                     << this->settings_.idle_connection_timeout_ms << "ms"
                     << BATT_INSPECT(this->idle_timer_.expires_at());

        if (ec) {
            BATT_VLOG(1) << "Error setting idle timer: " << BATT_INSPECT(to_status(ec));
            break;
        }

        if (this->halt_requested_.get_value()) {
            break;
        }

        IOResult<> result = Task::await<IOResult<>>([this](auto&& handler) {
            this->idle_timer_.async_wait(BATT_FORWARD(handler));
        });

        if (!result.ok() || this->halt_requested_.get_value()) {
            BATT_VLOG(1) << "Idle timer cancelled: " << BATT_INSPECT(result)
                         << BATT_INSPECT(this->halt_requested_);
            break;
        }

        if (this->activity_.get_value() == activity_before) {
            BATT_VLOG(1) << "Closing idle connection" << BATT_INSPECT(this->remote_endpoint())
                         << BATT_INSPECT(this->local_endpoint());
            this->idle_shutdown();
            break;
        }
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void HttpServerConnection::halt_idle_timeout_task() noexcept
{
    // IMPORTANT: halt_requested_ flag must be set before cancelling the timer.
    //
    this->halt_requested_.set_value(true);

    boost::system::error_code ec;

    this->idle_timer_.cancel(ec);
    this->idle_timer_.expires_from_now(boost::posix_time::seconds(-1), ec);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void HttpServerConnection::idle_shutdown() noexcept
{
    boost::system::error_code ec;
    this->socket_.close(ec);
    //----- --- -- -  -  -   -
    // Do NOT close the token pool, since we never do a blocking issue_grant during normal operation.
    //  this->request_processor_tokens_.close();
    //----- --- -- -  -  -   -
    this->input_buffer_.close();
    this->shutdown_request_queue();
    this->shutdown_response_queue();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL Status HttpServerConnection::process_requests(Grant token)
{
    const auto task_id = Task::current_id();

    BATT_VLOG(1) << "HttpServerConnection::process_requests Entered" << BATT_INSPECT(task_id);

    auto on_scope_exit = batt::finally([&] {
        BATT_VLOG(1) << "HttpServerConnection::process_requests Exiting" << BATT_INSPECT(task_id);
    });

    BATT_CHECK_EQ(token.size(), 1u);
    BATT_CHECK_EQ(token.get_issuer(), std::addressof(this->request_processor_tokens_));

    BATT_ASSIGN_OK_RESULT(HttpRequestDispatcherFn request_dispatcher, this->request_dispatcher_factory_());

    for (;;) {
        HttpRequest request;
        HttpResponse response;

        Status push_status = this->request_queue_.push(make_pin(&request), make_pin(&response));
        BATT_REQUIRE_OK(push_status);

        BATT_VLOG(1) << "HttpServerConnection::process_requests: Waiting for message"
                     << BATT_INSPECT(task_id);

        StatusOr<pico_http::Request&> request_message = request.await_message();
        BATT_REQUIRE_OK(request_message)
            << "HttpServerConnection::process_requests: Failed to receive HTTP request message";

        BATT_VLOG(1) << "HttpServerConnection::process_requests: Got request; dispatching"
                     << BATT_INSPECT(task_id);

        Status dispatch_status = request_dispatcher(request, response);
        BATT_REQUIRE_OK(dispatch_status);
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL Status HttpServerConnection::send_responses()
{
    BATT_VLOG(1) << "HttpServerConnection::send_responses Entered";

    auto on_scope_exit = batt::finally([&] {
        BATT_VLOG(1) << "HttpServerConnection::send_responses Exiting";

        this->shutdown_request_queue();
        this->shutdown_response_queue();

        boost::system::error_code ec;
        this->socket_.shutdown(boost::asio::ip::tcp::socket::shutdown_both, ec);
    });

    for (;;) {
        BATT_VLOG(1) << "HttpServerConnection::send_responses: Waiting for request processor";

        BATT_ASSIGN_OK_RESULT(Pin<HttpResponse> response, this->response_queue_.await_next());

        BATT_VLOG(1) << "HttpServerConnection::send_responses: Waiting for response";

        StatusOr<pico_http::Response&> response_message = response->await_message();
        BATT_REQUIRE_OK(response_message);

        HttpMessageInfo response_info{*response_message};
        if (!response_info.is_valid()) {
            response_info.keep_alive = false;
            if (!response_info.is_valid()) {
                BATT_LOG_WARNING() << "Invalid response message: " << response_info;
                break;
            }
        }

        this->activity_.fetch_add(1);

        BATT_VLOG(1) << "HttpServerConnection::send_responses: Got response; serializing";

        Status status = response->serialize(this->socket_);
        BATT_REQUIRE_OK(status);

        BATT_VLOG(1) << "HttpServerConnection::send_responses: Finished serializing response";

        this->activity_.fetch_add(1);

        if (!response_info.keep_alive) {
            break;
        }
    }

    return OkStatus();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void HttpServerConnection::shutdown_request_queue()
{
    BATT_VLOG(1) << "Shutting down request queue";

    this->request_queue_.close();

    this->request_queue_.drain([](std::tuple<Pin<HttpRequest>, Pin<HttpResponse>>& item) {
        Pin<HttpRequest> request;
        Pin<HttpResponse> response;
        std::tie(request, response) = std::move(item);

        BATT_CHECK_NOT_NULLPTR(request);
        BATT_CHECK_NOT_NULLPTR(response);

        request->close();
        response->close();
    });
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void HttpServerConnection::shutdown_response_queue()
{
    BATT_VLOG(1) << "Shutting down response queue";

    this->response_queue_.close();

    this->response_queue_.drain([](Pin<HttpResponse>& response) {
        BATT_CHECK_NOT_NULLPTR(response);
        response->close();
    });
}

}  //namespace batt

#endif  // BATTERIES_HTTP_HTTP_SERVER_CONNECTION_IMPL_HPP
