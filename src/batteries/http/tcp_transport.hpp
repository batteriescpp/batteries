//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2024 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_HTTP_TCP_TRANSPORT_HPP
#define BATTERIES_HTTP_TCP_TRANSPORT_HPP

#include <batteries/config.hpp>
//
#include <batteries/http/http_client_host_context_decl.hpp>

#include <batteries/async/task.hpp>

#include <batteries/asio/ip_tcp.hpp>

#include <mutex>

namespace batt {

/** \brief A thread-safe socket based TCP connection for HTTP clients.
 */
class TcpTransport
{
   public:
    /** \brief Construct a new TcpTransport that uses the passed io_context for socket communication.
     */
    explicit TcpTransport(HttpClientHostContext& host_context) noexcept
        : socket_{host_context.get_io_context()}
    {
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    /** \brief Returns the local endpoint associated with this connection, if connected; otherwise returns a
     * default endpoint value and sets the passed ErrorCode value.
     */
    boost::asio::ip::tcp::endpoint local_endpoint(ErrorCode& ec) const noexcept
    {
        std::unique_lock<std::mutex> lock{this->mutex_};
        return this->socket_.local_endpoint(ec);
    }

    /** \brief Returns the remote endpoint associated with this connection, if connected; otherwise returns a
     * default endpoint value and sets the passed ErrorCode value.
     */
    boost::asio::ip::tcp::endpoint remote_endpoint(ErrorCode& ec) const noexcept
    {
        std::unique_lock<std::mutex> lock{this->mutex_};
        return this->socket_.remote_endpoint(ec);
    }

    /** \brief Returns true iff this socket is open.
     */
    bool is_open() const noexcept
    {
        std::unique_lock<std::mutex> lock{this->mutex_};
        return this->socket_.is_open();
    }

    /** \brief Closes the socket (both directions).
     */
    void close(ErrorCode& ec) noexcept
    {
        std::unique_lock<std::mutex> lock{this->mutex_};
        this->socket_.close(ec);
    }

    /** \brief Performs a protocol shutdown in one or both directions for the underlying socket.
     */
    void shutdown(boost::asio::socket_base::shutdown_type mode, ErrorCode& ec) noexcept
    {
        std::unique_lock<std::mutex> lock{this->mutex_};
        this->socket_.shutdown(mode, ec);
    }

    /** \brief Starts an asynchronous connect operation for this socket, invoking `handler` when complete.
     */
    template <typename Handler = void(const ErrorCode&)>
    void async_connect(const boost::asio::ip::tcp::endpoint& ep, Handler&& handler)
    {
        std::unique_lock<std::mutex> lock{this->mutex_};
        this->socket_.async_connect(ep, BATT_FORWARD(handler));
    }

    /** \brief Starts reading some data from the connection, asychronously.
     */
    template <typename MutableBufferSequence, typename Handler = void(const ErrorCode&, usize)>
    void async_read_some(MutableBufferSequence&& buffers, Handler&& handler)
    {
        std::unique_lock<std::mutex> lock{this->mutex_};
        this->socket_.async_read_some(BATT_FORWARD(buffers), BATT_FORWARD(handler));
    }

    /** \brief Starts writing some data to the connection, asychronously.
     */
    template <typename ConstBufferSequence, typename Handler = void(const ErrorCode&, usize)>
    void async_write_some(ConstBufferSequence&& buffers, Handler&& handler)
    {
        std::unique_lock<std::mutex> lock{this->mutex_};
        this->socket_.async_write_some(BATT_FORWARD(buffers), BATT_FORWARD(handler));
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -
   private:
    /** \brief Protects access to `this->socket_`.
     */
    mutable std::mutex mutex_;

    /** \brief The underlying socket stream.
     */
    boost::asio::ip::tcp::socket socket_;
};

}  //namespace batt

#endif  // BATTERIES_HTTP_TCP_TRANSPORT_HPP
