//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2024 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_HTTP_HTTP_CLIENT_CONNECTION_IPP
#define BATTERIES_HTTP_HTTP_CLIENT_CONNECTION_IPP

#include <batteries/http/http_chunk_decoder.hpp>
#include <batteries/http/http_client_connection_decl.hpp>
#include <batteries/http/http_client_host_context_decl.hpp>
#include <batteries/http/http_message_info.hpp>

#include <chrono>

namespace batt {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename Transport>
inline /*static*/ void BasicHttpClientConnection<Transport>::spawn(HttpClientHostContext& context,
                                                                   Watch<usize>& active_connections) noexcept
{
    active_connections.fetch_add(1);

    Task::spawn(
        context.get_io_context().get_executor(),
        [&context, &active_connections] {
            auto on_scope_exit = batt::finally([&active_connections] {
                active_connections.fetch_sub(1);
            });

            {
                BasicHttpClientConnection<Transport> connection{context};
                connection.run();
            }
            //
            // Important: this nested scope is here to emphasize that the HttpClientConnection object should
            // be completely destroyed before decrementing `active_connections`.
        },
        /*name=*/"HttpClientConnection::parent_task");
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename Transport>
inline /*explicit*/ BasicHttpClientConnection<Transport>::BasicHttpClientConnection(
    HttpClientHostContext& context) noexcept
    : context_{context}
    , idle_connection_timeout_ms_{this->context_.get_connection_timeout_ms()}
    , transport_{this->context_}
    , idle_timer_{this->context_.get_io_context()}
{
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename Transport>
inline boost::asio::io_context& BasicHttpClientConnection<Transport>::get_io_context()
{
    return this->context_.get_io_context();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename Transport>
inline StatusOr<boost::asio::ip::tcp::endpoint> BasicHttpClientConnection<Transport>::local_endpoint()
    const noexcept
{
    boost::system::error_code ec;
    auto ep = this->transport_.local_endpoint(ec);
    BATT_REQUIRE_OK(ec);
    return ep;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename Transport>
inline StatusOr<boost::asio::ip::tcp::endpoint> BasicHttpClientConnection<Transport>::remote_endpoint()
    const noexcept
{
    boost::system::error_code ec;
    auto ep = this->transport_.remote_endpoint(ec);
    BATT_REQUIRE_OK(ec);
    return ep;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename Transport>
inline void BasicHttpClientConnection<Transport>::run()
{
    BATT_DEBUG_INFO(BATT_INSPECT(this->local_endpoint()) << BATT_INSPECT(this->remote_endpoint()));

    auto executor = Task::current().get_executor();

    Task process_requests_task{executor,
                               [this] {
                                   BATT_DEBUG_INFO(BATT_INSPECT(this->local_endpoint())
                                                   << BATT_INSPECT(this->remote_endpoint()));

                                   this->process_requests().IgnoreError();
                               },
                               "HttpClientConnection::process_requests"};

    Task process_responses_task{executor,
                                [this] {
                                    BATT_DEBUG_INFO(BATT_INSPECT(this->local_endpoint())
                                                    << BATT_INSPECT(this->remote_endpoint()));

                                    this->process_responses().IgnoreError();
                                },
                                "HttpClientConnection::process_responses"};

    Task idle_connection_timeout_task{executor,
                                      [this] {
                                          BATT_DEBUG_INFO(BATT_INSPECT(this->local_endpoint())
                                                          << BATT_INSPECT(this->remote_endpoint()));

                                          this->idle_connection_timeout_task_main();
                                      },
                                      "HttpClientConnection::idle_connection_timeout_task"};

    idle_connection_timeout_task.join();
    process_requests_task.join();
    process_responses_task.join();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename Transport>
inline Status BasicHttpClientConnection<Transport>::open_connection()
{
    StatusOr<SmallVec<boost::asio::ip::tcp::endpoint>> hosts =
        await_resolve(this->get_io_context(), this->context_.host_address());
    BATT_REQUIRE_OK(hosts);

    for (const boost::asio::ip::tcp::endpoint& endpoint : *hosts) {
        ErrorCode ec = Task::await_connect(this->transport_, endpoint);
        if (!ec) {
            return OkStatus();
        }
    }

    return StatusCode::kUnavailable;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename Transport>
inline void BasicHttpClientConnection<Transport>::halt() noexcept
{
    this->response_count_.close();

    if (this->transport_.is_open()) {
        boost::system::error_code ec;
        this->transport_.close(ec);
    }

    this->input_buffer_.close_for_read();
    this->input_buffer_.close_for_write();

    this->response_queue_.close();

    this->idle_timer_.cancel();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename Transport>
inline Status BasicHttpClientConnection<Transport>::process_requests()
{
    Optional<Task> fill_input_buffer_task;

    auto on_exit = finally([this, &fill_input_buffer_task] {
        BATT_VLOG(1) << "HttpClientConnection::process_requests() exiting"
                     << BATT_INSPECT(this->local_endpoint()) << BATT_INSPECT(this->remote_endpoint());

        boost::system::error_code ec;
        this->transport_.shutdown(boost::asio::socket_base::shutdown_send, ec);

        this->response_queue_.close();
        this->activity_.close();
        this->idle_timer_.cancel();

        if (fill_input_buffer_task) {
            fill_input_buffer_task->join();
        }
    });

    bool connected = false;
    usize request_count = 0;

    // We keep looping until we encounter an error or the socket is closed.
    //
    for (;;) {
        {
            // Wait for any outstanding responses to be consumed before deciding whether we can continue.
            //  Note: this does mean that we are not supporting request pipelining, which definitely makes
            //  it simpler to deal with scenarios where requests *are* pipelined but the server responds
            //  by closing the connection before we read their responses.
            //
            BATT_DEBUG_INFO("process_requests() waiting for response to be processed"
                            << BATT_INSPECT(request_count)
                            << BATT_INSPECT(this->response_count_.get_value()));

            BATT_REQUIRE_OK(this->response_count_.await_equal(request_count));
        }

        // If the response task closed the connection, exit the loop.
        //
        if (connected && !this->transport_.is_open()) {
            break;
        }

        // These are read from the host context's queue (on a FIFO, MPMC basis).  The Pin forces the
        // referred objects from going out of scope while this function (or some other part of the
        // connection) is still using them.  IMPORTANT: this means if the Pins aren't released, it may
        // block some part of the application that submitted the request or is waiting for a response.
        //
        Pin<HttpRequest> request;
        Pin<HttpResponse> response;

        BATT_VLOG(1) << "HttpClientConnection::process_requests() waiting for next request";

        StatusOr<std::tuple<Pin<HttpRequest>, Pin<HttpResponse>>>  //
            queue_result = this->context_.await_next_request();
        if (queue_result.status() == StatusCode::kPoke) {
            continue;
        }
        BATT_ASSIGN_OK_RESULT(std::tie(request, response), std::move(queue_result));

        this->activity_.fetch_add(1);

        BATT_VLOG(1) << "HttpClientConnection::process_requests() got request/response pair"
                     << BATT_INSPECT(connected);

        request_count += 1;

        BATT_CHECK_NOT_NULLPTR(request);
        BATT_CHECK_NOT_NULLPTR(response);
        BATT_CHECK_EQ(request->state().get_value(), HttpRequest::kInitialized);

        if (!connected) {
            Status status = this->open_connection();
            if (!status.ok()) {
                request->update_status(status);
                request->state().close();
            }
            BATT_REQUIRE_OK(status);
            connected = true;
            this->activity_.fetch_add(1);

            fill_input_buffer_task.emplace(
                Task::current().get_executor(),
                [this] {
                    BATT_DEBUG_INFO(BATT_INSPECT(this->local_endpoint())
                                    << BATT_INSPECT(this->remote_endpoint()));
                    this->fill_input_buffer().IgnoreError();
                },
                "HttpClientConnection::fill_input_buffer_task");
        }

        // Hand the response off to the process_responses() task; use of `std::move` here releases our
        // local pin on the response object.
        //
        BATT_REQUIRE_OK(this->response_queue_.push(std::move(response)));

        BATT_VLOG(1) << "HttpClientConnection::process_requests() serializing request";

        // Write the request message to the socket.  This function will block until the entire request has
        // been serialized.
        //
        Status status = request->serialize(this->transport_);
        BATT_REQUIRE_OK(status) << LogLevel::kError << "Error serializing request";

        this->activity_.fetch_add(1);

        BATT_VLOG(1)
            << "HttpClientConnection::process_requests() request serialized!  Setting state to kConsumed";

        request->state().set_value(HttpRequest::kConsumed);
        //
        // ~request - releases the Pin
    }

    return OkStatus();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename Transport>
inline Status BasicHttpClientConnection<Transport>::fill_input_buffer()
{
    auto on_exit = finally([this] {
        BATT_VLOG(1) << "HttpClientConnection::fill_input_buffer() exiting"
                     << BATT_INSPECT(this->local_endpoint()) << BATT_INSPECT(this->remote_endpoint());

        this->input_buffer_.close_for_write();
    });

    for (;;) {
        // Allocate some space in the input buffer for incoming data.
        //
        StatusOr<SmallVec<MutableBuffer, 2>> buffer = this->input_buffer_.prepare_at_least(1);
        BATT_REQUIRE_OK(buffer);

        // Read data from the socket into the buffer.
        //
        auto n_read = Task::await<IOResult<usize>>([&](auto&& handler) {
            this->transport_.async_read_some(*buffer, BATT_FORWARD(handler));
        });
        BATT_REQUIRE_OK(n_read);

        this->activity_.fetch_add(1);

        // Assuming we were successful, commit the read data so it can be consumed by the parser task.
        //
        this->input_buffer_.commit(*n_read);
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename Transport>
inline Status BasicHttpClientConnection<Transport>::process_responses()
{
    auto on_exit = finally([this] {
        BATT_VLOG(1) << "HttpClientConnection::process_responses() exiting"
                     << BATT_INSPECT(this->local_endpoint()) << BATT_INSPECT(this->remote_endpoint());

        // Unblock all other tasks by closing anything they might wait on.
        //
        this->activity_.close();
        this->idle_timer_.cancel();
        this->input_buffer_.close_for_read();
        this->response_count_.close();

        if (this->transport_.is_open()) {
            boost::system::error_code ec;
            this->transport_.close(ec);
        }
    });

    for (;;) {
        BATT_VLOG(1) << "Awaiting response";

        BATT_ASSIGN_OK_RESULT(Pin<HttpResponse> const response, this->response_queue_.await_next());
        BATT_CHECK_NOT_NULLPTR(response);

        this->activity_.fetch_add(1);

        BATT_VLOG(1) << "Got response";

        pico_http::Response response_message;
        StatusOr<i32> message_length = this->read_next_response(response_message);
        BATT_REQUIRE_OK(message_length) << LogLevel::kError << "Failed to read response";

        BATT_VLOG(1) << "Parsed response header";

        HttpMessageInfo response_info(response_message);
        if (!response_info.is_valid()) {
            response->update_status(StatusCode::kInvalidArgument);
            response->state().close();
            return StatusCode::kInvalidArgument;
        }

        response->state().set_value(HttpResponse::kInitialized);

        // Pass control over to the consumer and wait for it to signal it is done reading the message
        // headers.
        //
        Status message_consumed = response->await_set_message(response_message);
        BATT_REQUIRE_OK(message_consumed) << LogLevel::kError;

        BATT_VLOG(1) << "App done reading header";
        this->activity_.fetch_add(1);

        // The consume is done with the message; consume it and move on to the body.
        //
        this->input_buffer_.consume(*message_length);

        HttpData response_data{response_info.get_data(this->input_buffer_)};
        response->await_set_data(response_data).IgnoreError();
        //
        BATT_VLOG(1) << "App done reading body";
        //
        // We must not touch `response` after `await_set_data` returns!

        // Whether or not we keep the connection alive, acknowledge the response when we exit the loop.
        //
        auto on_scope_exit = batt::finally([this] {
            this->response_count_.fetch_add(1);
        });

        this->activity_.fetch_add(1);

        // If we are going to keep the connection alive, we must consume any extra data that wasn't read
        // by the application.  Otherwise we just close it.
        //
        if (response_info.keep_alive) {
            BATT_VLOG(1) << "response is keep_alive";
            Status data_consumed = std::move(response_data) | seq::consume();
            BATT_REQUIRE_OK(data_consumed) << LogLevel::kError;
        } else {
            BATT_VLOG(1) << "response is close; closing socket";
            break;
        }
    }

    return OkStatus();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename Transport>
inline StatusOr<i32> BasicHttpClientConnection<Transport>::read_next_response(pico_http::Response& response)
{
    i32 result = 0;
    usize min_to_fetch = 1;
    for (;;) {
        StatusOr<SmallVec<ConstBuffer, 2>> fetched = this->input_buffer_.fetch_at_least(min_to_fetch);
        BATT_REQUIRE_OK(fetched);

        auto& buffers = *fetched;
        const usize n_bytes_fetched = boost::asio::buffer_size(buffers);

        BATT_CHECK(!buffers.empty());
        result = response.parse(buffers.front());

        if (result == pico_http::kParseIncomplete) {
            min_to_fetch = std::max(min_to_fetch + 1, n_bytes_fetched);
            continue;
        }

        if (result == pico_http::kParseFailed) {
            auto chunk = buffers.front();
            BATT_VLOG(1) << "Failed to parse http message: "
                         << c_str_literal(std::string_view{(const char*)chunk.data(), chunk.size()});
            return {StatusCode::kInternal};
        }

        BATT_CHECK_GT(result, 0);
        return result;
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename Transport>
inline void BasicHttpClientConnection<Transport>::idle_connection_timeout_task_main()
{
    // Zero means disable timeouts.
    //
    if (this->idle_connection_timeout_ms_ == 0) {
        return;
    }

    // Whenever we wake up to find there has been some activity while we were asleep, we will reset
    // `idle_since` to the current clock time.
    //
    auto idle_since = std::chrono::steady_clock::now();

    // Local helper function - returns the elapsed time from `idle_since` to now.
    //
    const auto get_idle_ms = [&idle_since] {
        return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() -
                                                                     idle_since)
            .count();
    };

    // Repeatedly sleep for ~1/4 of the timeout value, until the activity object is closed, the idle_timer_ is
    // cancelled, or we reach the idle timeout.
    //
    while (get_idle_ms() < this->idle_connection_timeout_ms_ && !this->activity_.is_closed()) {
        const auto initial_activity = this->activity_.get_value();
        const auto timeout_ms = (this->idle_connection_timeout_ms_ + 3) / 4;

        BATT_VLOG(1) << "Sleeping for " << timeout_ms << "ms;" << BATT_INSPECT(get_idle_ms());

        ErrorCode ec = Task::await<ErrorCode>([&](auto&& handler) {
            ErrorCode ec;
            this->idle_timer_.expires_from_now(boost::posix_time::milliseconds(timeout_ms), ec);
            if (ec) {
                BATT_FORWARD(handler)(ec);
            } else {
                this->idle_timer_.async_wait(BATT_FORWARD(handler));
            }
        });

        if (ec) {
            BATT_VLOG(1) << "Idle connection timer was cancelled; exiting";
            break;
        }

        if (this->activity_.get_value() != initial_activity) {
            idle_since = std::chrono::steady_clock::now();
        }
    }
    BATT_VLOG(1) << "Finished idle connection timer loop";

    // Once we are out the loop, it is time to shut down!
    //
    if (!this->activity_.is_closed()) {
        BATT_VLOG(1) << "Shutting down HttpClientConnection due to inactivity;" << BATT_INSPECT(get_idle_ms())
                     << BATT_INSPECT(this->idle_connection_timeout_ms_)
                     << BATT_INSPECT(this->local_endpoint()) << BATT_INSPECT(this->remote_endpoint());

        this->halt();
    }
    // else - if activity_ is closed, then we are in the process of shutting down due to other causes
}

}  //namespace batt

#endif  // BATTERIES_HTTP_HTTP_CLIENT_CONNECTION_IPP
