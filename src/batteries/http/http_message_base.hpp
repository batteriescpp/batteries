//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2022 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_HTTP_HTTP_MESSAGE_BASE_HPP
#define BATTERIES_HTTP_HTTP_MESSAGE_BASE_HPP

#include <batteries/config.hpp>
//
#include <batteries/http/http_data.hpp>
#include <batteries/http/http_header.hpp>

#include <batteries/async/channel.hpp>
#include <batteries/async/pin.hpp>
#include <batteries/async/stream_buffer.hpp>
#include <batteries/async/task.hpp>
#include <batteries/async/watch.hpp>

#include <batteries/optional.hpp>
#include <batteries/small_vec.hpp>
#include <batteries/status.hpp>

#include <string_view>

namespace batt {

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------
/** \brief The common base type for HttpRequest and HttpResponse.
 *
 * The lifecycle of all HTTP message objects is tracked through the state property (see
 * HttpMessageBase::state):
 *
 *   - `HttpMessageBase<T>::kCreated`
 *     - The starting state for all message objects
 *   - `HttpMessageBase<T>::kInitialized`
 *     - The message has been fully initialized and is ready to be read/serialized
 *   - `HttpMessageBase<T>::kConsumed`
 *     - The reader (consumer) of the message is done and the object may safely be disposed of
 */
template <typename T>
class HttpMessageBase : public Pinnable
{
   public:
    using Message = T;

    enum State {
        kCreated = 0,
        kInitialized = 1,
        kConsumed = 2,
    };

    HttpMessageBase(const HttpMessageBase&) = delete;
    HttpMessageBase& operator=(const HttpMessageBase&) = delete;

    HttpMessageBase()
    {
    }

    ~HttpMessageBase() noexcept
    {
        this->release_message();
        this->release_data();
    }

    Watch<i32>& state() noexcept
    {
        return this->state_;
    }

    void update_status(Status status) noexcept
    {
        this->status_.Update(status);
    }

    Status get_status() const noexcept
    {
        return this->status_;
    }

    void close(Status status = StatusCode::kClosed) noexcept
    {
        this->update_status(status);
        this->state().close();
        this->message_.close_for_write();
        this->data_.close_for_write();
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    void async_set_message(Message& message)
    {
        this->message_.async_write(message, [](Status status) {
            status.IgnoreError();
        });
    }

    Status await_set_message(Message& message)
    {
        return this->message_.write(message);
    }

    StatusOr<Message&> await_message()
    {
        return this->message_.read();
    }

    Message& await_message_or_panic()
    {
        StatusOr<Message&> msg = this->await_message();
        BATT_CHECK_OK(msg);
        return *msg;
    }

    void release_message()
    {
        if (this->message_.is_active()) {
            this->message_.consume();
        }
    }

    void async_set_data(HttpData& data)
    {
        this->data_.async_write(data, [](Status status) {
            status.IgnoreError();
        });
    }

    Status await_set_data(HttpData& data)
    {
        return this->data_.write(data);
    }

    template <typename WriterFn /* = Status (AsyncWriteStream& dst) */>
    Status invoke_stream_writer(usize buffer_size, WriterFn&& writer_fn)
    {
        BATT_CHECK_NOT_NULLPTR(std::addressof(Task::current()))
            << "batt::HttpMessageBase::invoke_stream_writer must be called from inside a batt::Task";

        StreamBuffer buffer{buffer_size};

        Status status;
        {
            Status writer_status;

            Task writer_task{Task::current().get_executor(), [&] {
                                 writer_status = writer_fn(buffer);
                                 buffer.close_for_write();
                             }};

            HttpData http_data{
                .source = batt::BufferSource{std::ref(buffer)},
            };

            status = this->await_set_data(http_data);

            writer_task.join();
            status.Update(writer_status);
        }

        return status;
    }

    template <typename WriterFn /* = Status (AsyncWriteStream& dst) */>
    Status invoke_stream_writer(WriterFn&& writer_fn)
    {
        return this->invoke_stream_writer(/*buffer_size=*/4 * kKiB, BATT_FORWARD(writer_fn));
    }

    StatusOr<HttpData&> await_data()
    {
        this->release_message();
        return this->data_.read();
    }

    void release_data()
    {
        if (this->data_.is_active()) {
            this->data_.consume();
        }
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    i32 major_version()
    {
        return this->await_message_or_panic().major_version;
    }

    i32 minor_version()
    {
        return this->await_message_or_panic().minor_version;
    }

    const SmallVecBase<HttpHeader>& headers()
    {
        return this->await_message_or_panic().headers;
    }

    Optional<std::string_view> find_header(const std::string_view& name)
    {
        return ::batt::find_header(this->headers(), name);
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    /** \brief Writes this message to the passed stream.
     *
     * This function uses batt::Task::await to block the caller until the message has been fully written to
     * the stream.  Once it returns, it is safe to unpin/delete `this`.
     */
    template <typename AsyncWriteStream>
    Status serialize(AsyncWriteStream& stream)
    {
        StatusOr<Message&> message = this->await_message();
        BATT_REQUIRE_OK(message);

        const std::string message_str = to_string(*message);
        this->release_message();

        StatusOr<HttpData&> data = this->await_data();
        BATT_REQUIRE_OK(data);

        //----- --- -- -  -  -   -
        auto on_scope_exit = finally([&] {
            this->release_data();
        });
        //----- --- -- -  -  -

        StatusOr<usize> bytes_written =               //
            *data                                     //
            | seq::prepend(make_buffer(message_str))  //
            | seq::write_to(stream);

        BATT_REQUIRE_OK(bytes_written);

        return OkStatus();
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -

   protected:
    Status status_{OkStatus()};

    Watch<i32> state_{kCreated};

    Channel<Message> message_;

    Channel<HttpData> data_;
};

}  // namespace batt

#endif  // BATTERIES_HTTP_HTTP_MESSAGE_BASE_HPP
