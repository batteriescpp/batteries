//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2024 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_HTTP_HTTP_SERVER_IMPL_HPP
#define BATTERIES_HTTP_HTTP_SERVER_IMPL_HPP

#include <batteries/config.hpp>

#include <batteries/assert.hpp>
#include <batteries/async/backoff.hpp>
#include <batteries/http/http_server.hpp>
#include <batteries/http/http_server_connection.hpp>

namespace batt {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*explicit*/ HttpServer::HttpServer(boost::asio::io_context& io, HostAddress&& host_address,
                                                     HttpRequestDispatcherFactoryFn&& dispatcher_factory,
                                                     const Optional<HttpServerSettings>& settings) noexcept
    : io_{io}
    , host_address_{std::move(host_address)}
    , dispatcher_factory_{std::move(dispatcher_factory)}
    , settings_{settings.or_else(&HttpServerSettings::with_default_values)}
    , acceptor_{this->io_.get_executor()}
    , connection_tokens_{this->settings_.max_connections}
    , acceptor_task_{io.get_executor(),
                     [this] {
                         this->acceptor_task_main();
                     },
                     "HttpServer::acceptor_task"}
{
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL HttpServer::~HttpServer() noexcept
{
    this->halt();
    this->join();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void HttpServer::halt()
{
    this->halt_requested_.set_value(true);

    boost::system::error_code ec;
    this->acceptor_.close(ec);
    if (ec) {
        BATT_LOG_ERROR() << "Error closing acceptor: " << ec;
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void HttpServer::join()
{
    this->acceptor_task_.join();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void HttpServer::acceptor_task_main()
{
    this->final_status_ = [&]() -> Status {
        BATT_VLOG(1) << "HttpServer::acceptor_task entered";

        BATT_CHECK_EQ(this->host_address_.scheme, "http") << "TODO [tastolfi 2022-05-06] implement https!";

        // Resolve the server endpoint so we can bind our acceptor socket.
        //
        BATT_ASSIGN_OK_RESULT(SmallVec<boost::asio::ip::tcp::endpoint> endpoints,
                              await_resolve(this->io_, this->host_address_));

        if (endpoints.empty()) {
            return StatusCode::kInvalidArgument;
        }
        boost::asio::ip::tcp::endpoint endpoint = endpoints[0];

        {
            boost::system::error_code ec;

            this->acceptor_.open(endpoint.protocol(), ec);
            BATT_REQUIRE_OK(ec);

            this->acceptor_.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true), ec);
            BATT_REQUIRE_OK(ec);

            this->acceptor_.bind(endpoint, ec);
            BATT_REQUIRE_OK(ec);

            this->acceptor_.listen(boost::asio::ip::tcp::socket::max_listen_connections, ec);
            BATT_REQUIRE_OK(ec);
        }

        while (!this->halt_requested_.get_value()) {
            BATT_VLOG(1) << "Waiting for connection";

            IOResult<boost::asio::ip::tcp::socket> socket = Task::await_accept(this->acceptor_);
            BATT_REQUIRE_OK(socket);

            BATT_VLOG(1) << "Client connected; spawning connection task";

            HttpServerConnection::spawn(std::move(*socket), make_copy(this->dispatcher_factory_),
                                        this->settings_);
        }

        return OkStatus();
    }();
}

}  // namespace batt

#endif  // BATTERIES_HTTP_HTTP_SERVER_IMPL_HPP
