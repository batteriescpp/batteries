//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2024 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_HTTP_HTTP_SERVER_SETTINGS_IMPL_HPP
#define BATTERIES_HTTP_HTTP_SERVER_SETTINGS_IMPL_HPP

#include <batteries/config.hpp>

#include <batteries/http/http_server_settings.hpp>

#include <thread>

namespace batt {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL HttpServerSettings HttpServerSettings ::with_default_values() noexcept
{
    return HttpServerSettings{
        .max_connections = HttpServerSettings::kDefaultMaxConnections,
        .max_requests_per_connection = std::thread::hardware_concurrency(),
        .idle_connection_timeout_ms = HttpServerSettings::kDefaultIdleConnectionTimeoutMs,
        .connection_buffer_size = HttpServerSettings::kDefaultConnectionBufferSize,
    };
}

}  //namespace batt

#endif  // BATTERIES_HTTP_HTTP_SERVER_SETTINGS_IMPL_HPP
