//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2024 Anthony Paul Astolfi
//
#include <batteries/http/http_server.hpp>
//
#include <batteries/http/http_server.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <batteries/async/dump_tasks.hpp>
#include <batteries/async/stream_buffer.hpp>
#include <batteries/env.hpp>
#include <batteries/http/http_chunk_encoder.hpp>

namespace {

batt::Status dispatch_empty(batt::HttpRequest& request, batt::HttpResponse& response);
batt::Status dispatch_echo(batt::HttpRequest& request, batt::HttpResponse& response);
batt::Status dispatch_not_found(batt::HttpRequest& request, batt::HttpResponse& response);

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
TEST(HttpServerTest, Test)
{
    batt::enable_dump_tasks();

    const auto varname = "BATT_HTTP_SERVER_TEST_INTERACTIVE";

    if (batt::getenv_as<int>(varname).value_or(0) == 0) {
        BATT_LOG_INFO() << varname << " not set; skipping interactive HttpServerTest.";
        return;
    }

    boost::asio::io_context io;
    batt::HostAddress host_address{
        .scheme = "http",
        .hostname = "localhost",
        .port = 21991,
    };

    batt::HttpServer server{io, batt::make_copy(host_address), /*dispatcher=*/[] {
                                return [](batt::HttpRequest& request,
                                          batt::HttpResponse& response) -> batt::Status {
                                    // Echo request.
                                    //
                                    std::cout << request.method() << " " << request.path() << std::endl;

                                    if (request.path() == "/empty") {
                                        return dispatch_empty(request, response);

                                    } else if (request.path() == "/echo") {
                                        return dispatch_echo(request, response);
                                    }

                                    return dispatch_not_found(request, response);
                                };
                            }};

    BATT_LOG_INFO() << "Server running on " << host_address;

    io.run();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
batt::Status dispatch_empty(batt::HttpRequest& request [[maybe_unused]], batt::HttpResponse& response)
{
    // Send response message.
    {
        pico_http::Response response_message;
        response_message.major_version = 1;
        response_message.minor_version = 1;
        response_message.status = 200;
        response_message.message = "Ok";
        response_message.headers.emplace_back("Content-Length", "0");

        BATT_REQUIRE_OK(response.await_set_message(response_message));
    }

    // Send response data.
    {
        batt::HttpData response_data{
            .source = batt::BufferSource{batt::SingleBufferSource{}},
        };

        BATT_REQUIRE_OK(response.await_set_data(response_data));
    }

    return batt::OkStatus();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
batt::Status dispatch_echo(batt::HttpRequest& request, batt::HttpResponse& response)
{
    // Send response message.
    {
        pico_http::Response response_message;
        response_message.major_version = 1;
        response_message.minor_version = 1;
        response_message.status = 200;
        response_message.message = "Ok";
        response_message.headers.emplace_back("Transfer-Encoding", "chunked");
        response_message.headers.emplace_back(
            "Content-Type", request.find_header("Content-Type").value_or("application/octet-stream"));

        BATT_REQUIRE_OK(response.await_set_message(response_message));
    }

    // Send response data.
    {
        batt::StatusOr<batt::HttpData&> request_data = request.await_data();
        BATT_REQUIRE_OK(request_data);

        BATT_REQUIRE_OK(response.invoke_stream_writer([&](auto& response_stream) -> batt::Status {
            return batt::http_encode_chunked(*request_data, response_stream, batt::IncludeHttpTrailer{true});
        }));
    }

    return batt::OkStatus();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
batt::Status dispatch_not_found(batt::HttpRequest& request [[maybe_unused]], batt::HttpResponse& response)
{
    // Send response message.
    {
        pico_http::Response response_message;
        response_message.major_version = 1;
        response_message.minor_version = 1;
        response_message.status = 404;
        response_message.message = "Not Found";
        response_message.headers.emplace_back("Content-Length", "0");

        BATT_REQUIRE_OK(response.await_set_message(response_message));
    }

    // Send response data.
    {
        batt::HttpData response_data{
            .source = batt::BufferSource{batt::SingleBufferSource{}},
        };

        BATT_REQUIRE_OK(response.await_set_data(response_data));
    }

    return batt::OkStatus();

    return batt::OkStatus();
}

}  // namespace
