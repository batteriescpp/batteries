//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2024 Anthony Paul Astolfi
//
#include <batteries/config.hpp>

#if !BATT_HEADER_ONLY

#include <batteries/http/http_server_settings.hpp>
//
#include <batteries/http/http_server_settings_impl.hpp>

#endif  // !BATT_HEADER_ONLY
