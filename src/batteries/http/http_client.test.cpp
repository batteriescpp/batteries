//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2022-2024 Anthony Paul Astolfi
//
#include <batteries/http/http_client.hpp>
//
#include <batteries/http/http_client.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <batteries/async/dump_tasks.hpp>
#include <batteries/async/runtime.hpp>
#include <batteries/env.hpp>

#include <boost/asio/bind_executor.hpp>
#include <boost/asio/ssl/stream.hpp>
#include <boost/asio/strand.hpp>

#include <sstream>
#include <thread>
#include <vector>

namespace {

TEST(HttpClientTest, Test)
{
#if BATT_PLATFORM_IS_APPLE
    // https://gitlab.com/tonyastolfi/batteries/-/issues/5
    return;
#endif  // BATT_PLATFORM_IS_APPLE

    constexpr int kNumRequestsToSend = 5;
    constexpr int kTestIdleConnectionTimeoutMs = 2500;

    batt::enable_dump_tasks();

    batt::Task main_task{
        //
        batt::Runtime::instance().schedule_task(),
        [] {
            // Set an idle connection timeout short enough for the test to wait after all requests for the
            // connections to shut down.
            //
            batt::DefaultHttpClient::get().set_default_connection_timeout_ms(kTestIdleConnectionTimeoutMs);

            // Test Connection: close and Connection: keep-alive.
            //
            for (const char* connection_setting : {"close", "keep-alive"}) {
                // Test both TCP and SSL connections.
                //
                for (const char* scheme : {"http", "https"}) {
                    // Send many requests per outer loop.
                    //
                    for (int i = 0; i < kNumRequestsToSend; ++i) {
                        BATT_VLOG(1) << BATT_INSPECT(i) << BATT_INSPECT(connection_setting)
                                     << BATT_INSPECT(scheme);

                        // Send a GET.
                        //
                        batt::StatusOr<std::unique_ptr<batt::HttpResponse>> result =
                            batt::http_get(batt::to_string(scheme, "://www.google.com/"),
                                           batt::HttpHeader{"Connection", connection_setting});

                        EXPECT_TRUE(result.ok());

                        BATT_VLOG(1) << BATT_INSPECT(result.status());

                        // Wait for the response message (header) to be available.
                        //
                        batt::StatusOr<batt::HttpResponse::Message&> response_message =
                            result->get()->await_message();

                        ASSERT_TRUE(response_message.ok());

                        BATT_VLOG(1) << BATT_INSPECT(response_message.status())
                                     << BATT_INSPECT(*response_message);

                        // As soon as we ask for the data portion of the response, it signals to the client
                        // that we are done reading the message (header).
                        //
                        batt::StatusOr<batt::HttpData&> response_data = result->get()->await_data();

                        ASSERT_TRUE(response_data.ok());

                        // Capture the entire response in a buffer.
                        //
                        std::ostringstream oss;
                        batt::Status status = *response_data | batt::seq::print_out(oss);

                        ASSERT_TRUE(status.ok());

                        BATT_VLOG(1) << "Read response data: " << oss.str().size() << " bytes.";
                        //
                        // This request was a success!
                    }
                }
            }

            BATT_VLOG(1) << "Done with the test... sleeping for a while to let connections time out";

            batt::HostAddress host_address_http{
                .scheme = "http",
                .hostname = "www.google.com",
                .port = batt::None,
            };

            ASSERT_GT(batt::DefaultHttpClient::get().count_active_connections_for_host(host_address_http),
                      0u);

            batt::HostAddress host_address_https{
                .scheme = "https",
                .hostname = "www.google.com",
                .port = batt::None,
            };

            ASSERT_GT(batt::DefaultHttpClient::get().count_active_connections_for_host(host_address_https),
                      0u);

            while (batt::DefaultHttpClient::get().count_active_connections_for_host(host_address_http) > 0 &&
                   batt::DefaultHttpClient::get().count_active_connections_for_host(host_address_https) > 0) {
                batt::Task::sleep(boost::posix_time::milliseconds(100));
            }
        },
        "main_task"};

    main_task.join();
}

}  // namespace
