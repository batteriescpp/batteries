//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2024 Anthony Paul Astolfi
//
#include <batteries/config.hpp>
//
#pragma once
#ifndef BATTERIES_HTTP_SSL_TRANSPORT_HPP
#define BATTERIES_HTTP_SSL_TRANSPORT_HPP

#include <batteries/http/http_client_host_context_decl.hpp>

#include <batteries/async/handler.hpp>
#include <batteries/async/task.hpp>

#include <batteries/asio/io_context.hpp>
#include <batteries/asio/ip_tcp.hpp>

#include <batteries/int_types.hpp>

#include <boost/asio/bind_executor.hpp>
#include <boost/asio/dispatch.hpp>
#include <boost/asio/ssl/stream.hpp>
#include <boost/asio/strand.hpp>

#include <atomic>
#include <functional>

namespace batt {

/** \brief An SSL/TLS based transport for HTTPS clients.
 */
class SslTransport
{
   public:
    /** \brief Construct a new SslTransport.  The SSL init fn from the passed `host_context` is used to
     * instantiate and configure the `boost::asio::ssl::context` that will be used.
     */
    explicit SslTransport(HttpClientHostContext& host_context) noexcept;

    /** \brief Returns the bound address of the local end of the underlying TCP socket, if connected;
     * otherwise returns an undefined value and sets `ec` to an error code.
     */
    boost::asio::ip::tcp::endpoint local_endpoint(ErrorCode& ec) const noexcept;

    /** \brief Returns the bound address of the remote peer end of the underlying TCP socket, if connected;
     * otherwise returns an undefined value and sets `ec` to an error code.
     */
    boost::asio::ip::tcp::endpoint remote_endpoint(ErrorCode& ec) const noexcept;

    /** \brief Returns true iff the socket is connected and SSL shutdown has not been initiated.
     */
    bool is_open() const noexcept;

    /** \brief Shuts down the SSL stream (gracefully), then closes the socket.
     *
     * `this->is_open()` will return false immediately upon entering `close`, even if the SSL protocol
     * shutdown is still running.
     */
    void close(ErrorCode& ec) noexcept;

    /** \brief Shuts down the socket in one or both directions.  Does *not* perform SSL protocol shutdown, as
     * this is bi-directional.
     */
    void shutdown(boost::asio::socket_base::shutdown_type mode, ErrorCode& ec) noexcept;

    /** \brief Initiates an asynchronous connection and SSL handshake.
     */
    template <typename Handler>
    void async_connect(const boost::asio::ip::tcp::endpoint& ep, Handler&& handler);

    /** \brief Initiates an asynchronous read from the stream.
     */
    template <typename MutableBufferSequence, typename Handler>
    void async_read_some(MutableBufferSequence&& buffers, Handler&& handler);

    /** \brief Initiates an asynchronous write to the stream.
     */
    template <typename ConstBufferSequence, typename Handler>
    void async_write_some(ConstBufferSequence&& buffers, Handler&& handler);

    //+++++++++++-+-+--+----- --- -- -  -  -   -
   private:
    /** \brief The execution context for this transport.
     */
    boost::asio::io_context& io_;

    /** \brief A unique, monotonic id for this object; used for debugging.
     */
    const i64 stream_id_;

    /** \brief Flag that tracks whether close() has been entered, to avoid duplicate work and allow `is_open`
     * to immediately reflect when the transport is closed.
     */
    std::atomic<bool> close_initiated_;

    /** \brief The SSL context for this stream.
     */
    Optional<boost::asio::ssl::context> ssl_context_;

    /** \brief The SSL/TLS encrypted async stream.
     */
    boost::asio::ssl::stream<boost::asio::ip::tcp::socket> ssl_stream_;

    /** \brief The underlying socket object (contained within `this->ssl_stream_`).
     */
    boost::asio::basic_socket<boost::asio::ip::tcp>& socket_;

    /** \brief Used to serialize all access to the state of this object, both from external member function
     * calls and composed async operations.
     */
    boost::asio::strand<boost::asio::any_io_executor> strand_;
};

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename Handler>
inline void SslTransport::async_connect(const boost::asio::ip::tcp::endpoint& ep, Handler&& handler)
{
    boost::asio::dispatch(
        this->strand_,
        bind_handler(
            BATT_FORWARD(handler),  //
            [this, ep](auto&& handler) {
                //----- --- -- -  -  -   -
                // On strand.
                //
                this->socket_.async_connect(
                    ep, bind_executor(
                            this->strand_,
                            bind_handler(BATT_FORWARD(handler),  //
                                         [this](auto&& handler, const ErrorCode& ec) {
                                             //----- --- -- -  -  -   -
                                             // On strand.
                                             //
                                             if (ec) {
                                                 // Because this is a caller-supplied handler, we must
                                                 // call it in the "proper" way by extracting its
                                                 // associated executor and calling post, binding the
                                                 // completion args.
                                                 //
                                                 post_handler(BATT_FORWARD(handler), ec);

                                             } else {
                                                 this->ssl_stream_.async_handshake(
                                                     boost::asio::ssl::stream_base::client,
                                                     bind_executor(
                                                         this->strand_,
                                                         bind_handler(
                                                             BATT_FORWARD(handler),
                                                             [this](auto&& handler, const ErrorCode& ec) {
                                                                 //----- --- -- -  -  -   -
                                                                 // On strand.

                                                                 // If the handshake failed, then close
                                                                 // the socket.
                                                                 //
                                                                 if (ec) {
                                                                     ErrorCode close_ec;
                                                                     this->socket_.close(close_ec);
                                                                 }

                                                                 // Reset the close flag.
                                                                 //
                                                                 this->close_initiated_.store(false);

                                                                 // See comment above re: post_handler vs
                                                                 // directly calling `handler`.
                                                                 //
                                                                 post_handler(BATT_FORWARD(handler), ec);
                                                             })));
                                             }
                                         })));
            }));
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename MutableBufferSequence, typename Handler>
inline void SslTransport::async_read_some(MutableBufferSequence&& buffers, Handler&& handler)
{
    boost::asio::dispatch(
        this->strand_,
        bind_handler(BATT_FORWARD(handler), [this, buffers = BATT_FORWARD(buffers)](auto&& handler) mutable {
            //----- --- -- -  -  -   -
            // On strand.
            //
            this->ssl_stream_.async_read_some(std::move(buffers),
                                              bind_executor(this->strand_, BATT_FORWARD(handler)));
        }));
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename ConstBufferSequence, typename Handler>
inline void SslTransport::async_write_some(ConstBufferSequence&& buffers, Handler&& handler)
{
    boost::asio::dispatch(
        this->strand_,
        bind_handler(BATT_FORWARD(handler), [this, buffers = BATT_FORWARD(buffers)](auto&& handler) mutable {
            //----- --- -- -  -  -   -
            // On strand.
            //
            this->ssl_stream_.async_write_some(std::move(buffers),
                                               bind_executor(this->strand_, BATT_FORWARD(handler)));
        }));
}

}  //namespace batt

#endif  // BATTERIES_HTTP_SSL_TRANSPORT_HPP

#if BATT_HEADER_ONLY
#include <batteries/http/ssl_transport_impl.hpp>
#endif  // BATT_HEADER_ONLY
