//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2024 Anthony Paul Astolfi
//
#include <batteries/config.hpp>

#if !BATT_HEADER_ONLY

#include <batteries/http/http_server.hpp>
//
#include <batteries/http/http_server_impl.hpp>

#endif  // !BATT_HEADER_ONLY
