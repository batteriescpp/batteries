#include <batteries/skip_list.hpp>
//
#include <batteries/skip_list.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <batteries/stream_util.hpp>

#include <ostream>

namespace {

using namespace batt::int_types;

template <typename T>
auto dump_skip_list(T* sl)
{
    return [sl](std::ostream& out) {
        out << "levels=" << std::bitset<16>{sl->active_levels_} << std::endl;
        if (sl->height() == 0) {
            out << "[1] --> null" << std::endl;
            return;
        }
        for (i32 level = 0; level < sl->height(); ++level) {
            out << "[" << (level + 1) << "]: ";
            for (auto node = sl->head_.get_next(level); node && *node; node = (*node)->get_next(level)) {
                out << "(" << (*node)->key_ << ") --> ";
            }
            out << "null" << std::endl;
        }
    };
}

TEST(SkipListTest, Test)
{
    batt::SkipList<int, int> sl;
    sl.check_invariants();

    BATT_LOG_INFO() << BATT_INSPECT(sl.height());

    for (int n = 1; n < 14; n += 2) {
        auto* node = sl.emplace(n, n * 2 + 1);
        BATT_LOG_INFO() << BATT_INSPECT(n) << BATT_INSPECT(node->height_) << BATT_INSPECT(sl.height())
                        << std::endl
                        << "Inserted (" << n << "):\n"
                        << dump_skip_list(&sl) << std::endl;

        sl.check_invariants();

        auto* found = sl.find(n);
        sl.check_invariants();
        EXPECT_EQ(node, found);

        auto* updated = sl.emplace(n, -n);
        sl.check_invariants();
        EXPECT_EQ(updated, found);
        EXPECT_EQ(*updated->key_, n);
        EXPECT_EQ(*updated->value_, -n);

        auto* pred = sl.find(n - 1);
        sl.check_invariants();
        ASSERT_NE(pred, nullptr);
        if (n > 1) {
            EXPECT_EQ(*pred->key_, n - 2);
        } else {
            EXPECT_FALSE(pred->key_);
        }
    }

    for (int n = 0; n < 14; n += 2) {
        auto* node = sl.emplace(n, n * 2 + 1);
        BATT_LOG_INFO() << BATT_INSPECT(n) << BATT_INSPECT(node->height_) << BATT_INSPECT(sl.height())
                        << std::endl
                        << "Inserted (" << n << "):\n"
                        << dump_skip_list(&sl) << std::endl;

        sl.check_invariants();

        auto* found = sl.find(n);
        sl.check_invariants();
        EXPECT_EQ(node, found);

        auto* updated = sl.emplace(n, -n);
        sl.check_invariants();
        EXPECT_EQ(updated, found);
        EXPECT_EQ(*updated->key_, n);
        EXPECT_EQ(*updated->value_, -n);
    }

    for (int i = 0; i < 14; ++i) {
        int n = (i * 23) % 14;

        auto* found = sl.find(n);
        sl.check_invariants();
        EXPECT_NE(found, nullptr);

        bool erased = sl.erase(n);
        sl.check_invariants();
        EXPECT_TRUE(erased);

        BATT_LOG_INFO() << std::endl << "Erased (" << n << "):\n" << dump_skip_list(&sl) << std::endl;

        auto* found2 = sl.find(n);
        sl.check_invariants();
        if (found2) {
            if (found2->key_) {
                EXPECT_NE(*found2->key_, n);
            }
        }

        bool erased2 = sl.erase(n);
        sl.check_invariants();
        EXPECT_FALSE(erased2);
    }

    sl.check_invariants();
}

}  // namespace
