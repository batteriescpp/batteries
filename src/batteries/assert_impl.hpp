#pragma once
#ifndef BATTERIES_ASSERT_IMPL_HPP
#define BATTERIES_ASSERT_IMPL_HPP

#include <batteries/stacktrace.hpp>

#include <atomic>
#include <chrono>
#include <iostream>
#include <thread>

namespace batt {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_NORETURN BATT_INLINE_IMPL void fail_check_exit()
{
    const bool previously_entered = fail_check_exit_entered().exchange(true);
    BATT_FAIL_CHECK_OUT << std::endl << std::endl;

    if (!previously_entered) {
        print_stack_trace();
    }

    // Give the stack trace a chance to be printed before aborting the process.
    //
    using namespace std::chrono_literals;
    std::this_thread::sleep_for(200ms);

    std::abort();
    BATT_UNREACHABLE();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL std::atomic<bool>& fail_check_exit_entered()
{
    static std::atomic<bool> entered_{false};
    return entered_;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL std::atomic<bool>& fail_check_spin_lock()
{
    static std::atomic<bool> spin_lock_{false};
    return spin_lock_;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL bool lock_fail_check_mutex()
{
    thread_local bool locked_by_this_thread = false;

    std::atomic<bool>& spin_lock = fail_check_spin_lock();

    // If we re-enter this function on the same thread, then disable the nested CHECKs.
    //
    if (locked_by_this_thread) {
        return false;
    }

    const bool previously_locked = spin_lock.exchange(true);

    // If the lock is already held by some other thread, then park this thread and wait for the end.
    //
    if (previously_locked) {
        for (;;) {
            using namespace std::chrono_literals;
            std::this_thread::sleep_for(10s);
        }
    }

    // Prevent nested CHECK fails.
    //
    locked_by_this_thread = true;

    return true;
}

}  //namespace batt

#endif  // BATTERIES_ASSERT_IMPL_HPP
