//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2022 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_QUEUE_HPP
#define BATTERIES_ASYNC_QUEUE_HPP

#include <batteries/config.hpp>
//
#include <batteries/assert.hpp>
#include <batteries/async/mutex.hpp>
#include <batteries/async/watch.hpp>
#include <batteries/do_nothing.hpp>
#include <batteries/finally.hpp>
#include <batteries/status.hpp>
#include <batteries/utility.hpp>

#include <deque>

namespace batt {

/** \brief Type-agnostic base class for all Queue types.
 *
 * \see Queue
 */
class QueueBase
{
   public:
    /** \brief Tests whether the Queue is in an open state.
     */
    bool is_open() const
    {
        return !this->pending_count_.is_closed();
    }

    /** \brief Tests whether the Queue is in a closed state.
     */
    bool is_closed() const
    {
        return !this->is_open();
    }

    /** \brief The number of items currently in the Queue.
     */
    i64 size() const
    {
        return this->pending_count_.get_value();
    }

    /** \brief Tests whether this->size() is zero.
     */
    bool empty() const
    {
        return this->size() == 0;
    }

    /** \brief Blocks the current Task/thread until this->size() satisfies the given predicate.
     */
    template <typename Predicate = bool(i64)>
    StatusOr<i64> await_size_is_truly(Predicate&& predicate)
    {
        return this->pending_count_.await_true(BATT_FORWARD(predicate));
    }

    /** \brief Blocks the current Task/thread until the Queue is empty.
     */
    StatusOr<i64> await_empty()
    {
        return this->await_size_is_truly([](i64 count) {
            BATT_ASSERT_GE(count, 0);
            return count == 0;
        });
    }

    /** \brief Wakes all waiters with StatusCode::kPoke.
     */
    void poke()
    {
        this->pending_count_.poke();
    }

   protected:
    Status await_one() noexcept
    {
        StatusOr<i64> prior_count = this->pending_count_.await_modify(&decrement_if_positive);
        BATT_REQUIRE_OK(prior_count);

        BATT_CHECK_GT(*prior_count, 0);

        return OkStatus();
    }

    StatusOr<i64> await_count_range(i64 min_count, i64 max_count) noexcept
    {
        i64 modified_count = 0;

        StatusOr<i64> prior_count = this->pending_count_.await_modify([&](i64 n) -> Optional<i64> {
            if (n < min_count) {
                return None;
            }
            if (n > max_count) {
                modified_count = n - max_count;
            } else {
                modified_count = 0;
            }
            return modified_count;
        });

        BATT_REQUIRE_OK(prior_count);

        const i64 claimed_count = *prior_count - modified_count;

        BATT_CHECK_GE(claimed_count, min_count) << BATT_INSPECT(*prior_count) << BATT_INSPECT(modified_count);
        BATT_CHECK_LE(claimed_count, max_count) << BATT_INSPECT(*prior_count) << BATT_INSPECT(modified_count);

        return claimed_count;
    }

    bool try_acquire() noexcept
    {
        Optional<i64> prior_count = this->pending_count_.modify_if(&decrement_if_positive);
        if (!prior_count) {
            return false;
        }
        BATT_CHECK_GT(*prior_count, 0);
        return true;
    }

    void notify(i64 count)
    {
        this->pending_count_.fetch_add(count);
    }

    void close_impl() noexcept
    {
        this->pending_count_.close();
    }

   private:
    static Optional<i64> decrement_if_positive(i64 n) noexcept
    {
        if (n > 0) {
            return n - 1;
        }
        return None;
    }

    Watch<i64> pending_count_{0};
};

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------
/** \brief Unbounded multi-producer/multi-consumer (MPMC) FIFO queue.
 *
 * \see QueueBase
 */
template <typename T>
class Queue : public QueueBase
{
   public:
    /** \brief Closes the queue, causing all current/future read operations to fail/unblock immediately.
     */
    void close()
    {
        ScopedLock<std::deque<T>> locked{this->pending_items_};
        this->close_impl();
    }

    /** \brief Emplaces a single instance of T into the back of the queue using the passed arguments.
     *
     * \return true if the push succeeded; false if the queue was closed.
     */
    template <typename... Args>
    Status push(Args&&... args)
    {
        {
            ScopedLock<std::deque<T>> locked{this->pending_items_};
            if (!this->is_open()) {
                return {StatusCode::kClosed};
            }
            locked->emplace_back(BATT_FORWARD(args)...);
        }
        this->notify(1);
        return OkStatus();
    }

    /** \brief Atomically invokes the factory_fn to create an instance of T to push into the Queue.
     */
    template <typename FactoryFn>
    Status push_with_lock(FactoryFn&& factory_fn)
    {
        {
            ScopedLock<std::deque<T>> locked{this->pending_items_};
            if (!this->is_open()) {
                return {StatusCode::kClosed};
            }
            locked->emplace_back(BATT_FORWARD(factory_fn)());
        }
        this->notify(1);
        return OkStatus();
    }

    /** \brief Inserts multiple items atomically into the Queue.
     *
     * Queue::push_all guarantees that the passed items will be inserted in the given order, with no other
     * items interposed (via concurrent calls to Queue::push or similar).
     *
     * `items` should be an STL sequence; i.e., something that can be iterated via a for-each loop.
     */
    template <typename Items>
    Status push_all(Items&& items)
    {
        auto first = std::begin(items);
        auto last = std::end(items);

        const usize count = std::distance(first, last);

        bool closed = false;

        this->pending_items_.with_lock([&](auto& pending) {
            if (!this->is_open()) {
                closed = true;
            } else {
                pending.insert(pending.end(), first, last);
            }
        });

        if (closed) {
            return {StatusCode::kClosed};
        }

        this->notify(count);

        return OkStatus();
    }

    /** \brief Move-copies multiple items atomically into the Queue.
     */
    template <typename Items>
    Status consume_all(Items&& items)
    {
        auto first = std::begin(items);
        auto last = std::end(items);

        const usize count = std::distance(first, last);

        bool closed = false;

        this->pending_items_.with_lock([&](auto& pending) {
            if (!this->is_open()) {
                closed = true;
            } else {
                pending.insert(pending.end(),                   //
                               std::make_move_iterator(first),  //
                               std::make_move_iterator(last));
            }
        });

        if (closed) {
            return {StatusCode::kClosed};
        }

        this->notify(count);

        return OkStatus();
    }

    /** \brief Reads a single item from the Queue.
     *
     * Blocks until an item is available or the Queue is closed.
     */
    StatusOr<T> await_next()
    {
        Status acquired = this->await_one();
        BATT_REQUIRE_OK(acquired);

        return this->pop_next_or_panic();
    }

    /** \brief Reads a batch of items form the Queue.
     *
     * \return The end iterator for the read sequence on success, error status code otherwise.
     */
    template <typename Iter>
    StatusOr<Iter> await_some(Iter&& first_out, Iter&& last_out)
    {
        const i64 max_count = std::distance(first_out, last_out);

        StatusOr<i64> claimed_count = this->await_count_range(/*min_count=*/1, max_count);
        BATT_REQUIRE_OK(claimed_count);

        const usize n_to_pop = BATT_CHECKED_CAST(usize, *claimed_count);
        {
            ScopedLock<std::deque<T>> locked{this->pending_items_};

            BATT_CHECK_GE(locked->size(), n_to_pop);

            auto first = locked->begin();
            auto last = std::next(first, n_to_pop);

            std::copy(std::make_move_iterator(first),  //
                      std::make_move_iterator(last), first_out);

            locked->erase(first, last);
        }
        return std::next(first_out, n_to_pop);
    }

    /** \brief Attempts to read a single item from the Queue (non-blocking).
     *
     * \return The extracted item if successful; \ref batt::None otherwise
     */
    Optional<T> try_pop_next()
    {
        if (!this->try_acquire()) {
            return None;
        }
        return this->pop_next_or_panic();
    }

    /** \brief Reads a single item from the Queue (non-blocking), panicking if the Queue is empty.
     */
    T pop_next_or_panic()
    {
        ScopedLock<std::deque<T>> locked{this->pending_items_};
        BATT_CHECK(!locked->empty()) << "pop_next_or_panic FAILED because the queue is empty";

        auto on_return = batt::finally([&] {
            locked->pop_front();
        });

        return std::forward<T>(locked->front());
    }

    /** \brief Reads and discards items from the Queue until it is observed to be empty.
     *
     * \param cleanup_fn (Optional) A function to run on each drained item in the queue
     *
     * \return the number of items read.
     */
    template <typename CleanupFn = DoNothing>
    usize drain(CleanupFn&& cleanup_fn = {})
    {
        usize count = 0;
        for (;;) {
            Optional<T> next = this->try_pop_next();
            if (!next) {
                break;
            }
            ++count;
            try {
                cleanup_fn(*next);
            } catch (...) {
                continue;
            }
        }
        return count;
    }

   private:
    Mutex<std::deque<T>> pending_items_;
};

}  // namespace batt

#endif  // BATTERIES_ASYNC_QUEUE_HPP
