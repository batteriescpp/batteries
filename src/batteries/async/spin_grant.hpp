//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2024 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_SPIN_GRANT_HPP
#define BATTERIES_ASYNC_SPIN_GRANT_HPP

#include <batteries/config.hpp>

#include <batteries/async/spin_grant_decl.hpp>

#if BATT_HEADER_ONLY
#include <batteries/async/spin_grant_impl.hpp>
#endif

#endif  // BATTERIES_ASYNC_SPIN_GRANT_HPP
