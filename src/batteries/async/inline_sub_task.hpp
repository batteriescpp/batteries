//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2024 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_INLINE_SUB_TASK_HPP
#define BATTERIES_ASYNC_INLINE_SUB_TASK_HPP

#include <batteries/config.hpp>
//
#include <batteries/async/async_run_task.hpp>
#include <batteries/async/handler.hpp>
#include <batteries/async/task.hpp>
#include <batteries/async/watch.hpp>

#include <batteries/constants.hpp>
#include <batteries/int_types.hpp>

#include <atomic>

namespace batt {

/** \brief A scoped asynchronous task with inline stack memory.
 *
 * This class does not allocate any dynamic memory for the sub-task.
 */
template <usize kStackBufferSize>
class InlineSubTask
{
   public:
    static constexpr usize kTaskStackOverhead = 4 * kKiB;

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    /** \brief Creates a new InlineSubTask.  The arguments passed to this constructor are the same as those
     * that would be passed directly to batt::Task::Task.
     */
    template <typename BodyFn, typename... TaskArgs>
    explicit InlineSubTask(const boost::asio::any_io_executor& ex, BodyFn&& body_fn,
                           TaskArgs&&... task_args) noexcept
        : task_{async_run_task(                                  //
              ex,                                                //
              StackSize{kStackBufferSize - kTaskStackOverhead},  //
              BATT_FORWARD(body_fn),                             //
              Task::GetIsStarted{&this->is_started_},            //
              BATT_FORWARD(task_args)...,                        //
              /*completion_handler=*/[this] {
                  BATT_CHECK(!this->handler_memory_.in_use());
                  {
                      HandlerList<> local_handlers;
                      std::swap(local_handlers, this->completion_handlers_);
                      invoke_all_handlers(&local_handlers);
                  }
                  this->completed_.set_value_and_release(true);
              })}
    {
        // If the task was started in the ctor, then don't save `this->task_`, since it may dangle at any
        // moment!
        //
        if (this->is_started_) {
            this->task_ = nullptr;
        } else {
            BATT_CHECK(!this->completed_.get_value());
        }
    }

    //----- --- -- -  -  -   -
    InlineSubTask(const InlineSubTask&) = delete;
    InlineSubTask& operator=(const InlineSubTask&) = delete;
    //----- --- -- -  -  -   -

    /** \brief Automatically joins to the sub-task, blocking until it has completed.
     */
    ~InlineSubTask() noexcept
    {
        this->join();
    }

    /** \brief Adds a completion handler to the task; the task must be created with
     * batt::Task::DeferStart{true}, and must not have been started yet.
     */
    template <typename F = void()>
    void call_when_done(F&& handler)
    {
        BATT_CHECK(!this->is_started())
            << "InlineSubTask::call_when_done must be called _before_ the task is started!";

        push_handler(&this->completion_handlers_, BATT_FORWARD(handler));
    }

    /** \brief Starts the sub-task if batt::Task::DeferStart{true} was passed in at construction time;
     * otherwise does nothing.
     */
    void start() noexcept
    {
        if (this->task_) {
            Task* local_task = nullptr;
            std::swap(local_task, this->task_);
            local_task->start();
        }
    }

    /** \brief Returnes true if the task has been started.
     */
    bool is_started() const noexcept
    {
        return this->task_ == nullptr;
    }

    /** \brief Blocks the caller until the sub-task has completed.
     */
    void join() noexcept
    {
        BATT_CHECK_OK(this->completed_.await_equal(true));
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -
   private:
    /** \brief Used to signal that the task is done executing and this->handler_memory_ is no longer in use.
     */
    Watch<bool> completed_{false};

    /** \brief Set to indicate the task was started already.
     */
    bool is_started_ = false;

    /** \brief Retain a pointer to the created task object to start the task if DeferStart{true} was passed in
     * at construction time.
     */
    Task* task_ = nullptr;

    /** \brief Completion handlers that have been installed; we must run these strictly *before* setting
     * this->completed_ to true.
     */
    HandlerList<> completion_handlers_;

    /** \brief The inline memory buffer used for the batt::Task object and its call stack.
     */
    HandlerMemory<kStackBufferSize> handler_memory_;
};

}  //namespace batt

#endif  // BATTERIES_ASYNC_INLINE_SUB_TASK_HPP
