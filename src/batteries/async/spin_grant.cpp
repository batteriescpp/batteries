//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023-2024 Anthony Paul Astolfi
//
#include <batteries/config.hpp>

#if !BATT_HEADER_ONLY

#include <batteries/async/spin_grant.hpp>
//
#include <batteries/async/spin_grant_impl.hpp>

#endif  // !BATT_HEADER_ONLY
