//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2022 Anthony Paul Astolfi
//
#include <batteries/config.hpp>

#include <batteries/async/task_decl.hpp>

#include <batteries/async/task.ipp>

#if BATT_HEADER_ONLY
#include <batteries/async/task_impl.hpp>
#endif
