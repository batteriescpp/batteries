//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2024 Anthony Paul Astolfi
//
#include <batteries/async/types.hpp>
//
#include <batteries/async/types.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <batteries/status.hpp>

#include <sstream>

namespace {

TEST(WaitForResourceTest, OStreamPrint)
{
    {
        std::ostringstream oss;
        oss << batt::WaitForResource::kFalse;

        EXPECT_THAT(oss.str(), ::testing::StrEq("WaitForResource::kFalse"));
    }
    {
        std::ostringstream oss;
        oss << batt::WaitForResource::kTrue;

        EXPECT_THAT(oss.str(), ::testing::StrEq("WaitForResource::kTrue"));
    }
    {
        std::ostringstream oss;
        oss << (batt::WaitForResource)7;

        EXPECT_THAT(oss.str(), ::testing::StrEq("WaitForResource::kTrue"));
    }
}

}  // namespace
