//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2024 Anthony Paul Astolfi
//
#include <batteries/async/scalable_grant.hpp>
//
#include <batteries/async/scalable_grant.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <batteries/async/dump_tasks.hpp>
#include <batteries/async/fake_execution_context.hpp>
#include <batteries/async/fake_executor.hpp>
#include <batteries/async/spin_grant.hpp>
#include <batteries/async/task.hpp>

#include <batteries/asio/io_context.hpp>

#include <chrono>

namespace {

using namespace batt::int_types;

using Grant = batt::ScalableGrant;
using Issuer = batt::ScalableGrant::Issuer;

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename GrantT>
double run_scalability_test(usize n_tasks, usize pool_size, usize grants_per_task, usize grant_hold_time)
{
    BATT_CHECK_GE(pool_size, 4);

    boost::asio::io_context context;

    typename GrantT::Issuer issuer{pool_size};

    std::vector<std::unique_ptr<batt::Task>> tasks;
    for (usize i = 0; i < n_tasks; ++i) {
        tasks.emplace_back(std::make_unique<batt::Task>(
            context.get_executor(), [i, &issuer, grants_per_task, grant_hold_time] {
                for (usize j = 0; j < grants_per_task; ++j) {
                    u64 n = ((i + j) & 0b11) + 1;

                    BATT_DEBUG_INFO(BATT_INSPECT(n) << BATT_INSPECT(i) << BATT_INSPECT(j)
                                                    << BATT_INSPECT(issuer.available()));

                    batt::StatusOr<GrantT> g = issuer.issue_grant(n, batt::WaitForResource::kTrue);
                    BATT_CHECK_OK(g);

                    for (usize k = 0; k < grant_hold_time; ++k) {
                        batt::Task::yield();
                    }
                }
            }));
    }

    auto start = std::chrono::steady_clock::now();

    const usize n_threads = std::thread::hardware_concurrency();
    std::vector<std::thread> threads;
    for (usize i = 0; i < n_threads; ++i) {
        threads.emplace_back([&] {
            context.run();
        });
    }

    for (auto& thread : threads) {
        thread.join();
    }

    double elapsed_usec =
        std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - start)
            .count();

    for (auto& p_task : tasks) {
        p_task->join();
    }

    double total_grants = grants_per_task * n_tasks;
    double rate = total_grants / (elapsed_usec / 1000000.0);

    std::cout << BATT_INSPECT(n_tasks) << BATT_INSPECT(rate) << " " << batt::name_of<GrantT>() << std::endl;
    return rate;
}

TEST(AsyncScalableGrantTest, ScalabilityBenchmark)
{
    (void)batt::Status{batt::StatusCode::kInternal};

    batt::enable_dump_tasks();

    Issuer issuer{10};

    batt::StatusOr<Grant> g1 = issuer.issue_grant(3, batt::WaitForResource::kTrue);

    ASSERT_TRUE(g1.ok()) << BATT_INSPECT(g1.status());

    for (usize n_tasks : {1, 2, 5, 10, 25, 50, 100, 200, 400, 800}) {
        const usize pool_size = std::max<usize>(std::min<usize>(n_tasks / 2, 20), 5);
        const usize grants_per_task = 250;
        const usize grant_hold_time = 5;

        double spin_rate =
            run_scalability_test<batt::SpinGrant>(n_tasks, pool_size, grants_per_task, grant_hold_time);
        double scalable_rate =
            run_scalability_test<batt::ScalableGrant>(n_tasks, pool_size, grants_per_task, grant_hold_time);

        std::cout << BATT_INSPECT(scalable_rate / spin_rate) << BATT_INSPECT(pool_size)
                  << BATT_INSPECT(grants_per_task) << BATT_INSPECT(grant_hold_time) << std::endl
                  << std::endl;
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
//  1. Grant is empty, size is 0, is not valid, is not revoked.
//
TEST(AsyncScalableGrantTest, EmptyGrant)
{
    batt::ScalableGrant::Issuer issuer{0};
    batt::StatusOr<batt::ScalableGrant> grant = issuer.issue_grant(0, batt::WaitForResource::kFalse);

    ASSERT_TRUE(grant.ok());
    EXPECT_EQ(grant->get_issuer(), &issuer);
    EXPECT_TRUE(grant->empty());
    EXPECT_FALSE(grant->is_valid());
    EXPECT_FALSE(*grant);
    EXPECT_FALSE(grant->is_revoked());
    EXPECT_EQ(grant->size(), 0u);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
//  2. Issue a grant
//     a. wait = false, pool is large enough
//     c. wait = true, pool is large enough
//
TEST(AsyncScalableGrantTest, IssueGrantSuccess)
{
    batt::ScalableGrant::Issuer issuer{10};
    EXPECT_EQ(const_cast<const batt::ScalableGrant::Issuer&>(issuer).available(), 10u);

    for (batt::WaitForResource wait_for_resource :
         {batt::WaitForResource::kFalse, batt::WaitForResource::kTrue}) {
        for (usize count = 0; count <= 10; ++count) {
            {
                batt::StatusOr<batt::ScalableGrant> grant = issuer.issue_grant(count, wait_for_resource);
                EXPECT_EQ(issuer.available(), 10u - count);

                ASSERT_TRUE(grant.ok());
                EXPECT_EQ(grant->size(), count);
            }
            EXPECT_EQ(issuer.available(), 10u);
        }
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
//  2. Issue a grant
//     b. wait = false, pool is not large enough
//
TEST(AsyncScalableGrantTest, IssueGrantNoWaitNotAvailable)
{
    batt::ScalableGrant::Issuer issuer{10};
    batt::StatusOr<batt::ScalableGrant> grant = issuer.issue_grant(11, batt::WaitForResource::kFalse);

    EXPECT_FALSE(grant.ok());
    EXPECT_EQ(grant.status(), batt::StatusCode::kGrantUnavailable);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
// Define a test fixture for all the blocking test cases, to make wrangling Tasks and executors easier.
//
class AsyncScalableGrantWaitTest : public ::testing::Test
{
   public:
    void start_blocking_operation(const std::function<void()>& fn)
    {
        BATT_CHECK(!this->blocking_operation_);

        this->blocking_operation_.emplace(this->execution_context_.get_executor(), fn,
                                          /*name=*/"AsyncScalableGrantWaitTest::blocking_operation");

        // Allow the task to run up to the point where it blocks.
        //
        EXPECT_GT(this->execution_context_.poll(), 0u)
            << "There should have been some activity on the fake executor!";

        EXPECT_FALSE(this->blocking_operation_->try_join())
            << "The operation should have blocked, preventing the task from finishing!";
    }

    bool resume_blocking_operation()
    {
        BATT_CHECK(this->blocking_operation_);
        return this->execution_context_.poll() != 0u;
    }

    void SetUp() override
    {
        this->first_.emplace(this->issuer_.issue_grant(3, batt::WaitForResource::kTrue));
        ASSERT_TRUE(this->first_.ok());

        this->start_blocking_operation([this] {
            this->second_.emplace(this->issuer_.issue_grant(2, batt::WaitForResource::kTrue));
        });

        EXPECT_FALSE(this->resume_blocking_operation());
    }

    void TearDown() override
    {
        if (this->blocking_operation_) {
            ASSERT_TRUE(this->blocking_operation_->try_join());
            this->blocking_operation_ = batt::None;
        }
    }

    batt::FakeExecutionContext execution_context_;
    batt::Optional<batt::Task> blocking_operation_;
    batt::ScalableGrant::Issuer issuer_{4};
    batt::StatusOr<batt::ScalableGrant> first_;
    batt::StatusOr<batt::ScalableGrant> second_;
};

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
//  2. Issue a grant
//     d. wait = true, pool is not large enough, resolve by destructing a previous Grant
//
TEST_F(AsyncScalableGrantWaitTest, IssueGrantWaitForOldGrantDestruct)
{
    // This will cause the pending `issue_grant` to resolve.
    //
    this->first_ = batt::Status{batt::StatusCode::kUnknown};

    // Unblock the operation.
    //
    ASSERT_TRUE(this->resume_blocking_operation());

    // Verify success.
    //
    ASSERT_TRUE(this->second_.ok());
    EXPECT_EQ(this->second_->size(), 2u);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
//  2. Issue a grant
//     e. wait = true, pool is not large enough, resolve by increasing the pool via recycle
//
TEST_F(AsyncScalableGrantWaitTest, IssueGrantWaitForPoolIncrease)
{
    // This will cause the pending `issue_grant` to resolve.
    //
    this->issuer_.grow(1);

    // Unblock the operation.
    //
    ASSERT_TRUE(this->resume_blocking_operation());

    // Verify success.
    //
    ASSERT_TRUE(this->first_.ok());
    EXPECT_EQ(this->first_->size(), 3u);

    ASSERT_TRUE(this->second_.ok());
    EXPECT_EQ(this->second_->size(), 2u);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
//  2. Issue a grant
//     f. wait = true, pool is not large enough, resolve by closing the Issuer
//
TEST_F(AsyncScalableGrantWaitTest, IssueGrantWaitForClose)
{
    // This will cause the pending `issue_grant` to resolve.
    //
    this->issuer_.close();

    // Unblock the operation.
    //
    ASSERT_TRUE(this->resume_blocking_operation());

    // Verify result.
    //
    ASSERT_TRUE(this->first_.ok());
    EXPECT_EQ(this->first_->size(), 3u);

    ASSERT_FALSE(this->second_.ok());
    EXPECT_EQ(this->second_.status(), batt::StatusCode::kClosed);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
//  3. partially spend a grant, with subcases from (2), with d,e combined as described:
//     a. wait = false, pool is large enough
//     b. wait = false, pool not large enough
//     c. wait = true, pool is large enough
//
TEST(AsyncScalableGrantTest, PartialSpendSuccess)
{
    batt::ScalableGrant::Issuer issuer{10};

    for (batt::WaitForResource wait_for_resource :
         {batt::WaitForResource::kFalse, batt::WaitForResource::kTrue}) {
        batt::StatusOr<batt::ScalableGrant> grant = issuer.issue_grant(10, batt::WaitForResource::kFalse);
        ASSERT_TRUE(grant.ok());
        EXPECT_EQ(grant->size(), 10u);

        // 3b. expect failure because count too big
        {
            batt::StatusOr<batt::ScalableGrant> subgrant = grant->spend(11, batt::WaitForResource::kFalse);
            EXPECT_FALSE(subgrant.ok());
            EXPECT_EQ(subgrant.status(), batt::StatusCode::kGrantUnavailable);
        }

        for (usize count = 0; count < 10; ++count) {
            batt::StatusOr<batt::ScalableGrant> subgrant = grant->spend(1, wait_for_resource);

            ASSERT_TRUE(subgrant.ok());
            EXPECT_EQ(subgrant->size(), 1u);
            EXPECT_EQ(grant->size(), 9u - count);
        }

        // 3b. expect failure because grant too small
        {
            batt::StatusOr<batt::ScalableGrant> subgrant = grant->spend(1, batt::WaitForResource::kFalse);
            EXPECT_FALSE(subgrant.ok());
            EXPECT_EQ(subgrant.status(), batt::StatusCode::kGrantUnavailable);
        }
    }
}

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------

class AsyncScalableGrantWaitSpendTest : public AsyncScalableGrantWaitTest
{
   public:
    void SetUp() override
    {
        this->first_.emplace(this->issuer_.issue_grant(3, batt::WaitForResource::kTrue));
        ASSERT_TRUE(this->first_.ok());

        this->first_subgrant_.emplace(this->first_->spend(2, batt::WaitForResource::kTrue));
        ASSERT_TRUE(this->first_subgrant_.ok());

        this->start_blocking_operation([this] {
            this->second_subgrant_.emplace(this->first_->spend(2, batt::WaitForResource::kTrue));
        });

        EXPECT_FALSE(this->resume_blocking_operation());
    }

    batt::StatusOr<batt::ScalableGrant> first_subgrant_;
    batt::StatusOr<batt::ScalableGrant> second_subgrant_;
};

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
//  3. partially spend a grant, with subcases from (2), with d,e combined as described:
//       d. subsume a new grant from the issuer
//
TEST_F(AsyncScalableGrantWaitSpendTest, ResolveBySubsumeNewGrant)
{
    batt::StatusOr<batt::ScalableGrant> extra = this->issuer_.issue_grant(1, batt::WaitForResource::kFalse);
    ASSERT_TRUE(extra.ok());

    batt::ScalableGrant& retval = this->first_->subsume(std::move(*extra));
    EXPECT_EQ(&retval, &*this->first_);

    EXPECT_TRUE(this->resume_blocking_operation());
    ASSERT_TRUE(this->second_subgrant_.ok());
    EXPECT_EQ(this->second_subgrant_->size(), 2u);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
//  3. partially spend a grant, with subcases from (2), with d,e combined as described:
//       e. subsume a new grant from the issuer
//
TEST_F(AsyncScalableGrantWaitSpendTest, ResolveBySubsumeFirstSubgrant)
{
    batt::ScalableGrant& retval = this->first_->subsume(std::move(*this->first_subgrant_));
    EXPECT_EQ(&retval, &*this->first_);

    EXPECT_TRUE(this->resume_blocking_operation());
    ASSERT_TRUE(this->second_subgrant_.ok());
    EXPECT_EQ(this->second_subgrant_->size(), 2u);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
//  3. partially spend a grant, with subcases from (2), with d,e combined as described:
//     f. resolve by revoking the Grant
//
TEST_F(AsyncScalableGrantWaitSpendTest, ResolveByRevoke)
{
    EXPECT_FALSE(this->first_->is_revoked());

    this->first_->revoke();
    EXPECT_TRUE(this->first_->is_revoked());

    EXPECT_TRUE(this->resume_blocking_operation());
    EXPECT_FALSE(this->second_subgrant_.ok());
    EXPECT_EQ(this->second_subgrant_.status(), batt::StatusCode::kGrantRevoked);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
//  4. Grant move test
//
TEST(AsyncScalableGrantTest, Move)
{
    batt::ScalableGrant::Issuer issuer{3};
    EXPECT_EQ(issuer.available(), 3u);
    {
        batt::StatusOr<batt::ScalableGrant> first = issuer.issue_grant(2, batt::WaitForResource::kFalse);
        ASSERT_TRUE(first.ok());
        EXPECT_EQ(issuer.available(), 1u);
        EXPECT_EQ(first->size(), 2u);
        EXPECT_TRUE(first->is_valid());
        EXPECT_EQ(first->get_issuer(), &issuer);
        EXPECT_FALSE(first->is_revoked());

        batt::ScalableGrant second = std::move(*first);
        EXPECT_EQ(issuer.available(), 1u);
        EXPECT_EQ(first->size(), 0u);
        EXPECT_EQ(second.size(), 2u);
        EXPECT_FALSE(first->is_valid());
        EXPECT_TRUE(second.is_valid());
        EXPECT_EQ(first->get_issuer(), nullptr);
        EXPECT_EQ(second.get_issuer(), &issuer);
        EXPECT_FALSE(first->is_revoked());
        EXPECT_FALSE(second.is_revoked());

        batt::StatusOr<batt::ScalableGrant> subgrant = first->spend(1, batt::WaitForResource::kFalse);
        EXPECT_FALSE(subgrant.ok());
        EXPECT_EQ(subgrant.status(), batt::StatusCode::kFailedPrecondition);
    }
    EXPECT_EQ(issuer.available(), 3u);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
//  5. Grant swap test
//     a. same issuer
//
TEST(AsyncScalableGrantTest, SwapSameIssuer)
{
    for (bool swap_order : {false, true}) {
        batt::ScalableGrant::Issuer issuer{3};
        EXPECT_EQ(issuer.available(), 3u);
        {
            batt::StatusOr<batt::ScalableGrant> first = issuer.issue_grant(2, batt::WaitForResource::kFalse);
            EXPECT_EQ(issuer.available(), 1u);
            {
                batt::StatusOr<batt::ScalableGrant> second =
                    issuer.issue_grant(1, batt::WaitForResource::kFalse);
                EXPECT_EQ(issuer.available(), 0u);

                ASSERT_TRUE(first.ok());
                ASSERT_TRUE(second.ok());
                EXPECT_EQ(first->size(), 2u);
                EXPECT_EQ(second->size(), 1u);
                EXPECT_EQ(first->get_issuer(), &issuer);
                EXPECT_EQ(second->get_issuer(), &issuer);

                if (swap_order) {
                    first->swap(*second);
                } else {
                    second->swap(*first);
                }

                EXPECT_EQ(first->size(), 1u);
                EXPECT_EQ(second->size(), 2u);
                EXPECT_EQ(first->get_issuer(), &issuer);
                EXPECT_EQ(second->get_issuer(), &issuer);
                EXPECT_EQ(issuer.available(), 0u);
            }
            EXPECT_EQ(issuer.available(), 2u);
        }
        EXPECT_EQ(issuer.available(), 3u);
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
//  5. Grant swap test
//     b. different issuer
//
TEST(AsyncScalableGrantTest, DifferentIssuer)
{
    for (bool swap_order : {false, true}) {
        batt::ScalableGrant::Issuer issuer1{3};
        EXPECT_EQ(issuer1.available(), 3u);

        batt::ScalableGrant::Issuer issuer2{3};
        EXPECT_EQ(issuer2.available(), 3u);
        {
            batt::StatusOr<batt::ScalableGrant> first = issuer1.issue_grant(2, batt::WaitForResource::kFalse);
            EXPECT_EQ(issuer1.available(), 1u);
            EXPECT_EQ(issuer2.available(), 3u);
            {
                batt::StatusOr<batt::ScalableGrant> second =
                    issuer2.issue_grant(1, batt::WaitForResource::kFalse);
                EXPECT_EQ(issuer1.available(), 1u);
                EXPECT_EQ(issuer2.available(), 2u);

                ASSERT_TRUE(first.ok());
                ASSERT_TRUE(second.ok());
                EXPECT_EQ(first->size(), 2u);
                EXPECT_EQ(second->size(), 1u);
                EXPECT_EQ(first->get_issuer(), &issuer1);
                EXPECT_EQ(second->get_issuer(), &issuer2);

                if (swap_order) {
                    first->swap(*second);
                } else {
                    second->swap(*first);
                }

                EXPECT_EQ(first->size(), 1u);
                EXPECT_EQ(second->size(), 2u);
                EXPECT_EQ(first->get_issuer(), &issuer2);
                EXPECT_EQ(second->get_issuer(), &issuer1);
                EXPECT_EQ(issuer1.available(), 1u);
                EXPECT_EQ(issuer2.available(), 2u);
            }
            EXPECT_EQ(issuer1.available(), 3u);
            EXPECT_EQ(issuer2.available(), 2u);
        }
        EXPECT_EQ(issuer1.available(), 3u);
        EXPECT_EQ(issuer2.available(), 3u);
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
//  5. Grant swap test
//     c. one invalid
//
TEST(AsyncScalableGrantTest, OneInvalid)
{
    for (bool swap_order : {false, true}) {
        batt::ScalableGrant::Issuer issuer{3};
        EXPECT_EQ(issuer.available(), 3u);
        {
            batt::StatusOr<batt::ScalableGrant> first = issuer.issue_grant(2, batt::WaitForResource::kFalse);
            EXPECT_EQ(issuer.available(), 1u);
            ASSERT_TRUE(first.ok());
            EXPECT_TRUE(first->is_valid());
            {
                batt::ScalableGrant second = std::move(*first);
                EXPECT_EQ(second.size(), 2u);
                EXPECT_FALSE(first->is_valid());
                EXPECT_TRUE(second.is_valid());
                {
                    batt::StatusOr<batt::ScalableGrant> third =
                        issuer.issue_grant(1, batt::WaitForResource::kFalse);
                    ASSERT_TRUE(third.ok());
                    EXPECT_EQ(issuer.available(), 0u);

                    if (swap_order) {
                        first->swap(*third);
                    } else {
                        third->swap(*first);
                    }

                    EXPECT_FALSE(third->is_valid());
                    EXPECT_TRUE(first->is_valid());
                    EXPECT_EQ(first->size(), 1u);
                    EXPECT_EQ(issuer.available(), 0u);
                }
                EXPECT_EQ(issuer.available(), 0u);
            }
            EXPECT_EQ(issuer.available(), 2u);
        }
        EXPECT_EQ(issuer.available(), 3u);
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
//  6. Grant spend_all
//
TEST(AsyncScalableGrantTest, SpendAll)
{
    batt::ScalableGrant::Issuer issuer{10};
    EXPECT_EQ(issuer.available(), 10u);

    for (batt::WaitForResource wait_for_resource :
         {batt::WaitForResource::kFalse, batt::WaitForResource::kTrue}) {
        for (usize count = 0; count <= 10; ++count) {
            batt::StatusOr<batt::ScalableGrant> grant = issuer.issue_grant(count, wait_for_resource);
            EXPECT_EQ(issuer.available(), 10u - count);

            ASSERT_TRUE(grant.ok());
            EXPECT_EQ(grant->size(), count);

            const u64 spent_count = grant->spend_all();
            EXPECT_EQ(count, spent_count);
            EXPECT_EQ(issuer.available(), 10u);
        }
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
//  7. subsume invalid grant
//
TEST(AsyncScalableGrantTest, SubsumeInvalid)
{
    batt::ScalableGrant::Issuer issuer{10};
    batt::StatusOr<batt::ScalableGrant> first = issuer.issue_grant(3, batt::WaitForResource::kFalse);
    batt::StatusOr<batt::ScalableGrant> second = issuer.issue_grant(2, batt::WaitForResource::kFalse);

    ASSERT_TRUE(first.ok());
    ASSERT_TRUE(second.ok());

    batt::ScalableGrant first_copy = std::move(*first);

    EXPECT_EQ(second->size(), 2u);

    second->subsume(std::move(*first));
    EXPECT_EQ(second->size(), 2u);

    second->subsume(std::move(first_copy));
    EXPECT_EQ(second->size(), 5u);
}

#if BATT_PLATFORM_SUPPORTS_DEATH_TESTS

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
// Death Tests:
//  10. Issuer destroyed with active Grant still alive
//
TEST(AsyncScalableGrantTest, DeathIssuerDestroyedTooSoon)
{
    batt::Optional<batt::ScalableGrant::Issuer> issuer{10};
    batt::StatusOr<batt::ScalableGrant> grant = issuer->issue_grant(3, batt::WaitForResource::kFalse);
    ASSERT_TRUE(grant.ok());

    EXPECT_DEATH(issuer = batt::None, "[As]sert.*[Ff]ail.*This may indicate a Grant is still active");
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
// Death Tests:
//  11. subsume a grant from a different issuer
//      a. size={0,0}
//      b. size={0,1}
//      c. size={1,0}
//      d. size={1,1}
//
TEST(AsyncScalableGrantTest, DeathSubsumeDifferentIssuer)
{
    batt::ScalableGrant::Issuer issuer1{3};
    batt::ScalableGrant::Issuer issuer2{3};

    for (usize grant1_size : {0, 1}) {
        for (usize grant2_size : {0, 1}) {
            batt::StatusOr<batt::ScalableGrant> grant1 =
                issuer1.issue_grant(grant1_size, batt::WaitForResource::kFalse);
            ASSERT_TRUE(grant1.ok());

            batt::StatusOr<batt::ScalableGrant> grant2 =
                issuer2.issue_grant(grant2_size, batt::WaitForResource::kFalse);
            ASSERT_TRUE(grant2.ok());

            EXPECT_DEATH(grant1->subsume(std::move(*grant2)), "[As]sert.*[Ff]ail.*issuer");
        }
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
// Death Tests:
//  12. subsume into an invalid grant
//
TEST(AsyncScalableGrantTest, DeathInvalidSubsumesValid)
{
    batt::ScalableGrant::Issuer issuer{10};
    batt::StatusOr<batt::ScalableGrant> first = issuer.issue_grant(3, batt::WaitForResource::kFalse);
    batt::StatusOr<batt::ScalableGrant> second = issuer.issue_grant(2, batt::WaitForResource::kFalse);

    ASSERT_TRUE(first.ok());
    ASSERT_TRUE(second.ok());

    batt::ScalableGrant first_copy = std::move(*first);

    EXPECT_EQ(second->size(), 2u);
    EXPECT_DEATH(first->subsume(std::move(*second)),
                 "[As]sert.*[Ff]ail.*It is NOT legal to subsume a Grant into an invalidated Grant.");
}

#endif  // BATT_PLATFORM_SUPPORTS_DEATH_TESTS

}  // namespace
