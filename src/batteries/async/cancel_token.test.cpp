//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#include <batteries/async/cancel_token.hpp>
//
#include <batteries/async/cancel_token.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <batteries/async/fake_execution_context.hpp>
#include <batteries/async/fake_executor.hpp>
#include <batteries/async/simple_executor.hpp>
#include <batteries/async/task.hpp>

#include <boost/functional/hash.hpp>

#include <atomic>
#include <random>
#include <unordered_set>

namespace {

using namespace batt::int_types;

// Test Plan:
//
//  1. Create CancelToken with None
//  2. Test result of CancelToken::none()
//  3. Create/Destroy a valid CancelToken without using it
//  4. Attempt to await on the same CancelToken in two concurrent tasks - DEATH
//  5. Use FakeExecutionContext with random event orderings to run the following scenario:
//     - Start an async op in one Task via CancelToken::await
//     - Start a second Task that calls CancelToken::cancel
//     Use an atomic variable with CAS to store whether we expect cancellation or not.
//     Run for some sufficiently high number of RNG seeds
//  6. Test operator==, !=
//  7. Test operator<, >, ...
//  8. Test hash_value
//  9. Test debug_info()
//

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//  1. Create CancelToken with None
//
TEST(CancelTokenTest, InvalidCancelToken)
{
    batt::CancelToken t{batt::None};

    EXPECT_FALSE(t);
    EXPECT_FALSE(t.is_valid());

    {
        batt::StatusOr<i32> result = t.await<i32>([](auto&&...) {
            // nothing to do!
        });

        EXPECT_FALSE(result.ok());
        EXPECT_EQ(result.status(), batt::StatusCode::kFailedPrecondition);
    }
}

//----- --- -- -  -  -   -

TEST(CancelTokenTest, InvalidCancelTokenDeath)
{
    batt::CancelToken t{batt::None};

    const char* expected_pattern = "Assertion failed: .*impl.*nullptr.*";

    EXPECT_DEATH(t.is_cancelled(), expected_pattern);
    EXPECT_DEATH(t.cancel(), expected_pattern);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//  2. Test result of CancelToken::none()
//
TEST(CancelTokenTest, CancelTokenNone)
{
    const batt::CancelToken& t = batt::CancelToken::none();

    EXPECT_FALSE(t);
    EXPECT_FALSE(t.is_valid());
    EXPECT_EQ(t, batt::CancelToken{batt::None});
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//  3. Create/Destroy a valid CancelToken without using it
//
TEST(CancelTokenTest, CreateDestroy)
{
    batt::CancelToken t;

    EXPECT_TRUE(t);
    EXPECT_TRUE(t.is_valid());
    EXPECT_FALSE(t.is_cancelled());
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//  4. Attempt to await on the same CancelToken in two concurrent tasks - DEATH
//
TEST(CancelTokenTest, ConcurrentAwaitDeath)
{
    batt::CancelToken t;

    batt::StatusOr<bool> task1_result, task2_result;

    batt::SimpleExecutionContext ex;

    batt::Mutex<std::function<void(batt::StatusOr<bool>)>> saved_handler;

    batt::Task task1{ex.get_executor(),
                     [&] {
                         task1_result = t.await<bool>([&](auto&& handler) {
                             BATT_SCOPED_LOCK(lock, saved_handler);
                             *lock = BATT_FORWARD(handler);
                         });
                     },
                     "CancelTokenTest.ConcurrentAwaitDeath.task1"};

    const auto start_task2 = [&] {
        batt::Task task2{ex.get_executor(),
                         [&] {
                             task2_result = t.await<bool>([&](auto&& handler) {
                                 BATT_SCOPED_LOCK(lock, saved_handler);
                                 *lock = BATT_FORWARD(handler);
                             });
                         },
                         "CancelTokenTest.ConcurrentAwaitDeath.task2"};

        ex.run();
    };

    EXPECT_DEATH(start_task2(), "Multiple concurrent handlers created for the same CancelToken!");

    // Run the executor on a background helper thread so we can wake up task1 from the test fn.
    //
    std::thread helper_thread{[&ex] {
        ex.run();
    }};

    // Busy-wait on the handler becoming available.
    //
    for (;;) {
        {
            BATT_SCOPED_LOCK(lock, saved_handler);
            if (*lock) {
                break;
            }
        }
        batt::Task::yield();
    }

    // This will unblock task1 and cause it to exit.
    //
    {
        BATT_SCOPED_LOCK(lock, saved_handler);
        (*lock)(true);
    }
    task1.join();

    ASSERT_TRUE(task1_result.ok()) << BATT_INSPECT(task1_result.status());
    EXPECT_EQ(task2_result.status(), batt::StatusCode::kUnknown);

    // Tear down the executor and helper thread.
    //
    ex.stop();
    helper_thread.join();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//  5. Use FakeExecutionContext with random event orderings to run the following scenario:
//     - Start an async op in one Task via CancelToken::await
//     - Start a second Task that calls CancelToken::cancel
//     Use an atomic variable with CAS to store whether we expect cancellation or not.
//     Run for some sufficiently high number of RNG seeds
//
TEST(CancelTokenTest, Simulation)
{
    constexpr usize kNumRandomSeeds = 10 * 1000;

    usize cancel_count = 0;
    usize completed_count = 0;

    for (usize seed = 0; seed < kNumRandomSeeds; ++seed) {
        batt::CancelToken token;

        batt::FakeExecutionContext ex;

        batt::Mutex<std::function<void(batt::StatusOr<bool>)>> saved_handler;

        batt::StatusOr<bool> op_result;

        std::atomic<int> expected_outcome{0};
        std::atomic<bool> waiter_entered{false};
        std::atomic<int> was_cancelled_at_handler_create{-1};
        std::atomic<int> was_cancelled_at_handler_invoke{-1};

        batt::Task waiter_task{ex.get_executor(), [&] {
                                   waiter_entered.store(true);
                                   op_result = token.await<bool>([&](auto&& handler) {
                                       was_cancelled_at_handler_create = batt::is_cancelled(handler) ? 1 : 0;
                                       {
                                           BATT_SCOPED_LOCK(lock, saved_handler);

                                           *lock = batt::bind_handler(
                                               BATT_FORWARD(handler), [&](auto&& handler, auto&&... args) {
                                                   was_cancelled_at_handler_invoke =
                                                       batt::is_cancelled(handler) ? 1 : 0;
                                                   BATT_FORWARD(handler)(BATT_FORWARD(args)...);
                                               });
                                       }
                                   });
                               }};

        batt::Task completer_task{ex.get_executor(), [&] {
                                      for (;;) {
                                          {
                                              BATT_SCOPED_LOCK(lock, saved_handler);
                                              if (*lock) {
                                                  break;
                                              }
                                          }
                                          batt::Task::yield();
                                      }
                                      {
                                          int v = 0;
                                          while (v == 0 && !expected_outcome.compare_exchange_weak(v, 1)) {
                                              continue;
                                          }
                                      }
                                      {
                                          BATT_SCOPED_LOCK(lock, saved_handler);
                                          (*lock)(true);
                                      }
                                  }};

        batt::Task canceller_task{ex.get_executor(), [&] {
                                      {
                                          int v = 0;
                                          while (v == 0 && !expected_outcome.compare_exchange_weak(v, -1)) {
                                              continue;
                                          }
                                      }

                                      // If we never even start the operation, saved_handler won't be set by
                                      // the waiter task, so set it here to unblock the completer task.
                                      //
                                      if (!waiter_entered) {
                                          BATT_SCOPED_LOCK(lock, saved_handler);
                                          *lock = [](auto&&...) {
                                          };
                                      }

                                      token.cancel();
                                  }};

        ex.run_with_random_seed(seed);

        waiter_task.join();
        completer_task.join();
        canceller_task.join();

        if (was_cancelled_at_handler_create == 1) {
            EXPECT_EQ(expected_outcome, -1);
        }

        if (expected_outcome == -1) {
            ++cancel_count;

            EXPECT_NE(was_cancelled_at_handler_invoke, 0);
            EXPECT_EQ(op_result.status(), batt::StatusCode::kCancelled);

        } else {
            ++completed_count;

            EXPECT_EQ(was_cancelled_at_handler_invoke, 0);
            EXPECT_EQ(was_cancelled_at_handler_create, 0);
            EXPECT_EQ(expected_outcome, 1);
            ASSERT_TRUE(op_result.ok());
            EXPECT_TRUE(*op_result);
        }
    }

    const usize kMinCompletedCount = kNumRandomSeeds / 5;

    BATT_VLOG(1) << BATT_INSPECT(kMinCompletedCount);
    BATT_VLOG(1) << BATT_INSPECT(completed_count);

    // Verify that we get a sufficient blend of cancels and completions.
    //
    EXPECT_GT(cancel_count, kNumRandomSeeds / 3);
    EXPECT_GT(completed_count, kMinCompletedCount);

    // Because of the busy wait loop, the completer task should win the race less often.
    //
    EXPECT_GT(cancel_count, completed_count);

    // All runs should be a cancel or a completion.
    //
    EXPECT_EQ(cancel_count + completed_count, kNumRandomSeeds);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//  6. Test operator==, !=
//
TEST(CancelTokenTest, Equality)
{
    batt::CancelToken a, b;
    batt::CancelToken c = a;
    batt::CancelToken d;
    d = b;

    EXPECT_EQ(a, c);
    EXPECT_EQ(b, d);
    EXPECT_NE(a, b);
    EXPECT_NE(c, d);

    EXPECT_FALSE(a != c);
    EXPECT_FALSE(b != d);
    EXPECT_FALSE(a == b);
    EXPECT_FALSE(c == d);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//  7. Test operator<, >, ...
//
TEST(CancelTokenTest, Ordering)
{
    batt::CancelToken a, b;

    if (b < a) {
        std::swap(a, b);
    }

    batt::CancelToken c = a;
    batt::CancelToken d;
    d = b;

    EXPECT_LT(a, b);
    EXPECT_GT(b, a);
    EXPECT_LE(a, b);
    EXPECT_LE(a, c);
    EXPECT_LE(c, a);
    EXPECT_GE(b, d);
    EXPECT_GE(d, b);
    EXPECT_GE(b, a);

    EXPECT_FALSE(b < a);
    EXPECT_FALSE(a < a);
    EXPECT_FALSE(a < c);
    EXPECT_FALSE(a > a);
    EXPECT_FALSE(a > c);
    EXPECT_FALSE(a > b);
    EXPECT_FALSE(a >= b);
    EXPECT_FALSE(b <= a);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//  8. Test hash_value
//
TEST(CancelTokenTest, HashValue)
{
    const auto run_tests = [](batt::CancelToken a, batt::CancelToken b) {
        std::unordered_set<batt::CancelToken, boost::hash<batt::CancelToken>> s;

        EXPECT_EQ(s.count(a), 0u);

        s.emplace(a);

        EXPECT_EQ(s.count(a), 1u);
        EXPECT_EQ(s.count(b), 0u);

        b = a;

        EXPECT_EQ(s.count(b), 1u);
    };

    run_tests(batt::CancelToken{}, batt::CancelToken{});
    run_tests(batt::CancelToken::none(), batt::CancelToken{});
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//  9. Test debug_info()
//
TEST(CancelTokenTest, DebugInfo)
{
    batt::CancelToken token;

    EXPECT_THAT(batt::to_string(token.debug_info()), ::testing::StrEq("CancelToken{state=00000000,}"));

    token = batt::None;

    EXPECT_THAT(batt::to_string(token.debug_info()), ::testing::StrEq("CancelToken{None}"));
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//  10. Await without template arg.
//
TEST(CancelTokenTest, AwaitVoidResult)
{
    // single thread - cancel outcome
    {
        batt::CancelToken token;

        token.cancel();

        batt::Status result = token.await([](auto&& /*handler*/) {
            BATT_PANIC();
        });

        EXPECT_EQ(result, batt::StatusCode::kCancelled);
    }

    // multi thread - completed outcome
    {
        std::promise<std::function<void(batt::Status)>> handler_promise;

        std::thread helper_thread{[&handler_promise] {
            auto handler = handler_promise.get_future().get();

            handler(batt::OkStatus());
        }};
        auto on_scope_exit = batt::finally([&helper_thread] {
            helper_thread.join();
        });

        batt::CancelToken token;

        batt::Status result = token.await([&handler_promise](auto&& handler) {
            handler_promise.set_value(handler);
        });

        EXPECT_TRUE(result.ok()) << BATT_INSPECT(result);
    }
}

}  // namespace
