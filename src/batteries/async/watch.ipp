//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_WATCH_IPP
#define BATTERIES_ASYNC_WATCH_IPP

#include <batteries/async/debug_info.hpp>
#include <batteries/async/task_decl.hpp>

namespace batt {

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------
// class Watch

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
inline StatusOr<T> Watch<T>::await_not_equal(const T& last_seen)
{
    return Task::await<StatusOr<T>>([&](auto&& fn) {
        this->async_wait(last_seen, BATT_FORWARD(fn));
    });
}

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------
// class WatchAtomic

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
inline StatusOr<T> WatchAtomic<T>::await_not_equal(const T& last_seen) const
{
    BATT_DEBUG_INFO("WatchAtomic{value="
                    << this->value() << " spin_state=" << std::bitset<8>{this->spin_state_}
                    << "(locked=" << kLocked << ",open=" << kOpen << ",wait=" << kWaiting
                    << ") observers.size()=" << this->observers_.size() << " is_closed=" << this->is_closed()
                    << " last_seen=" << last_seen << "}");

    return Task::await<StatusOr<T>>([&](auto&& fn) {
        this->async_wait(last_seen, BATT_FORWARD(fn));
    });
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
template <typename Fn>
inline StatusOr<T> WatchAtomic<T>::await_modify(Fn&& fn)
{
    T old_value = this->value().load();

    for (;;) {
        const Optional<T> new_value = [&] {
            for (;;) {
                const Optional<T> modified_value = fn(old_value);
                if (!modified_value || this->value().compare_exchange_weak(old_value, *modified_value)) {
                    return modified_value;
                }
            }
        }();

        if (new_value) {
            if (*new_value != old_value) {
                this->notify(*new_value);
            }
            break;
        }

        BATT_DEBUG_INFO("[WatchAtomic::await_modify] waiting for update (old_value=" << old_value << ")");

        StatusOr<T> updated_value = this->await_not_equal(old_value);
        BATT_REQUIRE_OK(updated_value);

        old_value = *updated_value;
    }

    return old_value;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
inline T WatchAtomic<T>::poll() noexcept
{
    batt::Task::yield();
    return this->get_value();
}

}  //namespace batt

#endif  // BATTERIES_ASYNC_WATCH_IPP
