//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_SIMPLE_EXECUTOR_HPP
#define BATTERIES_ASYNC_SIMPLE_EXECUTOR_HPP

#include <batteries/config.hpp>
//

#include <batteries/asio/execution_context.hpp>
#include <batteries/asio/system_executor.hpp>
#include <batteries/async/basic_executor.hpp>
#include <batteries/async/handler.hpp>
#include <batteries/async/mutex.hpp>
#include <batteries/async/watch.hpp>
#include <batteries/utility.hpp>

#include <boost/asio/query.hpp>

#include <condition_variable>
#include <mutex>

namespace batt {

// Forward-declaration.
//
class SimpleExecutionContext;

// The executor type used by SimpleExecutionContext.
//
using SimpleExecutor = BasicExecutor<SimpleExecutionContext,  //
                                     boost::asio::execution::outstanding_work_t::untracked_t,
                                     boost::asio::execution::blocking_t::never_t,
                                     boost::asio::execution::relationship_t::fork_t>;

/** \brief A minimal multi-threaded execution context implementation, suitable for parallel-compute thread
 * pools.
 */
class SimpleExecutionContext : public boost::asio::execution_context
{
   public:
    /** \brief Alias for this type.
     */
    using Self = SimpleExecutionContext;

    /** \brief The executor type for this context; returned by `this->get_executor()`.
     */
    using executor_type = SimpleExecutor;

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    /** \brief Returns a thread-local data member used to track whether the current thread is inside `run()`
     * for this context, so that `dispatch` can decide whether to run the passed function immediately.
     */
    static auto inside_run_on_this_thread() noexcept -> SimpleExecutionContext**;

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    SimpleExecutionContext() = default;
    ~SimpleExecutionContext() = default;

    /** \brief Returns a new executor that schedules work onto this context.
     */
    auto get_executor() noexcept -> executor_type;

    /** \brief Returns the current work count.
     */
    auto work_count() noexcept -> Watch<i64>&;  // TODO [tastolfi 2023-07-31] make const &

    /** \brief Notifies all threads inside `this->run()` by signalling the condition variable.
     */
    auto wake() -> void;

    /** \brief Increments the work count by one.  This is automatically done inside `post()` (or equivalent;
     * i.e., `dispatch()` (within `this->run()`) or `defer`).
     */
    auto on_work_started() noexcept -> void;

    /** \brief Decrements the work count by one.  This is automatically done when posted completion handlers
     * are executed.
     */
    auto on_work_finished() noexcept -> void;

    /** \brief Signals to all threads inside `this->run()` to exit their event processing loop ASAP.
     *
     * Once all threads have exited `run()`, `this->reset()` must be called before `this->run()` to resume
     * normal processing.
     */
    auto stop() noexcept -> void;

    /** \brief Returns true as soon as `this->stop()` is called; this does _not_ indicate that threads have
     * exited `this->run()`, only that `this->stop()` has been called.
     *
     * Initially `false` when a context is constructed.
     */
    auto is_stopped() const noexcept -> bool;

    /** \brief Blocks the current thread processing `post()`-ed completion handlers while all of the following
     * are true:
     *
     * - This context's work count is non-zero
     * - `this->is_stopped()` is false
     *
     * \return The number of handlers processed.
     */
    auto run() noexcept -> usize;

    /** \brief Sets `this->is_stopped()` to false to allow threads to enter `this->run()` to resume work
     * processing.
     *
     * This should only be called once the caller is sure that no thread is inside `this->run()`.
     */
    auto reset() noexcept -> void;

    /** \brief Accesses the default allocator directly.
     */
    auto get_allocator() const -> std::allocator<void>;

    /** \brief Runs `fn` in this context, possibly blocking the caller.
     */
    template <typename Fn>
    auto dispatch(Fn&& fn) noexcept -> void;

    /** \brief Runs `fn` in this context; never blocks the caller.
     */
    template <typename Fn>
    auto post(Fn&& fn) noexcept -> void;

    /** \brief Runs `fn` in this context as a continuation of the currently executing completion handler; this
     * should be used to avoid contention.
     */
    template <typename Fn>
    auto defer(Fn&& fn) noexcept -> void;

    //+++++++++++-+-+--+----- --- -- -  -  -   -
   private:
    /** \brief The current work count.
     */
    Watch<i64> work_count_{0};

    /** \brief The default allocator associated with this context.
     */
    std::allocator<void> allocator_;

    /** \brief Protects `this->queue_`.
     */
    std::mutex mutex_;

    /** \brief Used to signal state changes in the runnable state of this context and the presence of handlers
     * in `this->queue_`.
     */
    std::condition_variable cond_;

    /** \brief Completion handlers ready to run in this context.
     */
    HandlerList<> queue_;

    /** \brief When set, causes threads to exit `this->run()` ASAP.
     */
    std::atomic<bool> stop_{false};
};

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------

}  //namespace batt

#include <batteries/async/simple_executor.ipp>

#endif  // BATTERIES_ASYNC_SIMPLE_EXECUTOR_HPP

#include <batteries/config.hpp>

#if BATT_HEADER_ONLY
#include <batteries/async/simple_executor_impl.hpp>
#endif
