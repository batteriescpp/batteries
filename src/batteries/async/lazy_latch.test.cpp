//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2024 Anthony Paul Astolfi
//
#include <batteries/async/lazy_latch.hpp>
//
#include <batteries/async/lazy_latch.hpp>

#include <batteries/suppress.hpp>

BATT_SUPPRESS_IF_GCC("-Wuninitialized")

#include <gmock/gmock.h>
#include <gtest/gtest.h>

BATT_UNSUPPRESS_IF_GCC()

#include <boost/asio/io_context.hpp>

#include <sstream>

namespace {

template <typename T>
struct MockInitFn {
    MOCK_METHOD(void, invoke, (batt::AbstractHandler<batt::StatusOr<T>&&>*));

    template <typename... Args>
    void operator()(Args&&... args)
    {
        this->invoke(BATT_FORWARD(args)...);
    }
};

template <typename T>
struct MockGetHandler {
    MOCK_METHOD(void, invoke, (batt::StatusOr<T>&&));

    template <typename... Args>
    void operator()(Args&&... args)
    {
        this->invoke(BATT_FORWARD(args)...);
    }
};

class LazyLatchTest : public ::testing::Test
{
   public:
    void trigger_first_time_immediate()
    {
        EXPECT_CALL(this->init_fn_, invoke(::testing::_))
            .WillOnce(::testing::Invoke([](batt::AbstractHandler<batt::StatusOr<int>&&>* handler) {
                handler->notify(5);
            }));

        this->latch_->trigger();

        EXPECT_TRUE(this->latch_->is_ready());
    }

    void save_handler_on_expected_trigger()
    {
        this->saved_handler_arg_ = nullptr;

        EXPECT_CALL(this->init_fn_, invoke(::testing::_))
            .WillOnce(::testing::SaveArg<0>(&this->saved_handler_arg_));
    }

    void trigger_saving_handler()
    {
        this->save_handler_on_expected_trigger();
        this->latch_->trigger();

        EXPECT_NE(this->saved_handler_arg_, nullptr);
        EXPECT_FALSE(this->latch_->is_ready());
    }

    void reset_latch()
    {
        this->latch_.emplace(std::ref(this->init_fn_));
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    ::testing::StrictMock<MockInitFn<int>> init_fn_;
    ::testing::StrictMock<MockGetHandler<int>> get_handler_;
    batt::AbstractHandler<batt::StatusOr<int>&&>* saved_handler_arg_ = nullptr;
    batt::Optional<batt::LazyLatch<int>> latch_{std::ref(this->init_fn_)};
};

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
TEST_F(LazyLatchTest, Construct)
{
    EXPECT_FALSE(this->latch_->is_ready());
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
TEST_F(LazyLatchTest, Trigger)
{
    this->trigger_first_time_immediate();

    // Trigger a second time; expect no-op.
    //
    this->latch_->trigger();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
TEST_F(LazyLatchTest, Await)
{
    for (bool await_move : {false, true}) {
        this->trigger_saving_handler();

        bool entered = false;
        batt::Optional<batt::StatusOr<int>> result;

        boost::asio::io_context io;

        batt::Task waiter{io.get_executor(), [await_move, &entered, &result, this] {
                              entered = true;
                              if (await_move) {
                                  result = std::move(*this->latch_).await();
                              } else {
                                  result = this->latch_->await();
                              }
                          }};

        EXPECT_FALSE(entered);
        EXPECT_FALSE(result);

        io.poll();
        io.reset();

        EXPECT_TRUE(entered);
        EXPECT_FALSE(result);

        this->saved_handler_arg_->notify(12);

        EXPECT_FALSE(result);

        io.poll();
        io.reset();

        EXPECT_TRUE(result);
        EXPECT_EQ(**result, 12);

        waiter.join();

        this->reset_latch();
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
TEST_F(LazyLatchTest, Poll)
{
    EXPECT_EQ(this->latch_->poll().status(), batt::StatusCode::kUnavailable);

    this->trigger_saving_handler();

    EXPECT_EQ(this->latch_->poll().status(), batt::StatusCode::kUnavailable);
    EXPECT_NE(this->saved_handler_arg_, nullptr);

    this->saved_handler_arg_->notify(12);

    ASSERT_TRUE(this->latch_->poll().ok());
    EXPECT_EQ(*this->latch_->poll(), 12);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
TEST_F(LazyLatchTest, AsyncGet)
{
    for (bool get_after_resolve : {false, true}) {
        for (bool resolve_with_error : {false, true}) {
            //----- --- -- -  -  -   -
            //
            const auto expect_call_handler = [&] {
                if (resolve_with_error) {
                    EXPECT_CALL(this->get_handler_, invoke(::testing::Eq(batt::StatusOr<int>{
                                                        batt::Status{batt::StatusCode::kPermissionDenied}})))
                        .WillOnce(::testing::Return());
                } else {
                    EXPECT_CALL(this->get_handler_, invoke(::testing::Eq(batt::StatusOr<int>{-71})))
                        .WillOnce(::testing::Return());
                }
            };

            const auto resolve_latch = [&] {
                EXPECT_NE(this->saved_handler_arg_, nullptr);
                EXPECT_FALSE(this->latch_->is_ready());

                // Resolve the latch value.
                //
                if (resolve_with_error) {
                    this->saved_handler_arg_->notify(batt::Status{batt::StatusCode::kPermissionDenied});
                } else {
                    this->saved_handler_arg_->notify(-71);
                }

                EXPECT_TRUE(this->latch_->is_ready());
            };
            //
            //----- --- -- -  -  -   -

            this->save_handler_on_expected_trigger();

            if (get_after_resolve) {
                this->latch_->trigger();
                resolve_latch();
                expect_call_handler();
            }

            this->latch_->async_get(std::ref(this->get_handler_));

            // Test AbstractHandler::dump(ostream).
            //
            {
                std::ostringstream oss;
                this->saved_handler_arg_->dump(oss);

                EXPECT_THAT(oss.str(), ::testing::StrEq("LazyLatch{}"));
            }

            if (!get_after_resolve) {
                expect_call_handler();
                resolve_latch();
            }

            this->reset_latch();
        }
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
TEST_F(LazyLatchTest, CancelViaDestroy)
{
    this->save_handler_on_expected_trigger();
    this->latch_->async_get(std::ref(this->get_handler_));

    EXPECT_FALSE(this->latch_->is_ready());

    EXPECT_CALL(this->get_handler_,
                invoke(::testing::Eq(batt::StatusOr<int>{batt::Status{batt::StatusCode::kCancelled}})))
        .WillOnce(::testing::Return());

    EXPECT_NE(this->saved_handler_arg_, nullptr);

    this->saved_handler_arg_->destroy();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
TEST_F(LazyLatchTest, AsyncGetPoke)
{
    this->save_handler_on_expected_trigger();
    this->latch_->async_get(std::ref(this->get_handler_));

    EXPECT_FALSE(this->latch_->is_ready());

    this->latch_->poke();

    EXPECT_FALSE(this->latch_->is_ready());

    EXPECT_CALL(this->get_handler_,
                invoke(::testing::Eq(batt::StatusOr<int>{batt::Status{batt::StatusCode::kCancelled}})))
        .WillOnce(::testing::Return());

    EXPECT_NE(this->saved_handler_arg_, nullptr);

    this->saved_handler_arg_->destroy();
}

}  // namespace
