//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2022-2024 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_FAKE_TIME_SERVICE_IMPL_HPP
#define BATTERIES_ASYNC_FAKE_TIME_SERVICE_IMPL_HPP

#include <batteries/config.hpp>
//
#include <batteries/asio/post.hpp>
#include <batteries/async/fake_execution_context.hpp>

#include <boost/asio/time_traits.hpp>

namespace batt {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*static*/ boost::asio::execution_context::id FakeTimeService::id;

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*explicit*/ FakeTimeService::FakeTimeService(boost::asio::execution_context& context)
    : boost::asio::execution_context::service{context}
{
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL bool operator<(const FakeTimeService::TimerInstance& l,
                                const FakeTimeService::TimerInstance& r)
{
    return (l.impl == nullptr && r.impl != nullptr) ||
           ((l.impl != nullptr && r.impl != nullptr) && (l.impl->expires_at < r.impl->expires_at));
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL FakeTimeService::State::State() : fake_time_{boost::asio::time_traits<TimePoint>::now()}
{
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL auto FakeTimeService::State::now() -> TimePoint
{
    Lock lock{this->mutex_};
    return this->fake_time_;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void FakeTimeService::State::advance_time(Duration delta)
{
    std::vector<TimerInstance> expired_timers;
    {
        Lock lock{this->mutex_};

        this->fake_time_ += delta;

        while (!this->timer_queue_.empty() && this->timer_queue_.top().impl->expires_at <= this->fake_time_) {
            expired_timers.emplace_back(this->timer_queue_.top());
            timer_queue_.pop();
        }
    }
    for (TimerInstance& timer : expired_timers) {
        boost::asio::post(timer.impl->executor, std::bind(std::move(timer.impl->handler), ErrorCode{}));
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void FakeTimeService::shutdown() /*override*/
{
    // TODO [tastolfi 2022-01-14] how to cancel timers associated with this?
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL bool FakeTimeService::State::has_scheduled_timers() noexcept
{
    Lock lock{this->mutex_};
    return !this->timer_queue_.empty();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL auto FakeTimeService::State::next_expiration_time() noexcept -> Optional<TimePoint>
{
    Lock lock{this->mutex_};

    if (timer_queue_.empty()) {
        return None;
    }
    return this->timer_queue_.top().impl->expires_at;
}

}  // namespace batt

#endif  // BATTERIES_ASYNC_FAKE_TIME_SERVICE_IMPL_HPP
