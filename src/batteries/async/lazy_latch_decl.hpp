//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_LAZY_LATCH_DECL_HPP
#define BATTERIES_ASYNC_LAZY_LATCH_DECL_HPP

#include <batteries/config.hpp>
//
#include <batteries/assert.hpp>
#include <batteries/async/handler.hpp>
#include <batteries/async/watch_decl.hpp>
#include <batteries/cpu_align.hpp>
#include <batteries/optional.hpp>
#include <batteries/shared_ptr.hpp>
#include <batteries/small_fn.hpp>
#include <batteries/status.hpp>
#include <batteries/utility.hpp>

namespace batt {

/** A write-once, single-value synchronized container that is populated lazily by an init_fn when the value is
 * requested.
 *
 * This class implements the `AbstractHandler<StatusOr<T>&&>` interface, which is only exposed when invoking
 * the init function passed in at construction time.  This function is invoked when the LazyLatch is
 * "triggered" (i.e., when its value is asked for) by calling one of the following member functions:
 *
 * - trigger
 * - await
 * - async_get
 *
 * The init function must resolve the latch by calling AbstractHandler::notify with a status or value, or
 * AbstractHandler::destroy (in which case, the LazyLatch is resolved to batt::StatusCode::kCancelled).  It
 * may do so synchronously or asynchronously.  In either case, when the latch is resolved, the init function
 * should assume that invoking notify or destroy will also destroy itself.  LazyLatch guarantees that the init
 * function will remain valid until this happens (or until the LazyLatch is destroyed, whichever comes first).
 * Once triggered, the LazyLatch MUST remain in scope until the latch is resolved by the init function.
 *
 */
template <typename T,
          typename InitFn = SmallFn<void(AbstractHandler<StatusOr<T>&&>*),
                                    /*size=*/kCpuCacheLineSize - (sizeof(void*) + sizeof(Watch<u32>)),
                                    /*move-only=*/true,
                                    /*allow-alloc=*/true>>
class LazyLatch
    : public RefCounted<LazyLatch<T>>
    , private AbstractHandler<StatusOr<T>&&>
{
   public:
    /** \brief The possible states for the LazyLatch.
     */
    enum State : u32 {
        kInitial = 0,
        kTriggered = 1,
        // 2 intentionally skipped; these states are treated as a bitmap of orthogonal conditions.
        kReady = 3,
    };

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    /** \brief Stores the init function until the latch is triggered.
     */
    explicit LazyLatch(InitFn&& init_fn) noexcept;

    /** \brief LazyLatch is not copy/move-constructible.
     */
    LazyLatch(const LazyLatch&) = delete;

    /** \brief Destructs the LazyLatch.
     */
    ~LazyLatch() noexcept;

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    /** \brief LazyLatch is not copy/move-assignable.
     */
    LazyLatch& operator=(const LazyLatch&) = delete;

    /** \brief Invokes the init function that was passed in at construction time.
     *
     * \details The init function is only invoked once, the first time any of the following member functions
     are called:
     *
     * - trigger
     * - await
     * - async_get
     */
    void trigger();

    /** Returns true iff the lazy_latch is in the ready state.
     */
    bool is_ready() const;

    /** Block the current task until the LazyLatch is ready, then return the set value (or Status).
     */
    StatusOr<T> await() & noexcept;

    /** Block the current task until the LazyLatch is ready, then return the set value (or Status) via move.
     */
    StatusOr<T> await() && noexcept;

    /** Same as await(), except this method never blocks; if the LazyLatch isn't ready yet, it immediately
     * returns `StatusCode::kUnavailable`.
     */
    StatusOr<T> poll();

    /** Returns the value of the LazyLatch (non-blocking), panicking if it is not in the ready state.
     */
    StatusOr<T> get_ready_value_or_panic() & noexcept;

    /** Returns the value of the LazyLatch (non-blocking) via move, panicking if it is not in the ready state.
     */
    StatusOr<T> get_ready_value_or_panic() && noexcept;

    /** Invokes `handler` when the LazyLatch value is set (i.e., when it enters the ready state); invokes
     * handler immediately if the LazyLatch is ready when this method is called.
     *
     * \param handler Should have signature `#!cpp void(`\ref StatusOr `<T>)`
     */
    template <typename Handler>
    void async_get(Handler&& handler);

    /** \brief For testing only.
     */
    void poke()
    {
        this->state_.poke();
    }

   private:
    class AsyncGetHandler;

    using Storage = std::aligned_storage_t<std::max(sizeof(InitFn), sizeof(StatusOr<T>)),
                                           std::max(alignof(InitFn), alignof(StatusOr<T>))>;

    //+++++++++++-+-+--+----- --- -- -  -  -   -
    // AbstractHandler impl
    //
    void notify(StatusOr<T>&&) override;

    void destroy() override;

    void dump(std::ostream& out) override;

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    InitFn& stored_init_fn() noexcept;

    StatusOr<T>& stored_result() noexcept;

    //==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -

    Watch<u32> state_{kInitial};
    Storage storage_;
};

//#=##=##=#==#=#==#===#+==#+==========+==+=+=+=+=+=++=+++=+++++=-++++=-+++++++++++

template <typename T, typename InitFn>
class LazyLatch<T, InitFn>::AsyncGetHandler
{
   public:
    explicit AsyncGetHandler(LazyLatch* latch) noexcept : latch_{latch}
    {
    }

    template <typename Handler>
    void operator()(Handler&& handler, const StatusOr<u32>& result) const
    {
        if (result.status() != StatusCode::kPoke) {
            if (!result.ok()) {
                BATT_FORWARD(handler)(result.status());
                return;
            }

            if (*result == kReady) {
                BATT_FORWARD(handler)(this->latch_->get_ready_value_or_panic());
                return;
            }
        }
        this->latch_->state_.async_wait(/*last_seen=*/*result, bind_handler(BATT_FORWARD(handler), *this));
    }

   private:
    LazyLatch* latch_;
};

}  // namespace batt

#endif  // BATTERIES_ASYNC_LAZY_LATCH_DECL_HPP
