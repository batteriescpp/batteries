//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2022-2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_BACKOFF_IPP
#define BATTERIES_ASYNC_BACKOFF_IPP

namespace batt {

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------
//
template <typename RetryPolicy, typename ActionFn, typename Result, typename SleepImpl,
          typename StatusIsRetryableImpl>
inline Result with_retry_policy(RetryPolicy&& policy, std::string_view action_name, ActionFn&& action_fn,
                                SleepImpl&& sleep_impl, StatusIsRetryableImpl&& status_is_retryable_impl)
{
    RetryState state;
    for (;;) {
        Result result = action_fn();
        if (!is_ok_status(result)) {
            auto status = to_status(result);
            if (status_is_retryable_impl(status)) {
                update_retry_state(state, policy);
                if (state.should_retry) {
                    BATT_VLOG(1) << "operation '" << action_name << "' failed with status=" << status
                                 << "; retrying after " << state.next_delay_usec << "us (" << state.n_attempts
                                 << " of " << policy.max_attempts << ")";

                    sleep_impl(boost::posix_time::microseconds(state.next_delay_usec));
                    continue;
                } else {
                    BATT_VLOG(1) << "policy says we should not retry";
                }
            } else {
                BATT_VLOG(1) << "status is not retryable: " << status;
            }
        }
        return result;
    }
}

}  //namespace batt

#endif  // BATTERIES_ASYNC_BACKOFF_IPP
