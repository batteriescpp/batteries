//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023-2024 Anthony Paul Astolfi
//
#include <batteries/async/read_write_lock.hpp>
//
#include <batteries/async/read_write_lock.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <batteries/asio/io_context.hpp>
#include <batteries/async/simple_executor.hpp>
#include <batteries/async/task.hpp>
#include <batteries/async/watch.hpp>
#include <batteries/cpu_align.hpp>
#include <batteries/stream_util.hpp>

#include <algorithm>
#include <array>
#include <memory>
#include <random>
#include <thread>
#include <unordered_map>
#include <vector>

namespace {

using namespace batt::int_types;

constexpr usize kNumReaders = 10;

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
enum struct TestTaskState {
    kInitialized,
    kBeforeLock,
    kHaveLock,
    kReleasedLock,
};

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
std::ostream& operator<<(std::ostream& out, TestTaskState state)
{
    switch (state) {
    case TestTaskState::kInitialized:
        return out << "kInitialized";

    case TestTaskState::kBeforeLock:
        return out << "kBeforeLock";

    case TestTaskState::kHaveLock:
        return out << "kHaveLock";

    case TestTaskState::kReleasedLock:
        return out << "kReleasedLock";

    default:
        break;
    }

    return out << "INVALID:" << (int)state;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
TEST(AsyncReadWriteLockTest, ReadersBeforeWriter)
{
    batt::Watch<i32> test_step{0};
    batt::ReadWriteLock rw_lock;

    boost::asio::io_context context;

    std::vector<std::unique_ptr<batt::Task>> readers;

    std::array<TestTaskState, kNumReaders> reader_state;
    reader_state.fill(TestTaskState::kInitialized);

    std::unique_ptr<batt::Task> writer;
    TestTaskState writer_state = TestTaskState::kInitialized;

    // First create all readers.  They should all immediately be able to acquire the lock for reading.
    //
    for (usize i = 0; i < kNumReaders; ++i) {
        readers.emplace_back(std::make_unique<batt::Task>(
            context.get_executor(),
            [&reader_state, &rw_lock, &test_step, i] {
                BATT_VLOG(1) << "reader_" << i << " entered";
                reader_state[i] = TestTaskState::kBeforeLock;
                {
                    batt::ReadWriteLock::Reader reader{rw_lock};
                    BATT_VLOG(1) << "reader_" << i << " acquired lock";
                    reader_state[i] = TestTaskState::kHaveLock;
                    test_step.await_equal(1).IgnoreError();
                }
                BATT_VLOG(1) << "reader_" << i << " released lock;" << BATT_INSPECT(test_step.get_value());
                reader_state[i] = TestTaskState::kReleasedLock;
            },
            batt::to_string("reader_", i)));
    }

    //----- --- -- -  -  -   -
    context.poll();
    context.reset();
    //----- --- -- -  -  -   -

    // After the readers have had a chance to run, they should all have successfully acquired the lock.
    //
    for (TestTaskState state : reader_state) {
        EXPECT_EQ(state, TestTaskState::kHaveLock);
    }

    // Now create a writer task.  It should be blocked on all the readers releasing their locks.
    //
    writer = std::make_unique<batt::Task>(
        context.get_executor(),
        [&rw_lock, &writer_state, &test_step] {
            BATT_VLOG(1) << "writer entered";
            writer_state = TestTaskState::kBeforeLock;
            {
                batt::ReadWriteLock::Writer writer{rw_lock};
                BATT_VLOG(1) << "writer acquired lock";
                writer_state = TestTaskState::kHaveLock;
                batt::Status status = test_step.await_equal(2);
                BATT_VLOG(1) << BATT_INSPECT(status);
            }
            BATT_VLOG(1) << "writer released lock;" << BATT_INSPECT(test_step.get_value());
            writer_state = TestTaskState::kReleasedLock;
        },
        "writer");

    //----- --- -- -  -  -   -
    context.poll();
    context.reset();
    //----- --- -- -  -  -   -

    // The writer should be blocked at lock acquisition.  It will remain here until we tell the readers to
    // release their locks.
    //
    EXPECT_EQ(writer_state, TestTaskState::kBeforeLock);

    // Advance the test step from 0 to 1; this will tell the readers to release their locks.
    //
    BATT_VLOG(1) << "test_step: 0 -> 1";
    test_step.set_value(1);

    //----- --- -- -  -  -   -
    context.poll();
    context.reset();
    //----- --- -- -  -  -   -

    // Now the readers should all report having released the lock, and the writer should have acquired it.
    //
    for (TestTaskState state : reader_state) {
        EXPECT_EQ(state, TestTaskState::kReleasedLock);
    }
    EXPECT_EQ(writer_state, TestTaskState::kHaveLock);

    // The reader tasks should have finished.
    //
    for (std::unique_ptr<batt::Task>& reader : readers) {
        EXPECT_TRUE(reader->try_join());
    }

    // Now we can tell the writer task to release its lock so we can join all tasks and exit.
    //
    BATT_VLOG(1) << "test_step: 1 -> 2";
    test_step.set_value(2);

    //----- --- -- -  -  -   -
    context.poll();
    context.reset();
    //----- --- -- -  -  -   -

    EXPECT_EQ(writer_state, TestTaskState::kReleasedLock);
    EXPECT_TRUE(writer->try_join());
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
TEST(AsyncReadWriteLockTest, StressTest)
{
    static const usize kNumThreads = std::thread::hardware_concurrency();
    static constexpr usize kNumIterations = 1000;
    static constexpr usize kIterationsPerReset = 50;
    static constexpr usize kWriterYieldsPerIteration = 25;
    static constexpr usize kReaderChecksPerIteration = 25;

    batt::SimpleExecutionContext context;

    batt::ReadWriteLock rw_lock;
    auto data = std::make_unique<std::unordered_map<usize, i64>>();

    // Bump the work count to keep the thread pool alive while we are setting things up.
    //
    context.on_work_started();

    // Create and start a thread pool.
    //
    std::vector<std::thread> thread_pool;
    auto join_thread_pool = batt::finally([&] {
        for (std::thread& t : thread_pool) {
            t.join();
        }
    });

    // Launch one thread for each processor, pinning them so we increase the changes of true parallel
    // execution.
    //
    for (usize i = 0; i < kNumThreads; ++i) {
        thread_pool.emplace_back([&context, i] {
            batt::pin_thread_to_cpu(i).IgnoreError();
            context.run();
        });
    }

    // Launch readers and writers (equal number of each).
    //
    std::vector<usize> read_ok_count(kNumThreads, 0);
    std::vector<i64> max_reader_count(kNumThreads, 0);
    std::vector<usize> write_ok_count(kNumThreads, 0);
    std::vector<std::unique_ptr<batt::Task>> tasks;
    std::vector<u64> task_progress(kNumThreads * 2, 0);

    for (usize i = 0; i < kNumThreads; ++i)  //
    {
        // Each reader `i` will check to make sure that data[i], if present, is a multiple of (i+1).
        //
        // We keep track of the total number of successful checks performed so we can verify at the end.
        //
        tasks.emplace_back(std::make_unique<batt::Task>(
            context.get_executor(),
            [i, &rw_lock, &read_ok_count, &data, &max_reader_count, &task_progress] {
                u64& my_progress = task_progress[i];
                for (usize j = 0; j < kNumIterations; ++j) {
                    batt::ReadWriteLock::Reader reader{rw_lock};
                    for (usize k = 0; k < kReaderChecksPerIteration; ++k) {
                        auto iter = data->find(i);
                        if (iter != data->end()) {
                            if ((iter->second % (i + 1)) == 0) {
                                ++read_ok_count[i];
                            }
                        } else {
                            ++read_ok_count[i];
                        }
                        max_reader_count[i] = std::max(rw_lock.reader_count(), max_reader_count[i]);
                        ++my_progress;
                    }
                }
            },
            batt::to_string("reader_", i)));

        // Each writer will uniquely seed a random number generator and chaotically increment the buckets in
        // `data`, maintaining the invariant that `data[i] % (i + 1) == 0` at critical section boundaries.
        //
        // In addition, writer 0 will also periodically (once every kIterationsPerReset) delete `data` and
        // replace it with a freshly allocated collection object.  The idea here is to try to trigger a crash
        // if the mutual exclusion isn't working properly.
        //
        tasks.emplace_back(std::make_unique<batt::Task>(
            context.get_executor(),
            [i, &write_ok_count, &data, &rw_lock, &task_progress] {
                u64& my_progress = task_progress[i + kNumThreads];
                std::default_random_engine rng{(unsigned)i};
                std::uniform_int_distribution<usize> pick_bucket{0, kNumThreads - 1};
                for (usize j = 0; j < kNumIterations; ++j) {
                    const usize bucket = pick_bucket(rng);
                    {
                        batt::ReadWriteLock::Writer writer{rw_lock};
                        if (i == 0 && (j % kIterationsPerReset) == 0) {
                            data = std::make_unique<std::unordered_map<usize, i64>>();
                        }
                        for (usize l = 0; l < (bucket + 1); ++l) {
                            (*data)[bucket] = (*data)[bucket] + 1;
                        }
                    }
                    ++write_ok_count[i];
                    for (usize k = 0; k < kWriterYieldsPerIteration; ++k) {
                        batt::Task::yield();
                    }
                    ++my_progress;
                }
            },
            batt::to_string("writer_", i)));
    }

    // Allow the thread pool to shut down as soon as all tasks are done running.
    //
    context.on_work_finished();

    std::atomic<bool> done{false};
    std::thread monitor_thread{[&] {
        int no_progress_count = 0;
        std::vector<u64> last_progress(kNumThreads * 2, 0);
        while (done.load() == false) {
            last_progress = task_progress;
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            if (last_progress == task_progress) {
                ++no_progress_count;
                if (no_progress_count == 10 || no_progress_count == 50) {
                    std::cerr << "No progress!" << BATT_INSPECT(no_progress_count)
                              << BATT_INSPECT(kNumIterations)
                              << BATT_INSPECT((kNumIterations * kReaderChecksPerIteration)) << "\n"
                              << BATT_INSPECT_RANGE(last_progress) << "\n"
                              << BATT_INSPECT_RANGE(task_progress) << std::endl;

                    batt::Task::backtrace_all(/*force=*/false);
                }
            } else {
                no_progress_count = 0;
            }
        }
    }};
    auto on_scope_exit = batt::finally([&] {
        done.store(true);
        monitor_thread.join();
    });

    // Run the tasks!
    //
    for (std::unique_ptr<batt::Task>& task : tasks) {
        task->join();
    }

    // Make sure that all checks were OK!
    //
    for (usize n : read_ok_count) {
        EXPECT_EQ(n, kNumIterations * kReaderChecksPerIteration);
    }
    for (usize n : write_ok_count) {
        EXPECT_EQ(n, kNumIterations);
    }

    // Each reader keeps track of the maximum concurrent `reader_count()` it sees; we verify that this is
    // greater than 1 (otherwise, what is the point of a reader/writer lock anyhow?)
    //
    auto iter = std::max_element(max_reader_count.begin(), max_reader_count.end());
    ASSERT_NE(iter, max_reader_count.end());
    EXPECT_GT(*iter, 1);
}

struct CustomProtectedObjectBase {
    explicit CustomProtectedObjectBase(int n) noexcept : counter{n}
    {
    }

    CustomProtectedObjectBase() noexcept : counter{0}
    {
    }

    std::atomic<int> counter;
};

struct CustomProtectedObject : CustomProtectedObjectBase {
    using ThreadSafeBase = CustomProtectedObjectBase;

    explicit CustomProtectedObject(int n, int i) noexcept : CustomProtectedObjectBase{n}, value{i}
    {
    }

    CustomProtectedObject() noexcept : value{0}
    {
    }

    int value;
};

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
TEST(AsyncReadWriteMutexTest, BasicTest)
{
    batt::ReadWriteMutex<std::string> m{"initial value"};
    {
        batt::ScopedReadLock<std::string> lock1{m};
        batt::ScopedReadLock<std::string> lock2{m};

        EXPECT_EQ(lock1->c_str(), (*lock2).c_str());

        // Infer the type and verify it is what we expect.
        //
        decltype(auto) ref = *lock1;

        EXPECT_TRUE((std::is_same_v<decltype(ref), const std::string&>));
    }
    {
        batt::ScopedWriteLock<std::string> lock{m};

        // Infer the type and verify it is what we expect.
        //
        decltype(auto) ref = *lock;

        EXPECT_TRUE((std::is_same_v<decltype(ref), std::string&>));

        *lock = "changed";
    }
    {
        batt::ScopedReadLock<std::string> lock{m};

        EXPECT_THAT(*lock, ::testing::StrEq("changed"));
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
TEST(AsyncReadWriteMutexTest, CustomType)
{
    {
        batt::ReadWriteMutex<CustomProtectedObject> m;

        EXPECT_EQ(m->counter.load(), 0);

        decltype(auto) base_ref = m.no_lock();

        EXPECT_TRUE((std::is_same_v<decltype(base_ref), CustomProtectedObjectBase&>));

        int observed_value = m.with_read_lock([](auto&& v) {
            static_assert(std::is_same_v<decltype(v), const CustomProtectedObject&>);
            return v.value;
        });

        EXPECT_EQ(observed_value, 0);

        m.with_write_lock([](auto&& v) {
            static_assert(std::is_same_v<decltype(v), CustomProtectedObject&>);
            v.value = 5;
        });

        int observed_value2 = m.with_read_lock([](const CustomProtectedObject& v) {
            return v.value;
        });

        EXPECT_EQ(observed_value2, 5);
    }
    {
        batt::ReadWriteMutex<CustomProtectedObject> m{7, 49};

        EXPECT_EQ(m->counter.load(), 7);

        int observed_value = m.with_read_lock([](const CustomProtectedObject& v) {
            return v.value;
        });

        EXPECT_EQ(observed_value, 49);
    }
}

}  // namespace
