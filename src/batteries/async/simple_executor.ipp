//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_SIMPLE_EXECUTOR_IPP
#define BATTERIES_ASYNC_SIMPLE_EXECUTOR_IPP

#include <batteries/config.hpp>
//
#include <batteries/async/simple_executor.hpp>

namespace batt {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename Fn>
inline auto SimpleExecutionContext::dispatch(Fn&& fn) noexcept -> void
{
    if (*Self::inside_run_on_this_thread() == this) {
        BATT_FORWARD(fn)();
        return;
    }

    this->post(BATT_FORWARD(fn));
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename Fn>
inline auto SimpleExecutionContext::post(Fn&& fn) noexcept -> void
{
    {
        std::unique_lock<std::mutex> lock{this->mutex_};
        push_handler(&this->queue_, std::decay_t<Fn>{BATT_FORWARD(fn)});
        this->on_work_started();
    }
    this->cond_.notify_one();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename Fn>
inline auto SimpleExecutionContext::defer(Fn&& fn) noexcept -> void
{
    this->post(BATT_FORWARD(fn));
}

}  //namespace batt

#endif  // BATTERIES_ASYNC_SIMPLE_EXECUTOR_IPP
