//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2024 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_WATCH_DECL_HPP
#define BATTERIES_ASYNC_WATCH_DECL_HPP

#include <batteries/config.hpp>
//
#include <batteries/assert.hpp>
#include <batteries/async/handler.hpp>
#include <batteries/cpu_align.hpp>
#include <batteries/finally.hpp>
#include <batteries/int_types.hpp>
#include <batteries/math.hpp>
#include <batteries/optional.hpp>
#include <batteries/seq/natural_order.hpp>
#include <batteries/status.hpp>
#include <batteries/type_traits.hpp>

#include <bitset>
#include <mutex>
#include <ostream>
#include <thread>
#include <type_traits>

namespace batt {

/**
 * A batt::Watch is like a `std::atomic` that you can block on, synchronously and asynchronously; see also
 * [batt::WatchAtomic](/_autogen/Classes/classbatt_1_1WatchAtomic).  Like `std::atomic`, it has methods to
 * atomically get/set/increment/etc.  But unlike `std::atomic`, you can also block a task waiting for some
 * condition to be true.
 *
 * Example:
 *
 * ```
 * #include <batteries/async/watch.hpp>
 * #include <batteries/assert.hpp>  // for BATT_CHECK_OK
 * #include <batteries/status.hpp>  // for batt::Status
 *
 * int main() {
 *   batt::Watch<bool> done{false};
 *
 *   // Launch some background task that will do stuff, then set `done`
 *   // to `true` when it is finished.
 *   //
 *   launch_background_task(&done);
 *
 *   batt::Status status = done.await_equal(true);
 *   BATT_CHECK_OK(status);
 *
 *   return 0;
 * }
 * ```
 */
template <typename T>
class Watch
{
   public:
    /** \brief Watch is not copy-constructible
     */
    Watch(const Watch&) = delete;

    /** \brief Watch is not copy-assignable
     */
    Watch& operator=(const Watch&) = delete;

    /** \brief Constructs a batt::Watch object with a default-initialized value of `T`.
     */
    Watch() = default;

    /** \brief Constructs a batt::Watch object with the given initial value.
     */
    template <typename Init, typename = EnableIfNoShadow<Watch, Init>>
    explicit Watch(Init&& init_value) noexcept : value_(BATT_FORWARD(init_value))
    {
    }

    /** \brief Destroys the Watch, automatically calling Watch::close.
     */
    ~Watch()
    {
        this->close();
    }

    /** \brief Sets the Watch to the "closed" state, which disables all blocking/async synchronization on the
     *  Watch, immediately unblocking any currently waiting tasks/threads.
     *
     * This method is safe to call multiple times.  The Watch value can still be modified and retrieved after
     * it is closed; this only disables the methods in the "Synchronization" category (see Summary section
     * above).
     */
    void close()
    {
        HandlerList<StatusOr<T>> local_observers;
        {
            std::unique_lock<std::mutex> lock{mutex_};
            this->closed_ = true;
            std::swap(local_observers, this->observers_);
        }

        invoke_all_handlers(&local_observers, Status{StatusCode::kClosed});
    }

    /** \brief Tests whether the Watch is in a "closed" state.
     */
    bool is_closed() const
    {
        std::unique_lock<std::mutex> lock{mutex_};
        return this->closed_;
    }

    /** \brief Atomically set the value of the Watch.
     */
    void set_value(const T& new_value)
    {
        HandlerList<StatusOr<T>> local_observers;
        {
            std::unique_lock<std::mutex> lock{mutex_};
            if (new_value != this->value_) {
                value_ = new_value;
                std::swap(local_observers, this->observers_);
            }
        }
        invoke_all_handlers(&local_observers, new_value);
    }

    /** \brief The current value of the Watch.
     */
    T get_value() const
    {
        std::unique_lock<std::mutex> lock{mutex_};
        return value_;
    }

    /** \brief Atomically modifies the Watch value by applying the passed transform `fn`.
     *
     * `fn` **MUST** be safe to call multiple times within a single call to `modify`.  This is because
     * `modify` may be implemented via an atomic compare-and-swap loop.
     *
     * \return if `T` is a primitive integer type (including `bool`), the new value of the Watch; else, the
     *         old value of the Watch
     *
     * _NOTE: This behavior is acknowledged to be less than ideal and will be fixed in the future to be
     * consistent, regardless of `T`_
     */
    template <typename Fn>
    T modify(Fn&& fn)
    {
        Optional<T> new_value;
        HandlerList<StatusOr<T>> local_observers;
        {
            std::unique_lock<std::mutex> lock{mutex_};
            new_value.emplace(BATT_FORWARD(fn)(value_));
            if (*new_value != value_) {
                value_ = *new_value;
                std::swap(local_observers, this->observers_);
            }
        }
        invoke_all_handlers(&local_observers, *new_value);
        return std::move(*new_value);
    }

    /** \brief Invokes the passed handler `fn` with the described value as soon as one of the following
     * conditions is true:
     *    - When the Watch value is _not_ equal to the passed value `last_seen`, invoke `fn` with the current
     *      value of the Watch.
     *    - When the Watch is closed, invoke `fn` with `batt::StatusCode::kClosed`.
     */
    template <typename Handler>
    void async_wait(const T& last_seen, Handler&& fn)
    {
        bool local_closed = false;
        bool changed = false;
        T new_value;
        {
            std::unique_lock<std::mutex> lock{mutex_};
            if (this->closed_) {
                local_closed = true;
            } else if (value_ == last_seen && !this->closed_) {
                push_handler(&this->observers_, BATT_FORWARD(fn));
            } else {
                changed = true;
                new_value = value_;
            }
        }

        if (local_closed) {
            BATT_FORWARD(fn)(Status{StatusCode::kClosed});
        } else if (changed) {
            BATT_FORWARD(fn)(new_value);
        }
    }

    /** \brief Blocks the current task/thread until the Watch value is _not_ equal to `last_seen`.
     *
     * \return  On success, the current value of the Watch, which is guaranteed to _not_ equal `last_seen`,
     *          else `batt::StatusCode::kClosed` if the Watch was closed before a satisfactory value was
     *          observed
     */
    StatusOr<T> await_not_equal(const T& last_seen);

    /** \brief Blocks the current task/thread until the passed predicate function returns `true` for the
     * current value of the Watch.
     *
     * This is the most general of Watch's blocking getter methods.
     *
     * \return  On success, the Watch value for which `pred` returned `true`, else `batt::StatusCode::kClosed`
     *          if the Watch was closed before a satisfactory value was observed
     */
    template <typename Pred>
    StatusOr<T> await_true(Pred&& pred)
    {
        StatusOr<T> last_seen = this->get_value();

        while (last_seen.ok() && !pred(*last_seen)) {
            last_seen = this->await_not_equal(*last_seen);
        }

        return last_seen;
    }

    /** \brief Modify the value to be at least `lower_bound`.
     *
     * \param lower_bound The (inclusive) lower bound value to enforce
     * \param order_fn A callable object taking two T values and returning bool that returns true iff the
     * first argument is less-than the second.
     */
    template <typename OrderFn = bool(const T&, const T&)>
    void clamp_min_value(const T& lower_bound, OrderFn&& order_fn)
    {
        this->modify([&](const T& observed) -> const T& {
            if (order_fn(observed, lower_bound)) {
                return lower_bound;
            }
            return observed;
        });
    }

    /** \brief Modify the value to be at most `upper_bound`.
     *
     * \param upper_bound The (inclusive) upper bound value to enforce
     * \param order_fn A callable object taking two T values and returning bool that returns true iff the
     * first argument is less-than the second.
     */
    template <typename OrderFn = bool(const T&, const T&)>
    void clamp_max_value(const T& upper_bound, OrderFn&& order_fn)
    {
        this->modify([&](const T& observed) -> const T& {
            if (order_fn(upper_bound, observed)) {
                return upper_bound;
            }
            return observed;
        });
    }

    /** \brief Modify the value to be at least `lower_bound`, using the default ordering for T.
     */
    void clamp_min_value(const T& lower_bound)
    {
        this->clamp_min_value(lower_bound, seq::NaturalOrder{});
    }

    /** \brief Modify the value to be at most `upper_bound`, using the default ordering for T.
     */
    void clamp_max_value(const T& upper_bound)
    {
        this->clamp_max_value(upper_bound, seq::NaturalOrder{});
    }

   private:
    mutable std::mutex mutex_;
    bool closed_ = false;
    T value_;
    HandlerList<StatusOr<T>> observers_;
};

/** \brief Support for printing Watch<T> to ostream.
 */
template <typename T>
inline std::ostream& operator<<(std::ostream& out, const Watch<T>& w)
{
    return out << make_printable(w.get_value());
}

struct HoldOwnership {
};

struct ReleaseOwnership {
};

/** Watch for atomic primitive type.
 */
template <typename T>
class WatchAtomic
{
   public:
    using value_type = T;

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    /** \brief (INTERNAL USE ONLY) spin-lock bit - indicates the state variable is locked.
     */
    static constexpr u32 kLocked = 0x01;

    /** \brief (INTERNAL USE ONLY) indicates that the Watch is not closed.
     */
    static constexpr u32 kOpen = 0x02;

    /** \brief (INTERNAL USE ONLY) indicates that one or more handlers are attached to the Watch, awaiting
     * change notification.
     */
    static constexpr u32 kWaiting = 0x04;

    /** \brief (INTERNAL USE ONLY) indicates the Watch was closed with end-of-stream condition true
     */
    static constexpr u32 kClosedAtEnd = 0x08;

    /** \brief (INTERNAL USE ONLY) indicates the Watch was closed with end-of-stream condition false
     */
    static constexpr u32 kClosedBeforeEnd = 0x10;

    /** \brief (INTERNAL USE ONLY)
     */
    static constexpr u32 kPoked = 0x20;

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    /** \brief Watch is not copy-constructible
     */
    WatchAtomic(const WatchAtomic&) = delete;

    /** \brief Watch is not copy-constructible
     */
    WatchAtomic& operator=(const WatchAtomic&) = delete;

    /** \brief Constructs a batt::Watch object with a default-initialized value of `T`.
     */
    WatchAtomic() = default;

    /** \brief Constructs a batt::Watch object with the given initial value.
     */
    template <typename Init, typename = EnableIfNoShadow<WatchAtomic, Init>>
    explicit WatchAtomic(Init&& init_value) noexcept : value_(BATT_FORWARD(init_value))
    {
        static_assert(kLocked != (kLocked | kOpen));
        static_assert((kLocked | kOpen) != (kLocked | kOpen | kWaiting));
        static_assert((kLocked | kOpen | kWaiting) != (kLocked | kOpen | kWaiting | kClosedAtEnd));
        static_assert((kLocked | kOpen | kWaiting | kClosedAtEnd) !=
                      (kLocked | kOpen | kWaiting | kClosedAtEnd | kClosedBeforeEnd));
        static_assert((kLocked | kOpen | kWaiting | kClosedAtEnd | kClosedBeforeEnd) !=
                      (kLocked | kOpen | kWaiting | kClosedAtEnd | kClosedBeforeEnd | kPoked));
    }

    /** \brief Destroy the Watch, automatically calling Watch::close.
     */
    ~WatchAtomic() noexcept
    {
        this->close();
    }

    /** \brief Set the Watch to the "closed" state, which disables all blocking/async synchronization on the
     *  Watch, immediately unblocking any currently waiting tasks/threads.
     *
     * This method is safe to call multiple times.  The Watch value can still be modified and retrieved after
     * it is closed; this only disables the methods in the "Synchronization" category (see Summary section
     * above).
     */
    void close(StatusCode final_status_code = StatusCode::kClosed)
    {
        HandlerList<StatusOr<T>> local_observers;
        {
            const u32 prior_state = this->lock_observers();
            std::swap(local_observers, this->observers_);

            const u32 desired_state = (prior_state & ~(kOpen | kWaiting)) | ([&]() -> u32 {
                                          // If already closed, don't change the closed status.
                                          //
                                          if ((prior_state & kOpen) != kOpen) {
                                              return 0;
                                          }

                                          BATT_SUPPRESS_IF_GCC("-Wswitch-enum")
                                          BATT_SUPPRESS_IF_CLANG("-Wswitch-enum")
                                          {
                                              switch (final_status_code) {
                                              case StatusCode::kEndOfStream:
                                                  return WatchAtomic::kClosedAtEnd;

                                              case StatusCode::kClosedBeforeEndOfStream:
                                                  return WatchAtomic::kClosedBeforeEnd;

                                              case StatusCode::kClosed:  // fall-through
                                              default:
                                                  // All other StatusCode values are ignored; set status
                                                  // StatusCode::kClosed.
                                                  //
                                                  return 0;
                                              }
                                          }
                                          BATT_UNSUPPRESS_IF_CLANG()
                                          BATT_UNSUPPRESS_IF_GCC()
                                      }());

            this->unlock_observers(desired_state);
        }

        invoke_all_handlers(&local_observers, this->get_final_status());
        //
        // IMPORTANT: Nothing can come after invoking observers, since we must allow one observer to delete
        // the WatchAtomic object (`this`).
    }

    /** \brief Wakes up all observers of this Watch, invoking their handlers with StatusCode::kPoke to
     * indicate they should check their current situation and possibly retry.
     */
    void poke() noexcept
    {
        HandlerList<StatusOr<T>> local_observers;
        {
            const u32 prior_state = this->lock_observers();
            std::swap(local_observers, this->observers_);

            // If we have an observer to invoke, then clear the waiting bit when we unlock; otherwise, set the
            // poked bit so that the next observer will be poked instead of waiting.  This resolves the race
            // condition:
            //
            // 1. (task A) check STATE
            // 2. (task B) update STATE
            // 3. (task B) poke (to force A to recheck STATE before awaiting on watch to change)
            // 4. (task A) watch.await_true(...)
            //
            const u32 desired_state = [&] {
                if ((prior_state & kWaiting) == kWaiting) {
                    return prior_state & ~kWaiting;
                }
                return prior_state | kPoked;
            }();
            this->unlock_observers(desired_state);
        }
        invoke_all_handlers(&local_observers, Status{StatusCode::kPoke});
    }

    /** \brief Returns true iff there is a pending poke on this object.
     */
    bool is_poked() const noexcept
    {
        return (this->spin_state_.load() & kPoked) == kPoked;
    }

    /** \brief Resets the 'closed' status of this Watch.
     */
    void reset()
    {
        const u32 prior_state = this->lock_observers();

        // Set the open bit again.
        //
        const u32 desired_state = prior_state | kOpen;

        this->unlock_observers(desired_state);
    }

    /** \brief Tests whether the Watch is in a "closed" state.
     */
    bool is_closed() const
    {
        return !(this->spin_state_.load() & kOpen);
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -

#define BATT_WATCH_HOLD_IMPL(old_value_expr, new_value_expr)                                                 \
    T old_value = old_value_expr;                                                                            \
    T new_value = new_value_expr;                                                                            \
    if (new_value != old_value) {                                                                            \
        this->notify(new_value);                                                                             \
    }                                                                                                        \
    return old_value

#define BATT_WATCH_RELEASE_IMPL(old_value_expr, new_value_expr)                                              \
    HandlerList<StatusOr<T>> local_observers;                                                                \
    T old_value;                                                                                             \
    T new_value;                                                                                             \
    {                                                                                                        \
        const auto pre_lock_state = this->lock_observers();                                                  \
        old_value = old_value_expr;                                                                          \
        new_value = new_value_expr;                                                                          \
        if (new_value != old_value) {                                                                        \
            std::swap(local_observers, this->observers_);                                                    \
        }                                                                                                    \
        this->unlock_observers(pre_lock_state & ~(kWaiting));                                                \
    }                                                                                                        \
    invoke_all_handlers(&local_observers, new_value);                                                        \
    return old_value

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    /** \brief Atomically set the value of the Watch.  NOT safe to call when updating the value may cause the
     * Watch to be destroyed on some other thread.
     *
     * \return The prior value of the Watch.
     */
    T set_value(T arg, HoldOwnership = {}) noexcept
    {
        BATT_WATCH_HOLD_IMPL(this->value().exchange(arg), arg);
    }

    /** \brief Same as set_value, but safe to call even when setting the value may cause the Watch object to
     * be destroyed.
     *
     * Using this function instead of `set_value` probably implies its a bad idea to make any future reference
     * to the Watch object after `set_value_and_release` returns.
     */
    T set_value(T arg, ReleaseOwnership) noexcept
    {
        BATT_WATCH_RELEASE_IMPL(this->value().exchange(arg), arg);
    }

    /** \brief Deprecated; use `this->set_value(x, ReleaseOwnership{})`.
     */
    T set_value_and_release(T new_value) noexcept
    {
        return this->set_value(new_value, ReleaseOwnership{});
    }

    /** \brief The current value of the Watch.
     */
    T get_value() const noexcept
    {
        return this->value().load();
    }

    /** \brief Atomically adds the specified amount to the Watch value, returning the previous value.
     */
    T fetch_add(T arg, HoldOwnership = {}) noexcept
    {
        BATT_WATCH_HOLD_IMPL(this->value().fetch_add(arg), old_value + arg);
    }

    T fetch_add(T arg, ReleaseOwnership) noexcept
    {
        BATT_WATCH_RELEASE_IMPL(this->value().fetch_add(arg), old_value + arg);
    }

    /** \brief Atomically sets the Watch value to the bitwise-or of the current value and the passed `arg`,
     * returning the previous value.
     */
    T fetch_or(T arg, HoldOwnership = {}) noexcept
    {
        BATT_WATCH_HOLD_IMPL(this->value().fetch_or(arg), old_value | arg);
    }

    T fetch_or(T arg, ReleaseOwnership) noexcept
    {
        BATT_WATCH_RELEASE_IMPL(this->value().fetch_or(arg), old_value | arg);
    }

    /** \brief Atomically subtracts the specified amount from the Watch value, returning the previous value.
     */
    T fetch_sub(T arg, HoldOwnership = {}) noexcept
    {
        BATT_WATCH_HOLD_IMPL(this->value().fetch_sub(arg), old_value - arg);
    }

    T fetch_sub(T arg, ReleaseOwnership) noexcept
    {
        BATT_WATCH_RELEASE_IMPL(this->value().fetch_sub(arg), old_value - arg);
    }

    /** \brief Atomically sets the Watch value to the bitwise-and of the current value and the passed `arg`,
     * returning the previous value.
     */
    T fetch_and(T arg, HoldOwnership = {})
    {
        BATT_WATCH_HOLD_IMPL(this->value().fetch_and(arg), old_value & arg);
    }

    /** \brief Same as `this->fetch_add(arg)`, but safe to call when this modification might cause the Watch
     * object to be destroyed.
     */
    T fetch_and(T arg, ReleaseOwnership)
    {
        BATT_WATCH_RELEASE_IMPL(this->value().fetch_and(arg), old_value & arg);
    }

    /** \brief Atomically modifies the Watch value by applying the passed transform `fn`.
     *
     * `fn` **MUST** be safe to call multiple times within a single call to `modify`.  This is because
     * `modify` may be implemented via an atomic compare-and-swap loop.
     *
     * \return if `T` is a primitive integer type (including `bool`), the new value of the Watch; else, the
     *         old value of the Watch
     *
     * _NOTE: This behavior is acknowledged to be less than ideal and will be fixed in the future to be
     * consistent, regardless of `T`_
     */
    template <typename Fn = T(T)>
    T modify(Fn&& fn)
    {
        T old_value = this->value().load();
        bool changed = false;

        const T new_value = [&] {
            for (;;) {
                const T modified_value = fn(old_value);
                changed = changed || (modified_value != old_value);
                if (this->value().compare_exchange_weak(old_value, modified_value)) {
                    return modified_value;
                }
            }
        }();

        if (changed) {
            this->notify(new_value);
            //
            // IMPORTANT: we must not touch *anything* in this after `notify`, since a handler may have
            // deleted this Watch object.
        }

        // TODO [tastolfi 2021-10-14] make the non-atomic version of modify consistent with this behavior!
        return old_value;
    }

    /** \brief Does an atomic compare-and-swap operation on the Watch value.
     *
     * If the watch is equal to `old_value`, then it will be set to `new_value` and this function will return
     * true. Otherwise, `old_value` will be updated with the observed value and this function will return
     * false.
     */
    bool compare_exchange(T& old_value, T new_value)
    {
        const T init_old_value = old_value;
        for (;;) {
            if (this->value().compare_exchange_weak(old_value, new_value)) {
                break;
            }
            if (old_value != init_old_value) {
                return false;
            }
        }

        if (init_old_value != new_value) {
            this->notify(new_value);
        }
        return true;
    }

    /** Retries `fn` on the watch value until it succeeds or the watch is closed.  Return the old (pre-modify)
     * value on which `fn` finally succeeded.
     *
     * `fn` should have the signature (T) -> Optional<T>.  Returning None indicates `fn` should not be called
     * again until a new value is available.
     *
     * `fn` **MUST** be safe to call multiple times within a single call to `await_modify`.  This is because
     * `await_modify` may be implemented via an atomic compare-and-swap loop.
     *
     * \return
     *    - If successful, the old (pre-modify) value on which `fn` finally succeeded
     *    - `batt::StatusCode::kClosed` if the Watch was closed before `fn` was successful
     */
    template <typename Fn = Optional<T>(T)>
    StatusOr<T> await_modify(Fn&& fn);

    /** \brief Conditionally modify the value of the Watch.
     *
     * Retries calling `fn` on the Watch value until EITHER of:
     *     - `fn` returns `batt::None`
     *     - BOTH of:
     *         - `fn` returns a non-`batt::None` value
     *         - the Watch value is atomically updated via compare-and-swap
     *
     * `fn` **MUST** be safe to call multiple times within a single call to `modify_if`.  This is because
     * `modify_if` may be implemented via an atomic compare-and-swap loop.
     *
     * Unlike batt::Watch::await_modify, this method never puts the current task/thread to sleep; it
     * keeps _actively_ polling the Watch value until it reaches one of the exit criteria described above.
     *
     * \return The final value returned by `fn`, which is either `batt::None` or the new Watch value.
     */
    template <typename Fn = Optional<T>(T)>
    Optional<T> modify_if(Fn&& fn)
    {
        T old_value = this->value().load();
        bool changed = false;

        const Optional<T> new_value = [&] {
            for (;;) {
                const Optional<T> modified_value = fn(old_value);
                changed = changed || (modified_value && *modified_value != old_value);
                if (!modified_value || this->value().compare_exchange_weak(old_value, *modified_value)) {
                    return modified_value;
                }
            }
        }();

        if (!new_value) {
            return None;
        }

        if (changed) {
            this->notify(*new_value);
            //
            // IMPORTANT: we must not touch *anything* in this after `notify`, since a handler may have
            // deleted this Watch object.
        }
        return old_value;
    }

    /** \brief Invokes the passed handler `fn` with the described value as soon as one of the following
     * conditions is true:
     *    - When the Watch value is _not_ equal to the passed value `last_seen`, invoke `fn` with the current
     *      value of the Watch.
     *    - When the Watch is closed, invoke `fn` with `batt::StatusCode::kClosed`.
     */
    template <typename Handler>
    void async_wait(T last_seen, Handler&& fn) const
    {
        T now_seen = this->value().load();

        bool changed = (now_seen != last_seen);

        if (!changed) {
            u32 prior_state = this->lock_observers();

            if (!(prior_state & kOpen)) {
                this->unlock_observers(prior_state);
                BATT_FORWARD(fn)(this->get_final_status());
                return;
            }
            if ((prior_state & kPoked) == kPoked) {
                this->unlock_observers(prior_state & ~kPoked);
                BATT_FORWARD(fn)(Status{StatusCode::kPoke});
                return;
            }
            auto unlock_guard = finally([&] {
                this->unlock_observers(prior_state);
            });

            now_seen = this->value().load();
            changed = (now_seen != last_seen);

            if (!changed) {
                push_handler(&this->observers_, BATT_FORWARD(fn));
                prior_state |= kWaiting;
                return;
                //
                // The dtor of `unlock_guard` will atomically clear the `kLocked` flag and set `kWaiting`.
            }
        }
        //
        // If we get here, either the initial `changed` check was true, we are closed, or the second `changed`
        // check (with the spin lock held) must have succeeded; in any case, invoke the handler immediately.

        BATT_FORWARD(fn)(now_seen);
    }

    /** \brief Blocks the current task/thread until the Watch value is _not_ equal to `last_seen`.
     *
     * \return  On success, the current value of the Watch, which is guaranteed to _not_ equal `last_seen`,
     *          else `batt::StatusCode::kClosed` if the Watch was closed before a satisfactory value was
     *          observed
     */
    StatusOr<T> await_not_equal(const T& last_seen) const;

    /** \brief Blocks the current task/thread until the passed predicate function returns `true` for the
     * current value of the Watch.
     *
     * This is the most general of Watch's blocking getter methods.
     *
     * \return  On success, the Watch value for which `pred` returned `true`, else `batt::StatusCode::kClosed`
     *          if the Watch was closed before a satisfactory value was observed
     */
    template <typename Pred>
    StatusOr<T> await_true(Pred&& pred) const
    {
        StatusOr<T> last_seen = this->get_value();

        while (last_seen.ok() && !pred(*last_seen)) {
            last_seen = this->await_not_equal(*last_seen);
        }

        return last_seen;
    }

    /** \brief Blocks the current task/thread until the Watch contains the specified value.
     *
     * \return One of the following:
     *         - `batt::OkStatus()` if the Watch value was observed to be `val`
     *         - `batt::StatusCode::kClosed` if the Watch was closed before `val` was observed
     */
    Status await_equal(T val) const
    {
        return this
            ->await_true([val](T observed) {
                return observed == val;
            })
            .status();
    }

    /** \brief Modify the value to be at least `lower_bound`.
     *
     * \param lower_bound The (inclusive) lower bound value to enforce
     * \param order_fn A callable object taking two T values and returning bool that returns true iff the
     * first argument is less-than the second.
     */
    template <typename OrderFn = bool(T, T)>
    void clamp_min_value(T lower_bound, OrderFn&& order_fn)
    {
        this->modify([&](T observed) -> T {
            if (order_fn(observed, lower_bound)) {
                return lower_bound;
            }
            return observed;
        });
    }

    /** \brief Modify the value to be at most `upper_bound`.
     *
     * \param upper_bound The (inclusive) upper bound value to enforce
     * \param order_fn A callable object taking two T values and returning bool that returns true iff the
     * first argument is less-than the second.
     */
    template <typename OrderFn = bool(T, T)>
    void clamp_max_value(T upper_bound, OrderFn&& order_fn)
    {
        this->modify([&](T observed) -> T {
            if (order_fn(upper_bound, observed)) {
                return upper_bound;
            }
            return observed;
        });
    }

    /** \brief Modify the value to be at least `lower_bound`, using the default ordering for T.
     */
    void clamp_min_value(T lower_bound)
    {
        this->clamp_min_value(lower_bound, seq::NaturalOrder{});
    }

    /** \brief Modify the value to be at most `upper_bound`, using the default ordering for T.
     */
    void clamp_max_value(T upper_bound)
    {
        this->clamp_max_value(upper_bound, seq::NaturalOrder{});
    }

    /** \brief Yields control of the current task to allow other tasks to potentially modify this Watch,
     * returning the post-resume value of this; for busy-polling.
     */
    T poll() noexcept;

   private:
    BATT_ALWAYS_INLINE std::atomic<T>& value() noexcept
    {
        return this->value_;
    }

    BATT_ALWAYS_INLINE const std::atomic<T>& value() const noexcept
    {
        return this->value_;
    }

    Status get_final_status() const
    {
        constexpr u32 mask = WatchAtomic::kClosedAtEnd | WatchAtomic::kClosedBeforeEnd;

        switch (this->spin_state_.load() & mask) {
        case WatchAtomic::kClosedBeforeEnd:
            return Status{StatusCode::kClosedBeforeEndOfStream};

        case WatchAtomic::kClosedAtEnd:
            return Status{StatusCode::kEndOfStream};

        default:
            break;
        }

        return Status{StatusCode::kClosed};
    }

    u32 lock_observers() const
    {
        for (;;) {
            const u32 prior_state = this->spin_state_.fetch_or(kLocked);
            if ((prior_state & kLocked) == 0) {
                return prior_state;
            }
            std::this_thread::yield();
        }
    }

    void unlock_observers(u32 desired_state) const
    {
        this->spin_state_.store(desired_state & ~kLocked);
    }

    void notify(T new_value)
    {
        const auto post_change_state = this->spin_state_.load();
        if ((post_change_state & (kLocked | kWaiting)) == 0) {
            //
            // If there is a concurrent call to async_wait that results in a handler being added to the
            // `observers_` list, it must go through the following atomic events:
            //
            //  1. load value (phase 1), no change
            //  2. set kLocked
            //  3. load value (phase 2), no change
            //  4. set kWaiting
            //
            // The notifier thread (this call), when not waking observers, goes through the following atomic
            // events:
            //
            //  a. change value
            //  b. load spin state, observe not kLocked and not kWaiting
            //
            // (b) must occur before (1) [therefore (a) < (1)] or between (1) and (2) [(a) < (3)].  In either
            // case, the async_wait call will load the value *after* this thread changes it (a), so there will
            // be no spurious deadlocks.
            //
            return;
        }

        // Acquire the spinlock.
        //
        const auto pre_lock_state = this->lock_observers();
        HandlerList<StatusOr<T>> local_observers;
        std::swap(local_observers, this->observers_);
        this->unlock_observers(pre_lock_state & ~(kWaiting));

        invoke_all_handlers(&local_observers, new_value);
        //
        // IMPORTANT: we must not touch *anything* in `this` after invoking handlers, since one of the
        // handlers may delete this Watch object.
    }

    std::atomic<T> value_{0};
    mutable std::atomic<u32> spin_state_{kOpen};
    mutable HandlerList<StatusOr<T>> observers_;
};

/** \brief Support for printing WatchAtomic<T> to ostream.
 */
template <typename T>
inline std::ostream& operator<<(std::ostream& out, const WatchAtomic<T>& w)
{
    return out << w.get_value();
}

#define BATT_SPECIALIZE_WATCH_ATOMIC(type)                                                                   \
    template <>                                                                                              \
    class Watch<type> : public WatchAtomic<type>                                                             \
    {                                                                                                        \
       public:                                                                                               \
        using WatchAtomic<type>::WatchAtomic;                                                                \
    }

BATT_SPECIALIZE_WATCH_ATOMIC(bool);

BATT_SPECIALIZE_WATCH_ATOMIC(i8);
BATT_SPECIALIZE_WATCH_ATOMIC(i16);
BATT_SPECIALIZE_WATCH_ATOMIC(i32);
BATT_SPECIALIZE_WATCH_ATOMIC(i64);

BATT_SPECIALIZE_WATCH_ATOMIC(u8);
BATT_SPECIALIZE_WATCH_ATOMIC(u16);
BATT_SPECIALIZE_WATCH_ATOMIC(u32);
BATT_SPECIALIZE_WATCH_ATOMIC(u64);

#if BATT_COMPILER_IS_CLANG
BATT_SPECIALIZE_WATCH_ATOMIC(signed long);
BATT_SPECIALIZE_WATCH_ATOMIC(unsigned long);
#endif  // BATT_COMPILER_IS_CLANG

BATT_SPECIALIZE_WATCH_ATOMIC(void*);

#undef BATT_SPECIALIZE_WATCH_ATOMIC

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------
// Specialize Watch for all raw pointer types, based on void*.
//
template <typename T>
class Watch<T*> : public WatchAtomic<void*>
{
   public:
    using Super = WatchAtomic<void*>;

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    Watch() = default;

    explicit Watch(T* init_value) noexcept : Super{init_value}
    {
    }

    //==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
    //
    T* set_value(T* new_value) noexcept
    {
        return static_cast<T*>(this->Super::set_value(new_value));
    }

    //==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
    //
    T* set_value_and_release(T* new_value) noexcept
    {
        return static_cast<T*>(this->Super::set_value_and_release(new_value));
    }

    //==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
    //
    T* get_value() const noexcept
    {
        return static_cast<T*>(this->Super::get_value());
    }

    //==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
    //
    template <typename Fn = T*(T*)>
    T* modify(Fn&& fn)
    {
        return static_cast<T*>(this->Super::modify([&fn](void* observed_value) -> void* {
            return fn(static_cast<T*>(observed_value));
        }));
    }

    //==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
    //
    template <typename Fn = Optional<T*>(T*)>
    StatusOr<T*> await_modify(Fn&& fn)
    {
        BATT_ASSIGN_OK_RESULT(void* old_value,
                              this->Super::await_modify([&fn](void* observed_value) -> Optional<void*> {
                                  return fn(static_cast<T*>(observed_value));
                              }));

        return {static_cast<T*>(old_value)};
    }

    //==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
    //
    template <typename Fn = Optional<T*>(T*)>
    Optional<T*> modify_if(Fn&& fn)
    {
        Optional<void*> old_value = this->Super::modify_if([&fn](void* observed_value) -> Optional<void*> {
            return fn(static_cast<T*>(observed_value));
        });
        if (!old_value) {
            return None;
        }
        return static_cast<T*>(*old_value);
    }

    //==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
    //
    StatusOr<T*> await_not_equal(T* old_value)
    {
        BATT_ASSIGN_OK_RESULT(void* new_value, this->Super::await_not_equal(old_value));

        return static_cast<T*>(new_value);
    }
};

}  // namespace batt

#endif  // BATTERIES_ASYNC_WATCH_DECL_HPP
