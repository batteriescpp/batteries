//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2024 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_SCALABLE_GRANT_IMPL_HPP
#define BATTERIES_ASYNC_SCALABLE_GRANT_IMPL_HPP

#include <batteries/config.hpp>
//
#include <batteries/async/debug_info.hpp>

namespace batt {

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------
// class ScalableGrantIssuer

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL ScalableGrantIssuer::~ScalableGrantIssuer() noexcept
{
    BATT_CHECK_EQ(this->total_size_.load(), this->state_.get_count())
        << "This may indicate a Grant is still active!";
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL ScalableGrantIssuer::ScalableGrantIssuer() noexcept : Self{0}
{
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*explicit*/ ScalableGrantIssuer::ScalableGrantIssuer(u64 size) noexcept
    : state_{size}
    , total_size_{size}
{
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL StatusOr<ScalableGrant> ScalableGrantIssuer::issue_grant(
    u64 count, WaitForResource wait_for_resource) noexcept
{
    return this->state_.acquire(this, count, wait_for_resource);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void ScalableGrantIssuer::grow(u64 count) noexcept
{
    const u64 old_size = this->total_size_.fetch_add(count);
    BATT_CHECK_GT(u64{old_size + count}, old_size) << "Integer overflow detected!";
    this->recycle(count);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void ScalableGrantIssuer::close() noexcept
{
    ConsumerList to_wake = this->state_.close();
    State::wake_all(this, to_wake, StatusCode::kClosed);
}

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------
// class ScalableGrantIssuer::State

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*explicit*/ ScalableGrantIssuer::State::State(u64 init_count) noexcept
    : count_{init_count}
    , mutex_{}
    , waiters_{}
    , closed_{false}
{
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL ScalableGrantIssuer::State::State(State&& that) noexcept
    : count_{that.count_.exchange(0)}
    , mutex_{}
    , waiters_{std::move(that.waiters_)}
    , closed_{that.closed_}
{
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*static*/ void ScalableGrantIssuer::State::wake_all(Issuer* issuer, ConsumerList& to_wake,
                                                                      Status status)
{
    while (!to_wake.empty()) {
        Consumer& next = to_wake.front();
        to_wake.pop_front();
        u64 size = next.needed;
        if (status.ok()) {
            next.notify(ScalableGrant{issuer, size});
        } else {
            next.notify(status);
        }
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL StatusOr<ScalableGrant> ScalableGrantIssuer::State::acquire(
    Issuer* issuer, u64 n, WaitForResource wait_for_resource) noexcept
{
    if (issuer == nullptr) {
        return {StatusCode::kFailedPrecondition};
    }

    if (this->try_acquire(n)) {
        return ScalableGrant{issuer, n};
    }

    if (wait_for_resource == WaitForResource::kFalse) {
        return {StatusCode::kGrantUnavailable};
    }

    BATT_DEBUG_INFO("Grant::acquire(n=" << n << ")" << BATT_INSPECT(this->count_)
                                        << BATT_INSPECT(issuer->total_size_));

    return Task::await<StatusOr<ScalableGrant>>([&](auto&& handler) {
        this->async_acquire(issuer, n, BATT_FORWARD(handler));
    });
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL bool ScalableGrantIssuer::State::try_acquire(u64 n) noexcept
{
    Optional<std::unique_lock<std::mutex>> optional_lock;

    u64 observed = this->count_.load();
    for (;;) {
        // If we observe the count to be zero, lock the mutex to rule out the possibility that there is
        // another thread inside release.
        //
        if (observed == 0 && !optional_lock) {
            optional_lock.emplace(this->mutex_);
            observed = this->count_.load();
        }
        if (observed < n) {
            return false;
        }
        if (this->count_.compare_exchange_weak(observed, observed - n)) {
            return true;
        }
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL auto ScalableGrantIssuer::State::release(u64 n_released) noexcept -> ConsumerList
{
    ConsumerList to_wake;

    if (n_released != 0) {
        std::unique_lock<std::mutex> lock{this->mutex_};

        // If there are no waiters, we just increment the count and return.
        //
        if (this->waiters_.empty()) {
            this->count_.fetch_add(n_released);

        } else {
            // Claim exclusive access to all count while release is running; because `release` must lock the
            // mutex (and all acquire methods only decrease count, never increase), we know that count will
            // stay 0 until we replace the local count below.
            //
            u64 local_count = this->count_.exchange(0) + n_released;
            auto on_scope_exit = batt::finally([&] {
                this->count_.store(local_count);
            });

            do {
                Consumer& next = this->waiters_.front();
                const u64 n_needed = next.needed;
                if (local_count < n_needed) {
                    break;
                }
                local_count -= n_needed;
                this->waiters_.pop_front();
                to_wake.push_back(next);
            } while (!this->waiters_.empty());
        }
    }

    return to_wake;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL auto ScalableGrantIssuer::State::close() noexcept -> ConsumerList
{
    ConsumerList to_wake;

    std::unique_lock<std::mutex> lock{this->mutex_};

    this->closed_ = true;
    std::swap(to_wake, this->waiters_);

    return to_wake;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void ScalableGrantIssuer::State::swap(State& that) noexcept
{
    if (BATT_HINT_FALSE(this == &that)) {
        return;
    }
    std::array<std::mutex*, 2> to_lock{&this->mutex_, &that.mutex_};
    if (to_lock[0] > to_lock[1]) {
        std::swap(to_lock[0], to_lock[1]);
    }

    std::unique_lock<std::mutex> lock0{*to_lock[0]};
    std::unique_lock<std::mutex> lock1{*to_lock[1]};

    this->count_.store(that.count_.exchange(this->count_.exchange(0)));
    std::swap(this->closed_, that.closed_);
    std::swap(this->waiters_, that.waiters_);
}

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------
// class ScalableGrant

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL ScalableGrant::ScalableGrant() noexcept : state_{0}, issuer_{nullptr}
{
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL ScalableGrant::ScalableGrant(ScalableGrant&& that) noexcept
    : state_{std::move(that.state_)}
    , issuer_{std::move(that.issuer_)}
{
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL ScalableGrant::ScalableGrant(Issuer* issuer, u64 count) noexcept
    : state_{count}
    , issuer_{issuer}
{
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL ScalableGrant::~ScalableGrant() noexcept
{
    this->revoke();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL StatusOr<ScalableGrant> ScalableGrant::spend(u64 count,
                                                              WaitForResource wait_for_resource) noexcept
{
    return this->state_.acquire(this->issuer_.get(), count, wait_for_resource);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL u64 ScalableGrant::spend_all() noexcept
{
    if (!this->issuer_) {
        return 0;
    }

    const u64 n_to_recycle = this->state_.acquire_all();
    if (n_to_recycle != 0) {
        this->issuer_->recycle(n_to_recycle);
    }

    return n_to_recycle;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL ScalableGrant& ScalableGrant::subsume(ScalableGrant&& that) noexcept
{
    if (this == &that || !that.issuer_) {
        return *this;
    }

    BATT_CHECK_NOT_NULLPTR(this->issuer_) << "It is NOT legal to subsume a Grant into an invalidated Grant.";
    BATT_CHECK_EQ(this->issuer_, that.issuer_);

    const u64 n_subsumed = that.state_.acquire_all();
    Issuer::ConsumerList to_wake = this->state_.release(n_subsumed);
    State::wake_all(this->issuer_.get(), to_wake, OkStatus());

    return *this;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void ScalableGrant::revoke() noexcept
{
    this->spend_all();
    Issuer::ConsumerList to_wake = this->state_.close();
    State::wake_all(this->issuer_.get(), to_wake, StatusCode::kGrantRevoked);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void ScalableGrant::swap(ScalableGrant& that) noexcept
{
    std::swap(this->issuer_, that.issuer_);
    this->state_.swap(that.state_);
}

}  //namespace batt

#endif  // BATTERIES_ASYNC_SCALABLE_GRANT_IMPL_HPP
