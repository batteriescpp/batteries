//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_CANCEL_TOKEN_IMPL_HPP
#define BATTERIES_ASYNC_CANCEL_TOKEN_IMPL_HPP

#include <batteries/config.hpp>
//

namespace batt {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*static*/ const CancelToken& CancelToken::none() noexcept
{
    static const CancelToken none_{None};
    return none_;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*implicit*/ CancelToken::CancelToken(NoneType) noexcept : impl_{nullptr}
{
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL CancelToken& CancelToken::operator=(const NoneType&) noexcept
{
    this->impl_ = nullptr;
    return *this;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void CancelToken::cancel() const noexcept
{
    BATT_CHECK_NOT_NULLPTR(this->impl_);
    this->impl_->state_.fetch_or(kCancelled);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL bool CancelToken::is_valid() const noexcept
{
    return this->impl_ != nullptr;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL bool CancelToken::is_cancelled() const noexcept
{
    BATT_CHECK_NOT_NULLPTR(this->impl_);
    return this->impl_->is_cancelled();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*explicit*/ CancelToken::operator bool() const noexcept
{
    return this->is_valid();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL SmallFn<void(std::ostream&)> CancelToken::debug_info() const noexcept
{
    return [impl = this->impl_](std::ostream& out) {
        if (impl) {
            out << "CancelToken{state=" << std::bitset<8>{impl->state_.get_value()} << ",}";
        } else {
            out << "CancelToken{None}";
        }
    };
}

}  //namespace batt

#endif  // BATTERIES_ASYNC_CANCEL_TOKEN_IMPL_HPP
