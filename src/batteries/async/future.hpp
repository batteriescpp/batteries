//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2023 Anthony Paul Astolfi
//
#include <batteries/config.hpp>

#include <batteries/async/future_decl.hpp>
//
#include <batteries/async/future.ipp>
//
#if BATT_HEADER_ONLY
#include <batteries/async/future_impl.hpp>
#endif

#include <batteries/async/latch.hpp>
