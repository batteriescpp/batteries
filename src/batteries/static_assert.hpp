//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2022 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_STATIC_ASSERT_HPP
#define BATTERIES_STATIC_ASSERT_HPP

#include <batteries/config.hpp>
//
#include <batteries/utility.hpp>

#include <boost/preprocessor/cat.hpp>
#include <type_traits>

namespace batt {

struct Eq;
struct Ne;
struct Lt;
struct Le;
struct Ge;
struct Gt;

template <typename T, typename U, T left, typename Op, U right, bool kCondition>
struct StaticBinaryAssertion : std::integral_constant<bool, kCondition> {
    static_assert(kCondition == true, "");
};

#define BATT_STATIC_ASSERT_EQ(x, y)                                                                          \
    BATT_MAYBE_UNUSED static ::batt::StaticBinaryAssertion<decltype(x), decltype(y), (x), ::batt::Eq, \
                                                           (y), ((x) == (y))>                                \
    BOOST_PP_CAT(BOOST_PP_CAT(BATTERIES_StaticAssert_Instance_, __LINE__), __COUNTER__)

#define BATT_STATIC_ASSERT_NE(x, y)                                                                          \
    BATT_MAYBE_UNUSED static ::batt::StaticBinaryAssertion<decltype(x), decltype(y), (x), ::batt::Ne, \
                                                           (y), ((x) != (y))>                                \
    BOOST_PP_CAT(BOOST_PP_CAT(BATTERIES_StaticAssert_Instance_, __LINE__), __COUNTER__)

#define BATT_STATIC_ASSERT_LT(x, y)                                                                          \
    BATT_MAYBE_UNUSED static ::batt::StaticBinaryAssertion<decltype(x), decltype(y), (x), ::batt::Lt, \
                                                           (y), ((x) < (y))>                                 \
    BOOST_PP_CAT(BOOST_PP_CAT(BATTERIES_StaticAssert_Instance_, __LINE__), __COUNTER__)

#define BATT_STATIC_ASSERT_LE(x, y)                                                                          \
    BATT_MAYBE_UNUSED static ::batt::StaticBinaryAssertion<decltype(x), decltype(y), (x), ::batt::Le, \
                                                           (y), ((x) <= (y))>                                \
    BOOST_PP_CAT(BOOST_PP_CAT(BATTERIES_StaticAssert_Instance_, __LINE__), __COUNTER__)

#define BATT_STATIC_ASSERT_GT(x, y)                                                                          \
    BATT_MAYBE_UNUSED static ::batt::StaticBinaryAssertion<decltype(x), decltype(y), (x), ::batt::Gt, \
                                                           (y), ((x) > (y))>                                 \
    BOOST_PP_CAT(BOOST_PP_CAT(BATTERIES_StaticAssert_Instance_, __LINE__), __COUNTER__)

#define BATT_STATIC_ASSERT_GE(x, y)                                                                          \
    BATT_MAYBE_UNUSED static ::batt::StaticBinaryAssertion<decltype(x), decltype(y), (x), ::batt::Ge, \
                                                           (y), ((x) >= (y))>                                \
    BOOST_PP_CAT(BOOST_PP_CAT(BATTERIES_StaticAssert_Instance_, __LINE__), __COUNTER__)

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -

template <typename T, typename U>
struct StaticSameTypeAssertion {
    static_assert(std::is_same_v<T, U>, "");
};

#if defined(_MSC_VER) && 0
#define BATT_STATIC_ASSERT_TYPE_EQ(x, y) static_assert(std::is_same_v<x, y>)
#else
#define BATT_STATIC_ASSERT_TYPE_EQ(x, y)                                                                     \
    BATT_MAYBE_UNUSED static ::batt::StaticSameTypeAssertion<x, y> BOOST_PP_CAT(                             \
        BOOST_PP_CAT(BATTERIES_StaticAssert_Instance_, __LINE__), __COUNTER__)
#endif

/** \brief Evaluates to `expr`, failing the compilation if the type of the expression doesn't match `type`.
 */
#define BATT_CHECK_TYPE(type, expr)                                                                          \
    [&]() noexcept(noexcept(expr)) -> decltype(auto) {                                                       \
        BATT_STATIC_ASSERT_TYPE_EQ(decltype(expr), type);                                                    \
        return expr;                                                                                         \
    }()

}  // namespace batt

#endif  // BATTERIES_STATIC_ASSERT_HPP
